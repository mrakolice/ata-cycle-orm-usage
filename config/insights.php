<?php

declare(strict_types=1);

use NunoMaduro\PhpInsights\Domain\Insights\ForbiddenTraits;
use SlevomatCodingStandard\Sniffs\PHP\UselessSemicolonSniff;
use NunoMaduro\PhpInsights\Domain\Metrics\Architecture\Classes;
use SlevomatCodingStandard\Sniffs\Functions\StaticClosureSniff;
use NunoMaduro\PhpInsights\Domain\Insights\ForbiddenNormalClasses;
use PHP_CodeSniffer\Standards\Generic\Sniffs\Commenting\TodoSniff;
use ObjectCalisthenics\Sniffs\Classes\ForbiddenPublicPropertySniff;
use PHP_CodeSniffer\Standards\Generic\Sniffs\Commenting\FixmeSniff;
use PHP_CodeSniffer\Standards\Generic\Sniffs\Files\LineLengthSniff;
use NunoMaduro\PhpInsights\Domain\Insights\ForbiddenPrivateMethods;
use SlevomatCodingStandard\Sniffs\Commenting\ForbiddenCommentsSniff;
use NunoMaduro\PhpInsights\Domain\Insights\ForbiddenDefineFunctions;
use SlevomatCodingStandard\Sniffs\TypeHints\TypeHintDeclarationSniff;
use NunoMaduro\PhpInsights\Domain\Insights\CyclomaticComplexityIsHigh;
use PHP_CodeSniffer\Standards\PSR1\Sniffs\Classes\ClassDeclarationSniff;
use SlevomatCodingStandard\Sniffs\Namespaces\AlphabeticallySortedUsesSniff;
use SlevomatCodingStandard\Sniffs\Commenting\InlineDocCommentDeclarationSniff;
use SlevomatCodingStandard\Sniffs\ControlStructures\DisallowYodaComparisonSniff;
use SlevomatCodingStandard\Sniffs\TypeHints\NullableTypeForNullDefaultValueSniff;
use SlevomatCodingStandard\Sniffs\Operators\RequireCombinedAssignmentOperatorSniff;
use PHP_CodeSniffer\Standards\Generic\Sniffs\CodeAnalysis\UselessOverridingMethodSniff;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Preset
    |--------------------------------------------------------------------------
    |
    | This option controls the default preset that will be used by PHP Insights
    | to make your code reliable, simple, and clean. However, you can always
    | adjust the `Metrics` and `Insights` below in this configuration file.
    |
    | Supported: "default", "laravel", "symfony", "magento2", "drupal"
    |
    */

    'preset' => 'laravel',

    /*
    |--------------------------------------------------------------------------
    | Configuration
    |--------------------------------------------------------------------------
    |
    | Here you may adjust all the various `Insights` that will be used by PHP
    | Insights. You can either add, remove or configure `Insights`. Keep in
    | mind that all added `Insights` must belong to a specific `Metric`.
    |
    */

    'exclude' => [
        'app/Core',
//        'app/Infrastructure',
    ],

    'add' => [
        Classes::class => [
            TodoSniff::class,
            FixmeSniff::class,
            StaticClosureSniff::class,
            UselessSemicolonSniff::class,
            ForbiddenCommentsSniff::class,
            TypeHintDeclarationSniff::class,
            DisallowYodaComparisonSniff::class,
            InlineDocCommentDeclarationSniff::class,
            NullableTypeForNullDefaultValueSniff::class,
            RequireCombinedAssignmentOperatorSniff::class,
        ],
    ],

    'remove' => [
        ForbiddenTraits::class,
        ClassDeclarationSniff::class,
//        ForbiddenNormalClasses::class,
        ForbiddenDefineFunctions::class,
        TypeHintDeclarationSniff::class,
        ForbiddenPublicPropertySniff::class,
        UselessOverridingMethodSniff::class,
        AlphabeticallySortedUsesSniff::class,
    ],

    'config' => [
        ForbiddenPrivateMethods::class => [
            'title' => 'The usage of private methods is not idiomatic in Laravel.',
        ],
        LineLengthSniff::class => [
            'lineLimit' => 120,
            'absoluteLineLimit' => 120,
            'ignoreComments' => true,
        ],
        CyclomaticComplexityIsHigh::class => [
            'maxComplexity' => 2,
        ],
        ObjectCalisthenics\Sniffs\Files\FunctionLengthSniff::class => [
            'exclude' => [
                'app/Domain/User/Database/Seeds/DivisionsSeeder.php',
                'app/Domain/User/Database/Migrations/2014_10_12_000000_create_users_table.php',
                'app/Domain/User/Database/Migrations/2019_09_27_160906_create_permission_tables.php',
            ],
        ],
    ],

];
