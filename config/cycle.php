<?php

return [
    'migrations' => [
        'directory' => app()->databasePath() . '/cycle_migrations',  // where to store migrations
        'table' => 'cycle_migrations'                 // database table to store migration status
    ],

    'database' => [
        'default' => 'default',
        'databases' => [
            'default' => ['connection' => env('DB_CONNECTION', 'sqlite')],
        ],
        'connections' => [
            'sqlite' => [
                'driver' => \Spiral\Database\Driver\SQLite\SQLiteDriver::class,
                'connection' => 'sqlite:' . database_path('database.sqlite'),
                'username' => '',
                'password' => '',
            ],
            'mysql' => [
                'driver' => \Spiral\Database\Driver\MySQL\MySQLDriver::class,
                'options' => [
                    'connection' => 'mysql:host=' . env('DB_HOST', '127.0.0.1') . ';dbname=' . env('DB_DATABASE', 'laravel'),
                    'username' => env('DB_USERNAME', 'root'),
                    'password' => env('DB_PASSWORD', ''),
                ]
            ],
            'pgsql' => [
                'driver' => \Spiral\Database\Driver\Postgres\PostgresDriver::class,
                'options' => [
                    'connection' => 'pgsql:host=' . env('DB_HOST', '127.0.0.1') . ';dbname=' . env('DB_DATABASE', 'laravel'),
                    'username' => env('DB_USERNAME', 'root'),
                    'password' => env('DB_PASSWORD', ''),
                ]
            ],
            'sqlServer' => [
                'driver'  => \Spiral\Database\Driver\SQLServer\SQLServerDriver::class,
                'options' => [
                    'connection' => 'sqlsrv:Server=OWNER;Database=DATABASE',
                    'username'   => 'sqlServer',
                    'password'   => 'sqlServer',
                ],
            ],
        ]
    ],

// where to find models, write folders with relative path
    'schema' => [
        'generators' => [
            new Cycle\Schema\Generator\ResetTables(),       // re-declared table schemas (remove columns)
            new Cycle\Annotated\MergeColumns(),             // register non field columns (table level)
            new Cycle\Schema\Generator\GenerateRelations(), // generate entity relations
            new Cycle\Schema\Generator\ValidateEntities(),  // make sure all entity schemas are correct
            new Cycle\Schema\Generator\RenderTables(),      // declare table schemas
            new Cycle\Schema\Generator\RenderRelations(),   // declare relation keys and indexes
            new Cycle\Schema\Generator\GenerateTypecast(),  // typecast non string columns
            new Cycle\Annotated\MergeIndexes(),             // register non entity indexes (table level
        ],

        'path' => ['app/Domain/*/Models']
    ]

];
