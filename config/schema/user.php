<?php

use Cycle\ORM\Mapper\StdMapper;
use Cycle\ORM\Relation;
use Cycle\ORM\Schema;

return [
    Schema::MAPPER      => StdMapper::class,
    Schema::DATABASE    => 'default',
    Schema::TABLE       => 'users',
    Schema::PRIMARY_KEY => 'id',
    Schema::COLUMNS     => [
        'id',
        'email',
        'balance'
    ],
    Schema::TYPECAST    => [
        'id' => 'int',
        'balance' => 'float'
    ],
    Schema::RELATIONS   => [
        'comments' => [
            Relation::TYPE   => Relation::HAS_MANY,
            Relation::TARGET => 'comment',
            Relation::SCHEMA => [
                Relation::CASCADE   => true,
                Relation::INNER_KEY => 'id',
                Relation::OUTER_KEY => 'user_id'
            ],
        ]
    ]
];
