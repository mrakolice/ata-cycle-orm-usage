<?php

use App\Domain\FrontRoute\Database\Seeds\FrontRoutesSeeder;
use Illuminate\Database\Seeder;
use App\Domain\User\Database\Seeds\UserStoriesSeeder;
use App\Domain\Profile\Database\Seeds\ProfilesSeeder;

final class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call([
//             UserStoriesSeeder::class,
         ]);
    }
}
