#!/usr/bin/make

SHELL := /bin/bash # Use bash syntax
DOCKER_COMPOSE_EXEC_APP_NO_TTY := docker-compose exec -T -u docker-user app
DOCKER_COMPOSE_EXEC_APP := docker-compose exec -u docker-user app

list: ## Show available commands list
	@printf "\033[33m%s:\033[0m\n" 'Available commands'
	@awk 'BEGIN {FS = ":.*?## "} /^[a-zA-Z_-]+:.*?## / {printf "  \033[32m%-18s\033[0m %s\n", $$1, $$2}' $(MAKEFILE_LIST)

init: ## Generate .env file, install composer dependencies, setup Laravel
	@$(MAKE) --no-print-directory env
	@$(MAKE) --no-print-directory build
	@$(MAKE) --no-print-directory clean
	@$(MAKE) --no-print-directory up
	@$(MAKE) --no-print-directory ide-helper
	@$(MAKE) --no-print-directory apidoc

docker-install: ## install docker and docker-compose for Ubuntu
	# install docker 19.03
	curl -fsSL https://get.docker.com -o get-docker.sh
	sudo sh get-docker.sh
	sudo usermod -aG docker $(USERNAME)
	#install docker-compose 1.25
	sudo curl -L https://github.com/docker/compose/releases/download/1.25.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
	sudo chmod +x /usr/local/bin/docker-compose

env: ## Make .env file
	@ls $(shell pwd)/.env >> /dev/null 2>&1 || cp .env.example .env
	@sh .docker/scripts/set_docker_env.sh >> /dev/null 2>&1

build: ## Build docker
	@docker-compose build

set-keys: ## Generate application keys
	@${DOCKER_COMPOSE_EXEC_APP_NO_TTY} php artisan key:generate
    @${DOCKER_COMPOSE_EXEC_APP_NO_TTY} php artisan jwt:secret

up: ## Up doker
	@$(MAKE) --no-print-directory bash-history
	@docker-compose up -d --remove-orphans
	@${DOCKER_COMPOSE_EXEC_APP_NO_TTY} composer install
	@${DOCKER_COMPOSE_EXEC_APP_NO_TTY} php vendor/bin/captainhook install -f
	@$(MAKE) --no-print-directory set-keys
	@${DOCKER_COMPOSE_EXEC_APP_NO_TTY} php artisan migrate:fresh --seed

bash-history: ## Generate shell history file clones
	@mkdir -p $(shell pwd)/.docker/app/home/bash
	@mkdir -p $(shell pwd)/.docker/app/home/psysh
	@touch $(shell pwd)/.docker/app/home/bash/.bash_history
	@touch $(shell pwd)/.docker/app/home/psysh/psysh_history

ide-helper: ## Generate PhpStorm helper files
	@docker-compose up -d --remove-orphans >> /dev/null 2>&1
	@${DOCKER_COMPOSE_EXEC_APP_NO_TTY} php artisan ide-helper:eloquent -n
	@${DOCKER_COMPOSE_EXEC_APP_NO_TTY} php artisan ide-helper:meta -n
	@${DOCKER_COMPOSE_EXEC_APP_NO_TTY} php artisan ide-helper:models -Wnr
	@${DOCKER_COMPOSE_EXEC_APP_NO_TTY} php artisan ide-helper:generate -n

githook-ide-helper: ## Generate PhpStorm helper files in container
	@php artisan ide-helper:eloquent -n
	@php artisan ide-helper:meta -n
	@php artisan ide-helper:models -Wnr
	@php artisan ide-helper:generate -n

log-rights: ## create necessary rights for logs
	sudo chmod -R 777 storage/logs

apidoc: ## Generate PhpStorm helper files
	@${DOCKER_COMPOSE_EXEC_APP_NO_TTY} php artisan docs

tinker: ## alias to tinker
	@${DOCKER_COMPOSE_EXEC_APP} php artisan tinker

art: ## alias to artisan. Should be used with variable CMD. make art CMD=routes:list
	@${DOCKER_COMPOSE_EXEC_APP} php artisan ${CMD}

app: ## alias to go to app
	@${DOCKER_COMPOSE_EXEC_APP}

clean: ## stop all docker containers, delete all networks, volumes and stopped containers
	@[[ $$(docker ps -aq) ]] \
	&& printf "\033[33m%s:\033[0m\n" 'Stop all containers' \
	&& docker stop $$(docker ps -aq) \
	&& docker system prune -f \
	|| printf "Nothing to clean...\n"
