<?php

declare(strict_types=1);

namespace App\Infrastructure\Contracts;

interface QueryBuilderContract
{
    public function getAllowedFields (): array;

    public function getAllowedIncludes (): array;

    public function getAllowedFilters (): array;

    public function getDefaultSorts (): string;

    public function getAllowedSorts (): array;
}
