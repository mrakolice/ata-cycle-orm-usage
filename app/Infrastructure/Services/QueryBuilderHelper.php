<?php

namespace App\Infrastructure\Services;

use App\Core\Exceptions\ModelCannotUseQueryBuilderException;
use App\Infrastructure\Contracts\QueryBuilderContract;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOneThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Eloquent\Relations\MorphToMany;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Spatie\QueryBuilder\QueryBuilderRequest;

class QueryBuilderHelper
{
    /**
     * @var Builder
     */
    protected $query;

    /**
     * @var Collection
     */
    protected $requestFields;

    /**
     * @var Request
     */
    protected $request;
    /**
     * @var Collection
     */
    protected $parsedAllowedFields;

    public function __construct(Builder $query, ?Request $request = null)
    {
        $this->query = $query;
        $this->request = QueryBuilderRequest::fromRequest($request ?? request());

        $this->parsedAllowedFields = collect();


        $model = $this->query->getModel();
        if (!$model instanceof QueryBuilderContract) {
            throw new ModelCannotUseQueryBuilderException();
        }

        if ($this->request->fields()) {
            $this->requestFields = $this->request->fields();
            if ($this->request->fields()->has($model->getTable())) {
                $this->parsedAllowedFields = $this->parsedAllowedFields
                    ->merge($model->getAllowedFields());
            }
            $this->parseFields($this->makeTree());
        }

        $this->parsedAllowedFields = $this->parsedAllowedFields->unique()->values();

        $this->request->query->replace(collect($this->request->query())
            ->replace([
                config('query-builder.parameters.fields') => $this->requestFields
                    ->transform(function ($fields) {
                        $fields = collect($fields)->unique()->values()->all();
                        return implode(',', $fields);
                    })
                    ->all(),
            ])->all()
        );
    }

    public function parseFields($nodes, $ancestors = []): void
    {
        foreach ($nodes as $relation => $children) {
            if (
                $this->isRelationInFields(Str::snake($relation)) &&
                $this->isRelationInIncludes(Str::snake($relation))
            ) {
                $relationModel = $this->getRelation($this->query->getModel(), $relation, $ancestors)->getRelated();
                $relationAllowedFields = $relationModel instanceof QueryBuilderContract ? $relationModel
                    ->getAllowedFields() : [];
                $relationAllowedFields = collect($relationAllowedFields)->transform(function ($field) use ($relation) {
                    return implode('.', [Str::snake($relation), $field]);
                });
                $this->parsedAllowedFields = $this->parsedAllowedFields->merge($relationAllowedFields);
            }
            if ($this->isRelationInIncludes(Str::snake($relation))) {
                $this->parseRelationAllowedFields($this->query->getModel(), $relation, $ancestors);
            }
            if ($children) {
                $this->parseFields($children, array_merge($ancestors, [$relation]));
            }
        }
    }

    protected function relationAllowedFieldsMap($pathToSelf, $pathToRelation, $relationInstance)
    {
        return [
            BelongsTo::class => [
                $this->makePathToField($pathToSelf, $relationInstance->getForeignKeyName()),
                $this->makePathToField($pathToRelation, $relationInstance->getOwnerKeyName()),
            ],

            BelongsToMany::class => [
                $this->makePathToField($pathToSelf, $relationInstance->getParentKeyName()),
                $this->makePathToField($pathToRelation, $relationInstance->getRelatedKeyName()),
            ],

            HasMany::class => [
                $this->makePathToField($pathToSelf, $relationInstance->getLocalKeyName()),
                $this->makePathToField($pathToRelation, $relationInstance->getForeignKeyName()),
            ],

            HasManyThrough::class => [
                $this->makePathToField($pathToSelf, $relationInstance->getLocalKeyName()),
                $this->makePathToField($pathToRelation, $relationInstance->getForeignKeyName()),
            ],

            HasOne::class => [
                $this->makePathToField($pathToSelf, $relationInstance->getLocalKeyName()),
                $this->makePathToField($pathToRelation, $relationInstance->getForeignKeyName()),
            ],

            HasOneThrough::class => [
                $this->makePathToField($pathToSelf, $relationInstance->getLocalKeyName()),
                $this->makePathToField(
                    $relationInstance->getRelated()->getTable(),
                    $relationInstance->getForeignKeyName()
                ),
            ],

            MorphMany::class => [
                $this->makePathToField($pathToSelf, $relationInstance->getLocalKeyName()),
                $this->makePathToField($pathToRelation, $relationInstance->getMorphType()),
                $this->makePathToField($pathToRelation, $relationInstance->getForeignKeyName()),
            ],

            MorphOne::class => [
                $this->makePathToField($pathToSelf, $relationInstance->getLocalKeyName()),
                $this->makePathToField($pathToRelation, $relationInstance->getMorphType()),
                $this->makePathToField($pathToRelation, $relationInstance->getForeignKeyName()),
            ],

            MorphToMany::class => [
                $this->makePathToField($pathToSelf, $relationInstance->getParentKeyName()),
                $this->makePathToField(
                    $relationInstance->getRelated()->getTable(),
                    $relationInstance->getRelatedKeyName()
                ),
            ],

            MorphTo::class => [
                $this->makePathToField($pathToSelf, $relationInstance->getMorphType()),
                $this->makePathToField($pathToSelf, $relationInstance->getForeignKeyName()),
            ],
        ];
    }

    protected function relationRequestFieldsMap($ownerEntryName, $relationEntryName, $relationInstance)
    {
        $ownerEntryName = Str::snake($ownerEntryName);
        $relationEntryName = Str::snake($relationEntryName);
        return [
            BelongsTo::class => [
                $ownerEntryName => [$relationInstance->getForeignKeyName()],
                $relationEntryName => [$relationInstance->getOwnerKeyName()],
            ],

            BelongsToMany::class => [
                $ownerEntryName => [$relationInstance->getParentKeyName()],
                $relationEntryName => [$relationInstance->getRelatedKeyName()],
            ],

            HasMany::class => [
                $ownerEntryName => [$relationInstance->getLocalKeyName()],
                $relationEntryName => [$relationInstance->getForeignKeyName()],
            ],

            HasManyThrough::class => [
                $ownerEntryName => [$relationInstance->getLocalKeyName()],
                $relationEntryName => [$relationInstance->getForeignKeyName()],
            ],

            HasOne::class => [
                $ownerEntryName => [$relationInstance->getLocalKeyName()],
                $relationEntryName => [$relationInstance->getForeignKeyName()],
            ],

            HasOneThrough::class => [
                $ownerEntryName => [$relationInstance->getLocalKeyName()],
                $relationEntryName => [
                    $this->makePathToField(
                        $relationInstance->getRelated()->getTable(),
                        $relationInstance->getForeignKeyName()
                    ),
                ],
            ],

            MorphMany::class => [
                $ownerEntryName => [$relationInstance->getLocalKeyName()],
                $relationEntryName => [
                    $relationInstance->getMorphType(),
                    $relationInstance->getForeignKeyName()
                ],
            ],

            MorphOne::class => [
                $ownerEntryName => [$relationInstance->getLocalKeyName()],
                $relationEntryName => [
                    $relationInstance->getMorphType(),
                    $relationInstance->getForeignKeyName()
                ],
            ],

            MorphToMany::class => [
                $ownerEntryName => [$relationInstance->getParentKeyName()],
                $relationEntryName => [
                    $this->makePathToField(
                        $relationInstance->getRelated()->getTable(),
                        $relationInstance->getRelatedKeyName()
                    )
                ],
            ],

            MorphTo::class => [
                $ownerEntryName => [
                    $relationInstance->getMorphType(),
                    $relationInstance->getForeignKeyName()
                ],
            ],
        ];
    }

    protected function parseRelationAllowedFields($model, $relationEntryName, $ancestors = []): void
    {
        $relationInstance = $this->getRelation($model, $relationEntryName, $ancestors);
        $ownerEntryName = $ancestors ? end($ancestors) : $model->getTable();

        /** @var Relation $relationInstance */
        $relationClass = get_class($relationInstance);

        $pathToSelf = $ancestors ? Str::snake(implode('.', $ancestors)) : '';
        $pathToRelation = Str::snake(implode('.', array_merge($ancestors, [$relationEntryName])));

        $allowedFields = $this->relationAllowedFieldsMap($pathToSelf, $pathToRelation, $relationInstance)[$relationClass];

        $requestFields = $this->relationRequestFieldsMap($ownerEntryName, $relationEntryName, $relationInstance)[$relationClass];

        if (in_array($relationClass, [HasManyThrough::class, HasOneThrough::class])) {
            $this->requestFields = $this->requestFields->replace([
                Str::snake($relationEntryName) => collect($this->request->fields()
                    ->get(Str::snake($relationEntryName)))
                    ->transform(function ($field) use ($relationInstance) {
                        return $this->makePathToField(
                            $relationInstance->getRelated()->getTable(),
                            $field
                        );
                    })->toArray(),
            ]);
        }

        if (in_array($relationClass, [MorphToMany::class, MorphTo::class]) && $this->isRelationInFields($relationEntryName)) {
            abort(400, 'Fields for relation' . $relationEntryName . 'is not acceptable');
        }


        if ($allowedFields) {
            $this->parsedAllowedFields = $this->parsedAllowedFields->merge($allowedFields);
        }
        if ($requestFields) {
            foreach ($requestFields as $relationEntryName => $fields) {
                if ($this->isRelationInFields($relationEntryName)) {
                    $this->requestFields = $this->requestFields->mergeRecursive([
                        $relationEntryName => $fields
                    ]);
                }
            }
        }
    }

    protected function makePathToField(string $path, string $field): string
    {
        return ltrim(implode('.', [$path, $field]), '.');
    }

    protected function getRelation($model, $relationName, $ancestors = []): Relation
    {
        if (!$ancestors) {
            return $model->$relationName();
        }
        $lastAncestor = array_pop($ancestors);
        /** @var Relation $relation */
        $relation = $model->$lastAncestor();

        return $this->getRelation($relation->getRelated(), $relationName, $ancestors);
    }

    public function getParsedFields()
    {
        return $this->parsedAllowedFields->all();
    }

    public function getRequestFields()
    {
        return $this->requestFields->all();
    }

    public function getRequest()
    {
        return $this->request;
    }

    protected function makeTree(): array
    {
        $includesTree = [];
        $this->request->includes()->each(function (string $relationChain) use (&$includesTree) {
            $relationChain = explode('.', $relationChain);
            $includesTree = $this->getNodes($relationChain, $includesTree);
        });

        return $includesTree;
    }

    protected function getNodes($relationChain, $includesTree)
    {
        $relation = array_shift($relationChain);
        if (!array_key_exists($relation, $includesTree)) {
            $includesTree[$relation] = [];
        }
        if (count($relationChain)) {
            $includesTree[$relation] = $this->getNodes($relationChain, $includesTree[$relation]);
        }

        return $includesTree;
    }

    protected function isRelationInFields($relationName): bool
    {
        return $this->request->fields()->has(Str::snake($relationName));
    }

    protected function isRelationInIncludes($relationName): bool
    {
        return !is_null($this->request->includes()->first(function ($relationChain) use ($relationName) {
            return Str::contains($relationChain, $relationName);
        }));
    }
}
