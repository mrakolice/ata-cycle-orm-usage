<?php

namespace App\Infrastructure\Traits\Models;

trait UseQueryBuilder
{
    public function getAllowedFields(): array
    {
        return $this->allowedFields ?? [];
    }

    public function getAllowedIncludes(): array
    {
        return $this->allowedIncludes ?? [];
    }

    public function getAllowedFilters(): array
    {
        return $this->allowedFilters ?? [];
    }

    public function getDefaultSorts(): string
    {
        return $this->defaultSorts ?? '';
    }

    public function getAllowedSorts(): array
    {
        return $this->allowedSorts ?? [];
    }
}
