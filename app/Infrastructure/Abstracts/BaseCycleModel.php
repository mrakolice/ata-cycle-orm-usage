<?php


namespace App\Infrastructure\Abstracts;

use Cycle\Annotated\Annotation\Column;
use Cycle\Annotated\Annotation\Entity;
use Cycle\ORM\Select\Repository;
use Cycle\ORM\Transaction;

abstract class BaseCycleModel
{
    /** @Column(type= "primary") */
    public $id;

    public static function orm(): Repository{
        return app()['cycle-db']->getRepository(get_called_class());
    }

    public function __construct(array $array = [])
    {
        foreach ($array as $key => $value){
            $this->{$key} = $value;
        }
    }

    public function save($mode=Transaction::MODE_CASCADE) {
        $transaction = app()['cycle-db.transaction'];
        $transaction->persist($this, $mode);
        $transaction->run();
    }

    public function delete(){
        $transaction = app()['cycle-db.transaction'];
        $transaction->delete($this);
        $transaction->run();
    }
}
