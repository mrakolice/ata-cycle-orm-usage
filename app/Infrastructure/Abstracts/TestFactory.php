<?php

declare(strict_types=1);

namespace App\Infrastructure\Abstracts;

use Faker;
use Faker\Generator;
use Illuminate\Database\Eloquent\Factory;

/**
 * Class ModelFactory.
 * Base Factory for usage inside domains.
 */
abstract class TestFactory
{
    /**
     * @var Factory
     */
    protected $factory;

    /**
     * @var string
     */
    protected $model;

    /**
     * @var \Faker\Generator
     */
    protected $faker;

    /**
     * BaseFactory constructor.
     */
    public function __construct()
    {
        $this->faker = Faker\Factory::create(config('app.faker_locale'));
        $this->factory = new Factory($this->faker);
    }

    public function define()
    {
        $this->states();

        $this->factory->define($this->model, array($this, 'fields'));
    }

    abstract public function states();

    /**
     * @return array
     *
     * @throws \Exception
     */
    abstract public function fields();
}
