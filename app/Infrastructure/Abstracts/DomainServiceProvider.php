<?php

declare(strict_types=1);

namespace App\Infrastructure\Abstracts;

use App\Infrastructure\Abstracts\DomainServiceProviderTraits\RegisterCommands;
use App\Infrastructure\Abstracts\DomainServiceProviderTraits\RegisterFactories;
use App\Infrastructure\Abstracts\DomainServiceProviderTraits\RegisterMigrations;
use App\Infrastructure\Abstracts\DomainServiceProviderTraits\RegisterTranslations;
use Illuminate\Support\ServiceProvider as LaravelServiceProvider;
use App\Infrastructure\Abstracts\DomainServiceProviderTraits\RegisterPolicies;
use App\Infrastructure\Abstracts\DomainServiceProviderTraits\RegisterProviders;

abstract class DomainServiceProvider extends LaravelServiceProvider
{
    use RegisterPolicies;
    use RegisterCommands;
    use RegisterFactories;
    use RegisterProviders;
    use RegisterMigrations;
    use RegisterTranslations;

    /**
     * Boot required registering of views and translations.
     *
     * @throws \ReflectionException
     */
    public function boot()
    {
        $this->registerPolicies();
        $this->registerCommands();
        $this->registerFactories();
        $this->registerMigrations();
        $this->registerTranslations();
    }

    /**
     * Detects the domain base path so resources can be proper loaded on child classes.
     *
     * @param string $append
     *
     * @return string
     *
     * @throws \ReflectionException
     */
    protected function domainPath($append = null)
    {
        $reflection = new \ReflectionClass($this);

        $realPath = realpath(dirname($reflection->getFileName()) . '/../');

        if (! $append) {
            return $realPath;
        }

        return $realPath . '/' . $append;
    }

}
