<?php

declare(strict_types=1);

namespace App\Infrastructure\Abstracts;

use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Database\Eloquent\Model;
use App\Infrastructure\Contracts\BaseRepository;

abstract class EloquentRepository implements BaseRepository
{
    /**
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model;
    /**
     * @var \Spatie\QueryBuilder\QueryBuilder
     */
    protected $query;

    protected $withoutGlobalScopes = false;

    protected $with = [];

    protected $defaultSelect = [];
    protected $defaultSort = ['created_at'];

    protected $allowedFields = [];
    protected $allowedFilters = [];
    protected $allowedIncludes = [];
    protected $allowedSorts = [];

    public function __construct(Model $model)
    {
        $this->query = QueryBuilder::for($model);
    }

    public function with(array $with = []): BaseRepository
    {
        $this->query = $this->query->with(func_get_args());

        return $this;
    }

    public function withoutGlobalScopes(): BaseRepository
    {
        $this->withoutGlobalScopes = true;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function store(array $data): Model
    {
        return $this->model->create($data);
    }

    /**
     * {@inheritdoc}
     */
    public function update(Model $model, array $data): Model
    {
        return tap($model)->update($data);
    }

    public function find($id, ?array $columns = null)
    {
        $query = $this->baseQuery();

        return $query->find($id, $columns);
    }

    public function baseQuery(): QueryBuilder
    {
        return QueryBuilder::for($this->model)
            ->when((bool) $this->defaultSelect, static function (QueryBuilder $query) {
                return $query->select($this->defaultSelect);
            })
            ->when((bool) $this->with, static function (QueryBuilder $query) {
                return $query->with($this->with);
            })
            ->when($this->withoutGlobalScopes, static function (QueryBuilder $query) {
                return $query->withoutGlobalScopes();
            });
    }

    public function filteredQuery()
    {
        return $this->baseQuery()
            ->when($this->allowedFields, static function (QueryBuilder $query) {
                return $query->allowedFields($this->allowedFields);
            })
            ->when($this->allowedFilters, static function (QueryBuilder $query) {
                return $query->allowedFilters($this->allowedFilters);
            })
            ->when($this->allowedIncludes, static function (QueryBuilder $query) {
                return $query->allowedIncludes($this->allowedIncludes);
            })
            ->when($this->allowedSorts, static function (QueryBuilder $query) {
                return $query->allowedSorts($this->allowedSorts);
            })
            ->when($this->defaultSort, static function (QueryBuilder $query) {
                return $query->defaultSort($this->defaultSort);
            })
            ->when($this->getPerPage(), static function (QueryBuilder $query) {
                return $query->paginate($this->getPerPage());
            });
    }

    static protected function getPerPage(): int
    {
        /** @noinspection PhpStrictTypeCheckingInspection */
        return (int) (request()->per_page ?? request()->limit);
    }
}
