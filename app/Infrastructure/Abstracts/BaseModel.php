<?php

declare(strict_types=1);

namespace App\Infrastructure\Abstracts;

use App\Infrastructure\Contracts\QueryBuilderContract;
use App\Infrastructure\Traits\Models\UseQueryBuilder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;

abstract class BaseModel extends Model implements QueryBuilderContract
{
    use SoftDeletes;
    use UseQueryBuilder;

    protected $guard_name = 'api';
    protected static $logOnlyDirty = true;
    protected static $logAttributes = ['*'];
    protected static $logName = 'models_tracking';

    public $defaultSorts = 'id';
}
