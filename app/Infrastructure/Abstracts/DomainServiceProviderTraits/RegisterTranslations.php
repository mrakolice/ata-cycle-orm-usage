<?php

declare(strict_types=1);

namespace App\Infrastructure\Abstracts\DomainServiceProviderTraits;

trait RegisterTranslations
{
    /**
     * @var bool Set if will load translations
     */
    protected $hasTranslations = false;

    /**
     * Register domain translations.
     */
    protected function registerTranslations()
    {
        if ($this->hasTranslations) {
            $this->loadJsonTranslationsFrom($this->domainPath('Resources/Lang'));
        }
    }
}
