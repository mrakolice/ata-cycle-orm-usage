<?php

declare(strict_types=1);

namespace App\Infrastructure\Abstracts\DomainServiceProviderTraits;

trait RegisterCommands
{
    /**
     * @var array List of custom Artisan commands
     */
    protected $commands = [];

    /**
     * Register domain custom Artisan commands.
     */
    protected function registerCommands()
    {
        if ((bool) $this->commands) {
            $this->commands($this->commands);
        }
    }
}
