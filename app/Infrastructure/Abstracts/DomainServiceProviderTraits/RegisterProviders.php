<?php

declare(strict_types=1);

namespace App\Infrastructure\Abstracts\DomainServiceProviderTraits;

trait RegisterProviders
{
    /**
     * @var array<string> List of providers to load
     */
    protected $providers = [];

    /**
     * Register Domain ServiceProviders.
     */
    public function register()
    {
        collect($this->providers)->each(function ($providerClass) {
            $this->app->register($providerClass);
        });
    }
}
