<?php

declare(strict_types=1);

namespace App\Infrastructure\Abstracts\DomainServiceProviderTraits;

trait RegisterFactories
{
    /**
     * @var array List of model factories to load
     */
    protected $factories = [];

    /**
     * Register BaseModel Factories.
     */
    protected function registerFactories()
    {
//        dump('asdf');
        if ((bool) $this->factories) {
            collect($this->factories)->each(static function ($factoryName) {
                (new $factoryName())->define();
            });
        }
    }
}
