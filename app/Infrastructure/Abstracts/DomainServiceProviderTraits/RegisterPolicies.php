<?php

declare(strict_types=1);

namespace App\Infrastructure\Abstracts\DomainServiceProviderTraits;

use Illuminate\Support\Facades\Gate;

trait RegisterPolicies
{
    /**
     * @var array List of policies to load
     */
    protected $policies = [];

    /**
     * Register the application's policies.
     *
     * @return void
     */
    public function registerPolicies()
    {
        collect($this->policies)->each(static function($value, $key) {
            Gate::policy($key, $value);
        });
    }
}
