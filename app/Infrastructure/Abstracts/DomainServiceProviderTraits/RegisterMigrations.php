<?php

declare(strict_types=1);

namespace App\Infrastructure\Abstracts\DomainServiceProviderTraits;

trait RegisterMigrations
{
    /**
     * @var bool Set if will load migrations
     */
    protected $hasMigrations = false;

    /**
     * Register domain migrations.
     *
     * @throws \ReflectionException
     */
    protected function registerMigrations()
    {
        if ($this->hasMigrations) {
            $this->loadMigrationsFrom($this->domainPath('Database/Migrations'));
        }
    }
}
