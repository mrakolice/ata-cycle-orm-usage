<?php


namespace App\Infrastructure\Abstracts\ModelTraits;


use Cycle\Annotated\Annotation\Column;

trait SoftDeleted
{
    /** @Column(type= "datetime", nullable=true) */
    public $deleted_at;
}
