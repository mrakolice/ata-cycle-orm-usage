<?php


namespace App\Infrastructure\Abstracts\ModelTraits;


use Cycle\Annotated\Annotation\Column;

trait Timestamps
{
    /** @Column(type= "datetime", nullable=true) */
    public $created_at;

    /** @Column(type= "datetime", nullable=true) */
    public $updated_at;
}
