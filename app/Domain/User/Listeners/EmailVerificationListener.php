<?php

namespace App\Domain\User\Listeners;

use App\Domain\User\Jobs\SendEmailsJob;
use App\Domain\User\Events\CreateUserEvent;

class EmailVerificationListener
{
    /**
     * Handle the event.
     *
     * @param CreateUserEvent $event
     * @return void
     */
    public function handle(CreateUserEvent $event)
    {
        //
    }
}
