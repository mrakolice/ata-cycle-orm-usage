<?php

declare(strict_types=1);

namespace App\Domain\User\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class ReregistrationRequest extends FormRequest
{
    public function attributes()
    {
        return [
            'login' => 'логин',
        ];
    }

    /**1
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'id' => 'numeric',
            'login' => [
                'required',
                'string',
                'unique:users',
            ],
            'email' => [
                'required',
                'email',
                'unique:users',
            ],
            'phone_number' => [
                'required',
                'string',
                'unique:users',
                'min:11',
                'max:12',
            ],
            'password' => [
                'required',
                'confirmed',
                'min:6',
            ],
            'first_name' => [
                'required',
                'string',
                'regex:/[А-Яа-яЁё]/u',
            ],
            'middle_name' => [
                'string',
                'regex:/[А-Яа-яЁё]/u',
            ],
            'last_name' => [
                'required',
                'string',
                'regex:/[А-Яа-яЁё]/u',
            ],
            'avatar' => 'string',
            'birthday' => [
                'required',
                'date_format:Y-m-d',
                'size:10',
            ],
            'contacts' => 'string',
            'payment_accounts' => [
                'string',
                'min:6',
            ],
            'timezone' => 'integer',
            'description' => 'string',
            'currency_id' => [
                'numeric',
                'exists:currencies',
            ],
            'department_id' => [
                'required',
                'numeric',
                'exists:departments,id',
            ],
            'email_verified_at' => 'date',
            'is_verified' => 'boolean',
            'author_pseudonym' => 'string',
        ];
    }
}
