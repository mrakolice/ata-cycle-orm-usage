<?php

declare(strict_types=1);

namespace App\Domain\User\Requests\Registration;

use Illuminate\Foundation\Http\FormRequest;

class RegistrationRequest extends FormRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'id' => 'numeric',
            'login' => [
                'required',
                'string',
                'unique:users',
            ],
            'email' => [
                'required',
                'email',
                'unique:users',
            ],
            'phone_number' => [
                'required',
                'string',
                'unique:users',
                'min:11',
                'max:12',
            ],
            'password' => [
                'required',
                'confirmed',
                'min:6',
            ],
            'first_name' => [
                'required',
                'string',
                'regex:/[А-Яа-яЁё]/u',
            ],
            'middle_name' => [
                'string',
                'regex:/[А-Яа-яЁё]/u',
            ],
            'last_name' => [
                'required',
                'string',
                'regex:/[А-Яа-яЁё]/u',
            ],
            'avatar' => 'string',
            'birthday' => [
                'required',
                'date_format:Y-m-d',
                'size:10',
            ],
            'contacts' => 'string',
            'payment_accounts' => [
                'string',
                'min:6',
            ],
            'timezone' => 'integer',
            'description' => 'string',
            'currency_id' => [
                'numeric',
                'exists:currencies',
            ],
            'department_id' => 'numeric',
            'author_pseudonym' => 'string',
            'role_id' => 'numeric',
            'about' => 'string',
            'university_name' => 'string',
            'university_department' => 'string',
            'education_started_at' => 'date_format:Y-m-d',
            'education_ended_at' => 'date_format:Y-m-d',
            'mode' => 'string',
            'days' => 'array',
            'hours' => 'array',
            'work_place_name' => 'string',
            'position' => 'string',
            'work_place_started_at' => 'date_format:Y-m-d',
            'work_place_ended_at' => 'date_format:Y-m-d',
        ];
    }
}
