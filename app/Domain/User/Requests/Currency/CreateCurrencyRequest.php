<?php

declare(strict_types=1);

namespace App\Domain\User\Requests\Currency;

use Illuminate\Foundation\Http\FormRequest;

class   CreateCurrencyRequest extends FormRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'id' => 'numeric',
            'name' => [
                'required',
                'string',
                'max:15',
            ],
            'code' => [
                'required',
                'string',
                'size:3',
            ],
            'unicode_sign' => [
                'required',
                'string',
                'min:3',
                'size:8',
            ],
            'html_sign' => [
                'required',
                'string',
                'size:7',
            ],
        ];
    }
}
