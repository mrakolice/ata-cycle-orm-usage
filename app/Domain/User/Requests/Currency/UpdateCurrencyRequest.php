<?php

declare(strict_types=1);

namespace App\Domain\User\Requests\Currency;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCurrencyRequest extends FormRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'id' => 'numeric',
            'name' => [
                'string',
                'max:15',
            ],
            'code' => [
                'string',
                'size:3',
            ],
            'unicode_sign' => [
                'string',
                'min:3',
                'size:8',
            ],
            'html_sign' => [
                'string',
                'size:7',
            ],
        ];
    }
}
