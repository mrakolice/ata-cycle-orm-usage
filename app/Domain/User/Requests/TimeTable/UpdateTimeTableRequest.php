<?php

declare(strict_types=1);

namespace App\Domain\User\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTimeTableRequest extends FormRequest
{

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'id' => 'numeric',
            'owner_id' => 'numeric',
            'work_day' => 'date_format:Y-m-d',
            'work_hours' => 'integer',
            'owner_comment' => 'string',
            'editor_comment' => 'string',
        ];
    }
}
