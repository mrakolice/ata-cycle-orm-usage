<?php

declare(strict_types=1);

namespace App\Domain\User\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateTimeTableRequest extends FormRequest
{

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'id' => 'numeric',
            'owner_id' => [
                'required',
                'numeric'
            ],
            'work_day' => [
                'required',
                'date_format:Y-m-d',
            ],
            'work_hours' => [
                'required',
                'integer',
            ],
            'owner_comment' => 'string',
            'editor_comment' => 'string',
        ];
    }
}
