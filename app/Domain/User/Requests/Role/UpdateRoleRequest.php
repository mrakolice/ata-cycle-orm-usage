<?php

declare(strict_types=1);

namespace App\Domain\User\Requests\Role;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRoleRequest extends FormRequest
{

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'name' => [
                'string',
                'max:255',
            ],
        ];
    }
}
