<?php

declare(strict_types=1);

namespace App\Domain\User\Requests\User;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{

    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'id' => 'numeric',
            'login' => [
                'string',
                Rule::unique('users')->ignore(request()->route('user'), 'id')
            ],
            'email' => [
                'email',
                Rule::unique('users')->ignore(request()->route('user'), 'id')
            ],
            'phone_number' => [
                'string',
                'min:11',
                'max:12',
                Rule::unique('users')->ignore(request()->route('user'), 'id')
            ],
            'password' => [
                'confirmed',
                'min:6',
            ],
            'first_name' => [
                'string',
                'regex:/[А-Яа-яЁё]/u',
            ],
            'middle_name' => [
                'string',
                'regex:/[А-Яа-яЁё]/u',
            ],
            'last_name' => [
                'string',
                'regex:/[А-Яа-яЁё]/u',
            ],
            'avatar' => 'string',
            'birthday' => [
                'string',
                'size:10',
            ],
            'contacts' => 'string',
            'payment_accounts' => [
                'string',
                'min:6',
            ],
            'timezone' => 'integer',
            'description' => 'string',
            'currency_id' => [
                Rule::exists('currencies'),
                'numeric',
            ],
            'department_id' => [
                'numeric',
                Rule::exists('departments'),
            ],
            'email_verified_at' => 'date',
            'is_verified' => 'boolean',
            'author_pseudonym' => 'string',
        ];
    }
}
