<?php

declare(strict_types=1);

namespace App\Domain\User\Requests\Department;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateDepartmentRequest extends FormRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'id' => 'numeric',
            'name' => [
                'string',
                'max:50',
            ],
            'division_id' => [
                Rule::exists('divisions'),
                'numeric',
            ]
        ];
    }
}
