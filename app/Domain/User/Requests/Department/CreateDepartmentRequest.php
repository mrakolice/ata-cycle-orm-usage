<?php

declare(strict_types=1);

namespace App\Domain\User\Requests\Department;

use Illuminate\Foundation\Http\FormRequest;

class CreateDepartmentRequest extends FormRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'id' => 'numeric',
            'name' => [
                'required',
                'string',
                'max:50',
            ],
            'division_id' => [
                'required',
                'exists:divisions',
                'numeric',
            ],
        ];
    }
}
