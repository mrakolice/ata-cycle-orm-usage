<?php

declare(strict_types=1);

namespace App\Domain\User\Requests\Transaction;

use Illuminate\Foundation\Http\FormRequest;

class CreateTransactionRequest extends FormRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'id' => 'numeric',
            'sender_id' => [
                'required',
                'numeric',
            ],
            'getter_id' => [
                'required',
                'numeric',
            ],
            'status' => [
                'required',
                'array',
            ],
            'amount' => [
                'required',
                'string',
            ],
        ];
    }
}
