<?php

declare(strict_types=1);

namespace App\Domain\User\Requests\Transaction;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTransactionRequest extends FormRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'id' => 'numeric',
            'sender_id' => 'numeric',
            'getter_id' => 'numeric',
            'status' => 'array',
            'amount' => 'string',
        ];
    }
}
