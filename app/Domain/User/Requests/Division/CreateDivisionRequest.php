<?php

declare(strict_types=1);

namespace App\Domain\User\Requests\Division;

use Illuminate\Foundation\Http\FormRequest;

class CreateDivisionRequest extends FormRequest
{
    /**
     * @return string[]
     */
    public function rules(): array
    {
        return [
            'id' => 'numeric',
            'name' => [
                'required',
                'string',
                'max:50',
            ]
        ];
    }
}
