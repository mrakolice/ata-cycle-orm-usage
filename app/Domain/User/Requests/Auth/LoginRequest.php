<?php

declare(strict_types=1);

namespace App\Http\Requests\Auth;

use Illuminate\Foundation\Http\FormRequest;

class LoginRequest extends FormRequest
{
    /** @var string[][]  */
    private static $rules = [
        'email' => [
            'required_without_all:username,login',
            'string',
            'email',
        ],
        'username' => [
            'required_without_all:email,login',
            'string',
        ],
        'login' => [
            'required_without_all:email,username',
            'string',
        ],
        'password' => [
            'required',
            'string',
            'min:6',
        ],
    ];

    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return string[][]
     */
    public function rules(): array
    {
        return static::$rules;
    }
}
