<?php

declare(strict_types=1);

namespace App\Domain\User\Models;

use App\Core\Support\QueryBuilder\Filters\DeletedModelFilter;
use App\Domain\User\Database\Scopes\UserDefaultScope;
use App\Domain\User\Models\Traits\JWT;
use App\Infrastructure\Abstracts\BaseModel;
use Illuminate\Auth\{Authenticatable, MustVerifyEmail, Passwords\CanResetPassword};
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Contracts\Auth\{Authenticatable as AuthenticatableContract, MustVerifyEmail as MustVerifyEmailContract};
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\Access\Authorizable as AuthorizableAccess;
use Illuminate\Notifications\Notifiable;
use Spatie\Activitylog\Traits\CausesActivity;
use Spatie\MediaLibrary\HasMedia\{HasMedia, HasMediaTrait};
use Spatie\Permission\Traits\HasRoles;
use Spatie\QueryBuilder\AllowedFilter;
use Tymon\JWTAuth\Contracts\JWTSubject;

class OldUser extends BaseModel implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract,
    HasMedia,
    JWTSubject,
    MustVerifyEmailContract
{
    use Authenticatable;
    use AuthorizableAccess;
    use CanResetPassword;
    use CausesActivity;
    use HasMediaTrait;
    use HasRoles;
    use JWT;
    use MustVerifyEmail;
    use Notifiable;
    use SoftDeletes;

    protected static $logAttributesToIgnore = ['password', 'remember_token'];

    protected $attributes = [
        'currency_id' => 1,
    ];

//    protected $with = ['roles.permissions', 'department.division'];

    public function __construct(array $attributes = [])
    {
        $this->allowedFilters = [
            AllowedFilter::exact('id'),
            'login',
            'email',
            'phone_number',
            'password',
            'first_name',
            'middle_name',
            'last_name',
            'avatar',
            'birthday',
            'contacts',
            'payment_accounts',
            'timezone',
            'description',
            'currency_id',
            'department_id',
            'email_verified_at',
            'is_verified',
            'author_pseudonym',
            'created_at',
            'updated_at',
            'deleted_at',
            AllowedFilter::custom('deleted', new DeletedModelFilter),
        ];

        parent::__construct($attributes);
    }

    protected $fillable = [
        'login',
        'email',
        'phone_number',
        'password',
        'first_name',
        'middle_name',
        'last_name',
        'avatar',
        'birthday',
        'contacts',
        'payment_accounts',
        'timezone',
        'description',
        'currency_id',
        'department_id',
        'email_verified_at',
        'is_verified',
        'author_pseudonym',
    ];

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
        'birthday' => 'date',
        'email_verified_at' => 'datetime',
        'contacts' => 'object',
        'payment_accounts' => 'object',
        'is_verified' => 'boolean',
        'currency_id' => 'integer',
        'department_id' => 'integer',
    ];

    public $allowedFields = [
        'id',
        'login',
        'email',
        'phone_number',
        'password',
        'first_name',
        'middle_name',
        'last_name',
        'avatar',
        'birthday',
        'contacts',
        'payment_accounts',
        'timezone',
        'description',
        'email_verified_at',
        'is_verified',
        'author_pseudonym',
    ];

    public $allowedIncludes = [
        'roles',
        'roles.permissions',
        'currency',
        'department',
        'department.division',
        'profile',
        'profile.education',
        'profile.schedule',
        'profile.work_places',
    ];

    public $allowedFilters = [];


    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope(new UserDefaultScope);
    }


    public function setPasswordAttribute(string $value): void
    {
        $value = bcrypt($value);
        $value = preg_replace('/\$2.\$/', '\$2a\$', $value);
        $this->attributes['password'] = $value;
    }

    public function currency()
    {
        return $this->belongsTo(Currency::class);
    }

    public function department()
    {
        return $this->belongsTo(Department::class);
    }



}
