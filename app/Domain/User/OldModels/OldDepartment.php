<?php

declare(strict_types=1);

namespace App\Domain\User\Models;

use App\Core\Support\QueryBuilder\Filters\DeletedModelFilter;
use App\Infrastructure\Abstracts\BaseModel;
use Spatie\QueryBuilder\AllowedFilter;

final /**
 * App\Domain\User\OldModels\Department
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int $division_id
 * @property-read \App\Domain\User\Models\Division $division
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Department newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Department newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDepartment query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDepartment whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Department whereDivisionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDepartment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDepartment whereName($value)
 * @mixin \Eloquent
 */
/**
 * App\Domain\User\OldModels\Department
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int $division_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \App\Domain\User\Models\OldDivision $division
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Department newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Department newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Department query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDepartment whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDepartment whereDivisionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Department whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDepartment whereName($value)
 * @mixin \Eloquent
 */
/**
 * App\Domain\User\OldModels\Department
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int $division_id
 * @property-read \App\Domain\User\Models\OldDivision $division
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDepartment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Department newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDepartment query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Department whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDepartment whereDivisionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Department whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDepartment whereName($value)
 * @mixin \Eloquent
 */
/**
 * App\Domain\User\OldModels\Department
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int $division_id
 * @property-read \App\Domain\User\Models\Division $division
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDepartment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Department newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Department query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDepartment whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Department whereDivisionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDepartment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDepartment whereName($value)
 * @mixin \Eloquent
 */
/**
 * App\Domain\User\OldModels\Department
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int $division_id
 * @property-read \App\Domain\User\Models\Division $division
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDepartment newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDepartment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDepartment query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Department whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDepartment whereDivisionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDepartment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDepartment whereName($value)
 * @mixin \Eloquent
 */
/**
 * App\Domain\User\OldModels\Department
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int $division_id
 * @property-read \App\Domain\User\Models\Division $division
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Department newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDepartment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDepartment query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Department whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDepartment whereDivisionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Department whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Department whereName($value)
 * @mixin \Eloquent
 */
/**
 * App\Domain\User\OldModels\Department
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int $division_id
 * @property-read \App\Domain\User\Models\OldDivision $division
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Department newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDepartment newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Department query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Department whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Department whereDivisionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDepartment whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDepartment whereName($value)
 * @mixin \Eloquent
 */
class OldDepartment extends BaseModel
{
    public function __construct(array $attributes = [])
    {
        $this->allowedFilters = [
            AllowedFilter::exact('id'),
            'name',
            'division_id',
            'deleted_at',
            AllowedFilter::custom('deleted', new DeletedModelFilter),
        ];

        parent::__construct($attributes);
    }

    /**
     * Timestamps name array or false to do not use.
     *
     * @var false|array
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'division_id',
    ];

    public $allowedFields = [
        'id',
        'name',
    ];

    public $allowedFilters = [];

    public $allowedIncludes = [
        'division',
        'users',
    ];

    public $allowedSorts = [
        'id',
        'name',
        'division_id',
    ];

    /**
     * Get the department's users.
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }

    /**
     * Get the department's division.
     */
    public function division()
    {
        return $this->belongsTo(OldDivision::class);
    }
}
