<?php

declare(strict_types=1);

namespace App\Domain\User\Models;

use App\Core\Support\QueryBuilder\Filters\DeletedModelFilter;
use App\Infrastructure\Abstracts\BaseModel;
use Spatie\QueryBuilder\AllowedFilter;

/**
 * App\Domain\User\OldModels\Currency
 *
 * @property int $id
 * @property mixed|null $status
 * @property string $amount
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int $author_id
 * @property int $target_user_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Log\OldModels\Log[] $activities
 * @property-read int|null $activities_count
 * @property-read \App\Domain\User\Models\User $author
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\User[] $editors
 * @property-read int|null $editors_count
 * @property-read \App\Domain\User\Models\User $targetUser
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereAuthorId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereTargetUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereUpdatedAt($value)
 * @mixin \Eloquent
 */
final /**
 * App\Domain\User\OldModels\Transaction
 *
 * @property int $id
 * @property string $status
 * @property int $amount
 * @property string|null $created_at
 * @property string|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int $sender_id
 * @property int $getter_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\User[] $editors
 * @property-read int|null $editors_count
 * @property-read \App\Domain\User\Models\User $sender
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereGetterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereSenderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereUpdatedAt($value)
 * @mixin \Eloquent
 */
/**
 * App\Domain\User\OldModels\Transaction
 *
 * @property int $id
 * @property mixed|null $status
 * @property string $amount
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int $sender_id
 * @property int $getter_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\User[] $editors
 * @property-read int|null $editors_count
 * @property-read \App\Domain\User\Models\User $sender
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereGetterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereSenderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereUpdatedAt($value)
 * @mixin \Eloquent
 */
/**
 * App\Domain\User\OldModels\Transaction
 *
 * @property int $id
 * @property mixed|null $status
 * @property string $amount
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int $sender_id
 * @property int $getter_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\User[] $editors
 * @property-read int|null $editors_count
 * @property-read \App\Domain\User\Models\User $sender
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereGetterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereSenderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereUpdatedAt($value)
 * @mixin \Eloquent
 */
/**
 * App\Domain\User\OldModels\Transaction
 *
 * @property int $id
 * @property mixed|null $status
 * @property string $amount
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int $sender_id
 * @property int $getter_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\User[] $editors
 * @property-read int|null $editors_count
 * @property-read \App\Domain\User\Models\User $sender
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereGetterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereSenderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereUpdatedAt($value)
 * @mixin \Eloquent
 */
/**
 * App\Domain\User\OldModels\Transaction
 *
 * @property int $id
 * @property mixed|null $status
 * @property string $amount
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int $sender_id
 * @property int $getter_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\User[] $editors
 * @property-read int|null $editors_count
 * @property-read \App\Domain\User\Models\User $sender
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereGetterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereSenderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereUpdatedAt($value)
 * @mixin \Eloquent
 */
/**
 * App\Domain\User\OldModels\Transaction
 *
 * @property int $id
 * @property mixed|null $status
 * @property string $amount
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int $sender_id
 * @property int $getter_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\User[] $editors
 * @property-read int|null $editors_count
 * @property-read \App\Domain\User\Models\User $sender
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereGetterId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereSenderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Transaction whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Transaction extends BaseModel
{
    public function __construct(array $attributes = [])
    {
        $this->allowedFilters = [
            AllowedFilter::exact('id'),
            'sender_id',
            'getter_id',
            'status',
            'amount',
            'created_at',
            'updated_at',
            'deleted_at',
            AllowedFilter::custom('deleted', new DeletedModelFilter),
        ];

        parent::__construct($attributes);
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'sender_id',
        'getter_id',
        'status',
        'amount',
        ];

    public $allowedFields = [
        'id',
        'sender_id',
        'getter_id',
        'status',
        'amount',
        ];

    public $allowedFilters = [];

    public $allowedIncludes = [
        'editors',
        'sender',
        'getter',
    ];

    public $allowedSorts = [
        'id',
        'sender_id',
        'getter_id',
        'status',
        'amount',
        'created_at',
        'updated_at',
    ];

    public function editors()
    {
        return $this->belongsToMany(
            User::class,
            'transaction_user',
            'transaction_id',
            'editor_id');
    }

    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_id');
    }


    public function getter()
    {
        return $this->belongsTo(User::class, 'target_user_id');
    }

}
