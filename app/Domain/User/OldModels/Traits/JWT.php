<?php

declare(strict_types=1);

namespace App\Domain\User\Models\Traits;

trait JWT
{
    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return int
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array<string|int,string>
     */
    public function getJWTCustomClaims(): array
    {
        return [
            'email' => $this->email,
            'login' => $this->login,
            'avatar' => $this->avatar,
            'name' => [
                'first' => $this->first_name,
                'middle' => $this->middle_name,
                'last' => $this->last_name,
            ],
            'currency_id' => $this->currency_id,
            'department_id' => $this->department_id,
        ];
    }
}
