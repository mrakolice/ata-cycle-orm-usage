<?php

declare(strict_types=1);

namespace App\Domain\User\Models;

use App\Core\Support\QueryBuilder\Filters\DeletedModelFilter;
use App\Infrastructure\Abstracts\BaseModel;
use Spatie\QueryBuilder\AllowedFilter;

final /**
 * App\Domain\User\OldModels\Division
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\Department[] $departments
 * @property-read int|null $departments_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Division newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDivision newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDivision query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Division whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Division whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Division whereName($value)
 * @mixin \Eloquent
 */
/**
 * App\Domain\User\OldModels\Division
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\OldDepartment[] $departments
 * @property-read int|null $departments_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Division newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Division newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Division query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Division whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Division whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDivision whereName($value)
 * @mixin \Eloquent
 */
/**
 * App\Domain\User\OldModels\Division
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\OldDepartment[] $departments
 * @property-read int|null $departments_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDivision newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDivision newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDivision query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDivision whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Division whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Division whereName($value)
 * @mixin \Eloquent
 */
/**
 * App\Domain\User\OldModels\Division
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\Department[] $departments
 * @property-read int|null $departments_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Division newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Division newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Division query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Division whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Division whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Division whereName($value)
 * @mixin \Eloquent
 */
/**
 * App\Domain\User\OldModels\Division
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\OldDepartment[] $departments
 * @property-read int|null $departments_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDivision newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDivision newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Division query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Division whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Division whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Division whereName($value)
 * @mixin \Eloquent
 */
/**
 * App\Domain\User\OldModels\Division
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\Department[] $departments
 * @property-read int|null $departments_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Division newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Division newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDivision query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Division whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDivision whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDivision whereName($value)
 * @mixin \Eloquent
 */
/**
 * App\Domain\User\OldModels\Division
 *
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\OldDepartment[] $departments
 * @property-read int|null $departments_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDivision newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDivision newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\OldDivision query()
 * @mixin \Eloquent
 */
class OldDivision extends BaseModel
{
    public function __construct(array $attributes = [])
    {
        $this->allowedFilters = [
            AllowedFilter::exact('id'),
            'name',
            'deleted_at',
            AllowedFilter::custom('deleted', new DeletedModelFilter),
        ];

        parent::__construct($attributes);
    }

    /**
     * Timestamps name array or false to do not use.
     *
     * @var false|array
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    public $allowedFields = [
        'id',
        'name',
    ];

    public $allowedFilters = [];

    public $allowedIncludes = [
        'departments',
        'departments.users',
    ];

    public $allowedSorts = [
        'id',
        'name',
    ];

    /**
     * Get the division's departments.
     */
    public function departments()
    {
        return $this->hasMany(OldDepartment::class);
    }
}
