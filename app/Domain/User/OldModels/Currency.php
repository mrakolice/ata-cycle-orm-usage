<?php

declare(strict_types=1);

namespace App\Domain\User\Models;

use App\Core\Support\QueryBuilder\Filters\DeletedModelFilter;
use App\Infrastructure\Abstracts\BaseModel;
use Spatie\QueryBuilder\AllowedFilter;

/**
 * App\Domain\User\OldModels\Currency
 *
 * @property int $id
 * @property string $name
 * @property string $code
 * @property string $unicode_sign
 * @property string $html_sign
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\User[] $users
 * @property-read int|null $users_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Currency newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Currency newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Currency query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Currency whereCode($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Currency whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Currency whereHtmlSign($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Currency whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Currency whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Currency whereUnicodeSign($value)
 * @mixin \Eloquent
 */
class Currency extends BaseModel
{
    public function __construct(array $attributes = [])
    {
        $this->allowedFilters = [
            AllowedFilter::exact('id'),
            'name',
            'code',
            'unicode_sign',
            'html_sign',
            'deleted_at',
            AllowedFilter::custom('deleted', new DeletedModelFilter),
        ];

        parent::__construct($attributes);
    }

    /**
     * Timestamps name array or false to do not use.
     *
     * @var false|array
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'code',
        'unicode_sign',
        'html_sign',
    ];

    public $allowedFields = [
        'id',
        'name',
        'code',
        'unicode_sign',
        'html_sign',
    ];

    public $allowedFilters = [];

    public $allowedIncludes = [
        'users',
    ];

    public $allowedSorts = [
        'id',
        'name',
        'code',
        'unicode_sign',
        'html_sign',
    ];

    /**
     * Get the currency's users.
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }
}
