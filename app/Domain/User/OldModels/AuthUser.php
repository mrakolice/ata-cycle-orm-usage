<?php

declare(strict_types=1);

namespace App\Domain\User\Models;

use App\Domain\User\Database\Scopes\UserDefaultScope;
use Illuminate\Database\Eloquent\Relations\Relation;

final /**
 * App\Domain\User\OldModels\AuthUser
 *
 * @property int $id
 * @property string $login
 * @property string $email
 * @property string $phone_number
 * @property string $password
 * @property string $first_name
 * @property string|null $middle_name
 * @property string $last_name
 * @property string|null $avatar
 * @property \Illuminate\Support\Carbon $birthday
 * @property object|null $contacts
 * @property object|null $payment_accounts
 * @property int|null $timezone
 * @property string|null $description
 * @property bool|null $is_verified
 * @property string|null $author_pseudonym
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int $currency_id
 * @property int $department_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Log\OldModels\Log[] $actions
 * @property-read int|null $actions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Log\OldModels\Log[] $activities
 * @property-read int|null $activities_count
 * @property-read \App\Domain\User\Models\Currency $currency
 * @property-read \App\Domain\User\Models\OldDepartment $department
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read int|null $media_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Post\OldModels\Post[] $posts
 * @property-read int|null $posts_count
 * @property-read \App\Domain\Profile\OldModels\Profile $profile
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Post\OldModels\Publication[] $publications
 * @property-read int|null $publications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\User permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\User role($roles, $guard = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereAuthorPseudonym($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereContacts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereDepartmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereIsVerified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereMiddleName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser wherePaymentAccounts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser wherePhoneNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereTimezone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereUpdatedAt($value)
 * @mixin \Eloquent
 */
/**
 * App\Domain\User\OldModels\AuthUser
 *
 * @property int $id
 * @property string $login
 * @property string $email
 * @property string $phone_number
 * @property string $password
 * @property string $first_name
 * @property string|null $middle_name
 * @property string $last_name
 * @property string|null $avatar
 * @property \Illuminate\Support\Carbon $birthday
 * @property array|null $contacts
 * @property array|null $payment_accounts
 * @property int|null $timezone
 * @property string|null $description
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int $currency_id
 * @property int $department_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $actions
 * @property-read int|null $actions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \App\Domain\User\Models\Currency $currency
 * @property-read \App\Domain\User\Models\Department $department
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read int|null $media_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Post\OldModels\Post[] $posts
 * @property-read int|null $posts_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Post\OldModels\Publication[] $publications
 * @property-read int|null $publications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\User permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\User role($roles, $guard = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereContacts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereDepartmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereMiddleName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser wherePaymentAccounts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser wherePhoneNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereTimezone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereUpdatedAt($value)
 * @mixin \Eloquent
 */
/**
 * App\Domain\User\OldModels\AuthUser
 *
 * @property int $id
 * @property string $login
 * @property string $email
 * @property string $phone_number
 * @property string $password
 * @property string $first_name
 * @property string|null $middle_name
 * @property string $last_name
 * @property string|null $avatar
 * @property \Illuminate\Support\Carbon $birthday
 * @property object|null $contacts
 * @property object|null $payment_accounts
 * @property int|null $timezone
 * @property string|null $description
 * @property bool|null $is_verified
 * @property string|null $author_pseudonym
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int $currency_id
 * @property int $department_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Log\OldModels\Log[] $actions
 * @property-read int|null $actions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Log\OldModels\Log[] $activities
 * @property-read int|null $activities_count
 * @property-read \App\Domain\User\Models\Currency $currency
 * @property-read \App\Domain\User\Models\OldDepartment $department
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read int|null $media_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Post\OldModels\Post[] $posts
 * @property-read int|null $posts_count
 * @property-read \App\Domain\Profile\OldModels\Profile $profile
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Post\OldModels\Publication[] $publications
 * @property-read int|null $publications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\User permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\User role($roles, $guard = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereAuthorPseudonym($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereContacts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereDepartmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereIsVerified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereMiddleName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser wherePaymentAccounts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser wherePhoneNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereTimezone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereUpdatedAt($value)
 * @mixin \Eloquent
 */
/**
 * App\Domain\User\OldModels\AuthUser
 *
 * @property int $id
 * @property string $login
 * @property string $email
 * @property string $phone_number
 * @property string $password
 * @property string $first_name
 * @property string|null $middle_name
 * @property string $last_name
 * @property string|null $avatar
 * @property \Illuminate\Support\Carbon $birthday
 * @property object|null $contacts
 * @property object|null $payment_accounts
 * @property int|null $timezone
 * @property string|null $description
 * @property bool|null $is_verified
 * @property string|null $author_pseudonym
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int $currency_id
 * @property int $department_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Log\OldModels\Log[] $actions
 * @property-read int|null $actions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Log\OldModels\Log[] $activities
 * @property-read int|null $activities_count
 * @property-read \App\Domain\User\Models\Currency $currency
 * @property-read \App\Domain\User\Models\Department $department
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read int|null $media_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Post\OldModels\Post[] $posts
 * @property-read int|null $posts_count
 * @property-read \App\Domain\Profile\OldModels\Profile $profile
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Post\OldModels\Publication[] $publications
 * @property-read int|null $publications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\User permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\User role($roles, $guard = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereAuthorPseudonym($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereContacts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereDepartmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereIsVerified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereMiddleName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser wherePaymentAccounts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser wherePhoneNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereTimezone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereUpdatedAt($value)
 * @mixin \Eloquent
 */
/**
 * App\Domain\User\OldModels\AuthUser
 *
 * @property int $id
 * @property string $login
 * @property string $email
 * @property string $phone_number
 * @property string $password
 * @property string $first_name
 * @property string|null $middle_name
 * @property string $last_name
 * @property string|null $avatar
 * @property \Illuminate\Support\Carbon $birthday
 * @property object|null $contacts
 * @property object|null $payment_accounts
 * @property int|null $timezone
 * @property string|null $description
 * @property bool|null $is_verified
 * @property string|null $author_pseudonym
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int $currency_id
 * @property int $department_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Log\OldModels\Log[] $actions
 * @property-read int|null $actions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Log\OldModels\Log[] $activities
 * @property-read int|null $activities_count
 * @property-read \App\Domain\User\Models\Currency $currency
 * @property-read \App\Domain\User\Models\Department $department
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read int|null $media_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Post\OldModels\Post[] $posts
 * @property-read int|null $posts_count
 * @property-read \App\Domain\Profile\OldModels\Profile $profile
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Post\OldModels\Publication[] $publications
 * @property-read int|null $publications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\User permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\User role($roles, $guard = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereAuthorPseudonym($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereContacts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereDepartmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereIsVerified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereMiddleName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser wherePaymentAccounts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser wherePhoneNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereTimezone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereUpdatedAt($value)
 * @mixin \Eloquent
 */
/**
 * App\Domain\User\OldModels\AuthUser
 *
 * @property int $id
 * @property string $login
 * @property string $email
 * @property string $phone_number
 * @property string $password
 * @property string $first_name
 * @property string|null $middle_name
 * @property string $last_name
 * @property string|null $avatar
 * @property \Illuminate\Support\Carbon $birthday
 * @property object|null $contacts
 * @property object|null $payment_accounts
 * @property int|null $timezone
 * @property string|null $description
 * @property bool|null $is_verified
 * @property string|null $author_pseudonym
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int $currency_id
 * @property int $department_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Log\OldModels\Log[] $actions
 * @property-read int|null $actions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Log\OldModels\Log[] $activities
 * @property-read int|null $activities_count
 * @property-read \App\Domain\User\Models\Currency $currency
 * @property-read \App\Domain\User\Models\Department $department
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read int|null $media_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Post\OldModels\Post[] $posts
 * @property-read int|null $posts_count
 * @property-read \App\Domain\Profile\OldModels\Profile $profile
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Post\OldModels\Publication[] $publications
 * @property-read int|null $publications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\User permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\User role($roles, $guard = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereAuthorPseudonym($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereContacts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereDepartmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereIsVerified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereMiddleName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser wherePaymentAccounts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser wherePhoneNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereTimezone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereUpdatedAt($value)
 * @mixin \Eloquent
 */
/**
 * App\Domain\User\OldModels\AuthUser
 *
 * @property int $id
 * @property string $login
 * @property string $email
 * @property string $phone_number
 * @property string $password
 * @property string $first_name
 * @property string|null $middle_name
 * @property string $last_name
 * @property string|null $avatar
 * @property \Illuminate\Support\Carbon $birthday
 * @property object|null $contacts
 * @property object|null $payment_accounts
 * @property int|null $timezone
 * @property string|null $description
 * @property bool|null $is_verified
 * @property string|null $author_pseudonym
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property int $currency_id
 * @property int $department_id
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Log\OldModels\Log[] $actions
 * @property-read int|null $actions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Log\OldModels\Log[] $activities
 * @property-read int|null $activities_count
 * @property-read \App\Domain\User\Models\Currency $currency
 * @property-read \App\Domain\User\Models\Department $department
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\MediaLibrary\Models\Media[] $media
 * @property-read int|null $media_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Post\OldModels\Post[] $posts
 * @property-read int|null $posts_count
 * @property-read \App\Domain\Profile\OldModels\Profile $profile
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Post\OldModels\Publication[] $publications
 * @property-read int|null $publications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\User permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\User role($roles, $guard = null)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereAuthorPseudonym($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereBirthday($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereContacts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereCurrencyId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereDepartmentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereIsVerified($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereLogin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereMiddleName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser wherePaymentAccounts($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser wherePhoneNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereTimezone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\AuthUser whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class AuthUser extends User
{
    protected $table = 'users';

//    protected $with = ['roles.permissions', 'department.division'];

    public function getMorphClass() {
        $morphMap = Relation::morphMap();

        if (! empty($morphMap) && in_array(User::class, $morphMap)) {
            return array_search(User::class, $morphMap, true);
        }

        return User::class;
    }
}
