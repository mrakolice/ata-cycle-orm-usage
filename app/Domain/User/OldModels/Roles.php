<?php

declare(strict_types=1);

namespace App\Domain\User\Models;

use App\Infrastructure\Contracts\QueryBuilderContract;
use App\Infrastructure\Traits\Models\UseQueryBuilder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\Permission\Models\Role as SpatieRole;

/**
 * App\Domain\User\OldModels\Role
 *
 * @property int $id
 * @property string $name
 * @property string $guard_name
 * @property bool $is_protected
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\User[] $users
 * @property-read int|null $users_count
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\User\Models\Role onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Spatie\Permission\Models\Role permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereGuardName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereIsProtected($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\User\Models\Role withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\User\Models\Role withoutTrashed()
 * @mixin \Eloquent
 */
final /**
 * App\Domain\User\OldModels\Role
 *
 * @property int $id
 * @property string $name
 * @property string $guard_name
 * @property bool $is_protected
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Activitylog\Models\Activity[] $activities
 * @property-read int|null $activities_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\User[] $users
 * @property-read int|null $users_count
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\User\Models\Role onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Spatie\Permission\Models\Role permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereGuardName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereIsProtected($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\User\Models\Role withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\User\Models\Role withoutTrashed()
 * @mixin \Eloquent
 */
/**
 * App\Domain\User\OldModels\Role
 *
 * @property int $id
 * @property string $name
 * @property string $guard_name
 * @property bool $is_protected
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\User[] $users
 * @property-read int|null $users_count
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\User\Models\Role onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Spatie\Permission\Models\Role permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereGuardName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereIsProtected($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\User\Models\Role withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\User\Models\Role withoutTrashed()
 * @mixin \Eloquent
 */
/**
 * App\Domain\User\OldModels\Role
 *
 * @property int $id
 * @property string $name
 * @property string $guard_name
 * @property bool $is_protected
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\User[] $users
 * @property-read int|null $users_count
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\User\Models\Role onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Spatie\Permission\Models\Role permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereGuardName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereIsProtected($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\User\Models\Role withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\User\Models\Role withoutTrashed()
 * @mixin \Eloquent
 */
/**
 * App\Domain\User\OldModels\Role
 *
 * @property int $id
 * @property string $name
 * @property string $guard_name
 * @property bool $is_protected
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\User[] $users
 * @property-read int|null $users_count
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\User\Models\Role onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Spatie\Permission\Models\Role permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereGuardName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereIsProtected($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\User\Models\Role withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\User\Models\Role withoutTrashed()
 * @mixin \Eloquent
 */
/**
 * App\Domain\User\OldModels\Role
 *
 * @property int $id
 * @property string $name
 * @property string $guard_name
 * @property bool $is_protected
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\User[] $users
 * @property-read int|null $users_count
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\User\Models\Role onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Spatie\Permission\Models\Role permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereGuardName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereIsProtected($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\User\Models\Role withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\User\Models\Role withoutTrashed()
 * @mixin \Eloquent
 */
/**
 * App\Domain\User\OldModels\Role
 *
 * @property int $id
 * @property string $name
 * @property string $guard_name
 * @property bool $is_protected
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\User[] $users
 * @property-read int|null $users_count
 * @method static bool|null forceDelete()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role newQuery()
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\User\Models\Role onlyTrashed()
 * @method static \Illuminate\Database\Eloquent\Builder|\Spatie\Permission\Models\Role permission($permissions)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereGuardName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereIsProtected($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\Role whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\User\Models\Role withTrashed()
 * @method static \Illuminate\Database\Query\Builder|\App\Domain\User\Models\Role withoutTrashed()
 * @mixin \Eloquent
 */
class Role extends SpatieRole implements QueryBuilderContract
{
    use SoftDeletes;
    use UseQueryBuilder;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'guard_name',
    ];

    public $allowedFields = [
        'id',
        'name',
        'guard_name',
    ];
}
