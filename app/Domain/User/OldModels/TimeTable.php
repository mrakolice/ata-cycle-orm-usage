<?php

namespace App\Domain\User\Models;

use App\Core\Support\QueryBuilder\Filters\DeletedModelFilter;
use App\Infrastructure\Abstracts\BaseModel;
use Spatie\QueryBuilder\AllowedFilter;

/**
 * App\Domain\TimeTable\OldModels\TimeTable
 *
 * @property int $id
 * @property int $owner_id
 * @property string $work_day
 * @property int $work_hours
 * @property string $owner_comment
 * @property string $editor_comment
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\Log\OldModels\Log[] $activities
 * @property-read int|null $activities_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\User[] $editors
 * @property-read int|null $editors_count
 * @property-read \App\Domain\User\Models\User $owner
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereEditorComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereownerComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereownerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereWorkDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereWorkHours($value)
 * @mixin \Eloquent
 */
/**
 * App\Domain\User\OldModels\TimeTable
 *
 * @property int $id
 * @property int $owner_id
 * @property string $work_day
 * @property int $work_hours
 * @property string $owner_comment
 * @property string $editor_comment
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\User[] $editors
 * @property-read int|null $editors_count
 * @property-read \App\Domain\User\Models\User $owner
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereEditorComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereOwnerComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereWorkDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereWorkHours($value)
 * @mixin \Eloquent
 */
/**
 * App\Domain\User\OldModels\TimeTable
 *
 * @property int $id
 * @property int $owner_id
 * @property string $work_day
 * @property int $work_hours
 * @property string $owner_comment
 * @property string $editor_comment
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\User[] $editors
 * @property-read int|null $editors_count
 * @property-read \App\Domain\User\Models\User $owner
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereEditorComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereOwnerComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereWorkDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereWorkHours($value)
 * @mixin \Eloquent
 */
/**
 * App\Domain\User\OldModels\TimeTable
 *
 * @property int $id
 * @property int $owner_id
 * @property string $work_day
 * @property int $work_hours
 * @property string $owner_comment
 * @property string $editor_comment
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\User[] $editors
 * @property-read int|null $editors_count
 * @property-read \App\Domain\User\Models\User $owner
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereEditorComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereOwnerComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereWorkDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereWorkHours($value)
 * @mixin \Eloquent
 */
/**
 * App\Domain\User\OldModels\TimeTable
 *
 * @property int $id
 * @property int $owner_id
 * @property string $work_day
 * @property int $work_hours
 * @property string $owner_comment
 * @property string $editor_comment
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\User[] $editors
 * @property-read int|null $editors_count
 * @property-read \App\Domain\User\Models\User $owner
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereEditorComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereOwnerComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereWorkDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereWorkHours($value)
 * @mixin \Eloquent
 */
/**
 * App\Domain\User\OldModels\TimeTable
 *
 * @property int $id
 * @property int $owner_id
 * @property string $work_day
 * @property int $work_hours
 * @property string $owner_comment
 * @property string $editor_comment
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Domain\User\Models\User[] $editors
 * @property-read int|null $editors_count
 * @property-read \App\Domain\User\Models\User $owner
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereEditorComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereOwnerComment($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereWorkDay($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Domain\User\Models\TimeTable whereWorkHours($value)
 * @mixin \Eloquent
 */
class TimeTable extends BaseModel
{
    public function __construct(array $attributes = [])
    {
        $this->allowedFilters = [
            AllowedFilter::exact('id'),
            'owner_id',
            'work_day',
            'work_hours',
            'owner_comment',
            'editor_comment',
            'created_at',
            'updated_at',
            'deleted_at',
            AllowedFilter::custom('deleted', new DeletedModelFilter),
        ];

        parent::__construct($attributes);
    }

    protected $fillable = [
        'owner_id',
        'work_day',
        'work_hours',
        'owner_comment',
        'editor_comment',
    ];

    public $allowedFields = [
        'id',
        'owner_id',
        'work_day',
        'work_hours',
        'owner_comment',
        'editor_comment',
    ];

    public $allowedFilters = [];

    public $allowedIncludes = [
        'owner',
        'editors',
    ];

    public $allowedSorts = [
        'id',
        'owner_id',
        'work_day',
        'work_hours',
        'owner_comment',
        'editor_comment',
        'created_at',
        'updated_at',
    ];

    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }

    public function editors()
    {
        return $this->belongsToMany(
            User::class,
            'time_table_user',
            'time_table_id',
            'editor_id');
    }
}
