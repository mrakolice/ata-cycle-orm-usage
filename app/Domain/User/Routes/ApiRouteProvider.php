<?php

declare(strict_types=1);

namespace App\Domain\User\Routes;

use App\Domain\User\Http\Controllers\Auth\LoginController;
use App\Domain\User\Http\Controllers\Auth\LogoutController;
use App\Domain\User\Http\Controllers\Auth\MeController;
use App\Domain\User\Http\Controllers\Auth\RefreshController;
use App\Domain\User\Http\Controllers\Auth\SignUpNewController;
use App\Domain\User\Http\Controllers\Auth\SignUpOldController;
use App\Domain\User\Http\Controllers\DepartmentController;
use App\Domain\User\Http\Controllers\DivisionController;
use App\Domain\User\Http\Controllers\RoleController;
use App\Domain\User\Http\Controllers\UserRoleController;
use Illuminate\Support\Facades\Route;
use App\Domain\User\Http\Controllers\TimeTableController;
use App\Domain\User\Http\Controllers\TransactionController;
use App\Domain\User\Http\Controllers\UserController;
use App\Domain\User\Http\Controllers\CurrencyController;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;


final class ApiRouteProvider extends ServiceProvider
{
    public function map(): void
    {
        Route::middleware(['api'])->group(function () {

            Route::prefix('auth')->group(static function () {
                Route::get('me', MeController::class);
                Route::post('login', [LoginController::class, 'apiLogin']);
                Route::post('logout', [LogoutController::class, 'apiLogout']);
                Route::post('sign_up_new', SignUpNewController::class);
                Route::post('sign_up_old', SignUpOldController::class);
                Route::post('refresh', RefreshController::class);
            });

            Route::prefix('users')->group(static function () {
                Route::get('', [UserController::class, 'index']);
                Route::get('{user_id}', [UserController::class, 'show']);
                Route::post('', [UserController::class, 'store']);
                Route::put('{user_id}', [UserController::class, 'update']);
                Route::delete('{user_id}', [UserController::class, 'delete']);

                Route::prefix('trashed')
                    ->group(static function () {
                        Route::put('{user_id}', [UserController::class, 'restore']);
                        Route::delete('{user_id}', [UserController::class, 'destroy']);
                    });
                Route::prefix('roles')
                    ->group(static function () {
                        Route::put('{user_id}/attach/{role_id}', [UserRoleController::class, 'attachRole']);
                        Route::put('{user_id}/detach/{role_id}', [UserRoleController::class, 'detachRole']);
                    });
            });

            Route::prefix('roles')->group(static function () {
                Route::get('', [RoleController::class, 'index']);
                Route::get('{role_id}', [RoleController::class, 'show']);
                Route::post('', [RoleController::class, 'store']);
                Route::put('{role_id}', [RoleController::class, 'update']);
                Route::delete('{role_id}', [RoleController::class, 'delete']);

                Route::prefix('trashed')
                    ->group(static function () {
                        Route::put('{role_id}', [RoleController::class, 'restore']);
                        Route::delete('{role_id}', [RoleController::class, 'destroy']);
                    });
            });

            Route::prefix('currencies')->group(static function () {
                Route::get('', [CurrencyController::class, 'index']);
                Route::get('{currency_id}', [CurrencyController::class, 'show']);
                Route::post('', [CurrencyController::class, 'store']);
                Route::put('{currency_id}', [CurrencyController::class, 'update']);
                Route::delete('{currency_id}', [CurrencyController::class, 'delete']);

                Route::prefix('trashed')
                    ->group(static function () {
                        Route::put('{currency_id}', [CurrencyController::class, 'restore']);
                        Route::delete('{currency_id}', [CurrencyController::class, 'destroy']);
                    });
            });

            Route::prefix('departments')->group(static function () {
                Route::get('', [DepartmentController::class, 'index']);
                Route::get('{department_id}', [DepartmentController::class, 'show']);
                Route::post('', [DepartmentController::class, 'store']);
                Route::put('{department_id}', [DepartmentController::class, 'update']);
                Route::delete('{department_id}', [DepartmentController::class, 'delete']);

                Route::prefix('trashed')
                    ->group(static function () {
                        Route::put('{department_id}', [DepartmentController::class, 'restore']);
                        Route::delete('{department_id}', [DepartmentController::class, 'destroy']);
                    });
            });

            Route::prefix('divisions')->group(static function () {
                Route::get('', [DivisionController::class, 'index']);
                Route::get('{division_id}', [DivisionController::class, 'show']);
                Route::post('', [DivisionController::class, 'store']);
                Route::put('{division_id}', [DivisionController::class, 'update']);
                Route::delete('{division_id}', [DivisionController::class, 'delete']);

                Route::prefix('trashed')
                    ->group(static function () {
                        Route::put('{division_id}', [DivisionController::class, 'restore']);
                        Route::delete('{division_id}', [DivisionController::class, 'destroy']);
                    });
            });

            Route::prefix('time_tables')->group(static function () {
                Route::get('', [TimeTableController::class, 'index']);
                Route::get('{time_table_id}', [TimeTableController::class, 'show']);
                Route::post('', [TimeTableController::class, 'store']);
                Route::put('{time_table_id}', [TimeTableController::class, 'update']);
                Route::delete('{time_table_id}', [TimeTableController::class, 'delete']);

                Route::prefix('trashed')
                    ->group(static function () {
                        Route::put('{time_table_id}', [TimeTableController::class, 'restore']);
                        Route::delete('{time_table_id}', [TimeTableController::class, 'destroy']);
                    });
            });

            Route::prefix('transactions')->group(static function () {
                Route::get('', [TransactionController::class, 'index']);
                Route::get('{transaction_id}', [TransactionController::class, 'show']);
                Route::post('', [TransactionController::class, 'store']);
                Route::put('{transaction_id}', [TransactionController::class, 'update']);
                Route::delete('{transaction_id}', [TransactionController::class, 'delete']);

                Route::prefix('trashed')
                    ->group(static function () {
                        Route::put('{transaction_id}', [TransactionController::class, 'restore']);
                        Route::delete('{transaction_id}', [TransactionController::class, 'destroy']);
                    });
            });
        });
    }
}
