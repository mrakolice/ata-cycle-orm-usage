<?php

declare(strict_types=1);

namespace App\Domain\User\Routes;

use App\Domain\User\Http\Controllers\Auth\LoginController;
use App\Domain\User\Http\Controllers\Auth\LogoutController;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;


final class WebRouteProvider extends ServiceProvider
{
    public function map(): void
    {
        Route::middleware(['web'])->group(function () {

            /*
            http://localhost/admin/login
            http://localhost/admin/logout
            http://localhost/admin/dashboard
            */

            Route::prefix('admin')->group(static function () {
                Route::permanentRedirect('/', '/admin/dashboard');
                Route::view('dashboard', 'dashboard')->name('dashboard');
                Route::view('login', 'login')->name('login');
                Route::post('login', [LoginController::class, 'webLogin']);
                Route::get('logout', [LogoutController::class, 'webLogout']);
            });

        });
    }
}
