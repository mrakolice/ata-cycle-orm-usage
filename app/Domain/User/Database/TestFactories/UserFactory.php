<?php

declare(strict_types=1);

namespace App\Domain\User\Database\TestFactories;

use App\Domain\User\Models\Department;
use App\Domain\User\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Infrastructure\Abstracts\TestFactory;

final class UserFactory extends TestFactory
{
    protected $model = User::class;

    public function fields()
    {
        $gender = $this->faker->randomElement(['male', 'female']);

        $last_name = $this->faker->lastName() . ($gender !== 'male' ? 'а' : '');
        $email = $this->faker->numerify(Str::slug($last_name, '.') . '##') . '@example.com';

        return [
            'login'             => $this->faker->userName(),
            'email'             => $email,
            'phone_number'      => $this->faker->numerify('+79#########'),
            'password'          => 'secret',
            'first_name'        => $this->faker->firstName($gender),
            'middle_name'       => $this->faker->middleName($gender),
            'last_name'         => $last_name,
            'birthday'          => $this->faker->dateTimeBetween($startDate = '-30 years', $endDate = '-19 years'),
            'remember_token'    => Str::random(10),
//            'department_id'     => Department::inRandomOrder()->first('id')->id ?? null,
        ];
    }

    public function states()
    {
        $this->factory->state($this->model, 'deleted', static function () {
            return ['deleted_at' => Carbon::now()->subMonths(2)];
        });

        $this->factory->state($this->model, 'email_verified', static function () {
            return ['email_verified_at' => now()];
        });

        $this->roleStates();
    }

    private function roleStates()
    {
        $this->factory->afterCreatingState(User::class, 'is_developer', static function (User $user) {
            $user->assignRole('Разработчик');
        });
    }
}
