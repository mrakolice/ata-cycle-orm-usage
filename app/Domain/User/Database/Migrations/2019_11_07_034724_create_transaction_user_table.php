<?php
declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


final class CreateTransactionUserTable extends Migration
{
    protected const TABLE = 'transaction_user';

    static public function up()
    {
        Schema::create(self::TABLE, static function (Blueprint $table) {
            $table->unsignedBigInteger('edited_transaction_id')->index();
            $table->foreign('edited_transaction_id')->references('id')->on('transactions')
                ->onDelete('cascade');

            $table->unsignedBigInteger('editor_id')->index();
            $table->foreign('editor_id')->references('id')->on('users')
                ->onDelete('cascade');

            $table->timestamps();
        });
    }


    static public function down()
    {
        Schema::dropIfExists(self::TABLE);
    }
}
