<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

final class AddForeignKeysToDepartmentsTable extends Migration
{
    protected const TABLE = 'departments';

    static public function up()
    {
        Schema::table(self::TABLE, static function (Blueprint $table) {
            $table->unsignedBigInteger('division_id')->index();
            $table->foreign('division_id')->references('id')->on('divisions')
                ->onDelete('set null');
        });
    }

    static public function down()
    {
        Schema::table(self::TABLE, static function (Blueprint $table) {
            $table->dropForeign(['division_id']);
            $table->dropColumn(['division_id']);
        });
    }
}
