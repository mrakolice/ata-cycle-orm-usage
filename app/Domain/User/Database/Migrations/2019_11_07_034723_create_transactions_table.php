<?php
declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    protected const TABLE = 'transactions';

    static public function up(): void
    {
        Schema::create(self::TABLE, static function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->json('status')->nullable();

            $table->string('amount');

            $table->timestamps();
            $table->softDeletes();

            $table->unsignedBigInteger('sender_id')->index(); // создатель (авторизованый юзер)
            $table->unsignedBigInteger('getter_id')->index(); //получатель

            $table->foreign('sender_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('getter_id')->references('id')->on('users')->onDelete('set null');
        });
    }


    public function down()
    {
        Schema::dropIfExists(self::TABLE);
    }
}
