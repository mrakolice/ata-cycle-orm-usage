<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

final class CreateUsersTable extends Migration
{
    protected const TABLE = 'users';

    static public function up()
    {
        Schema::create(self::TABLE, static function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('login')->unique();
            $table->string('email')->unique();
            $table->string('phone_number',12)->unique();
            $table->string('password');
            $table->string('first_name');
            $table->string('middle_name')->nullable();
            $table->string('last_name');
            $table->string('avatar')->nullable();
            $table->date('birthday');
            $table->json('contacts')->nullable();
            $table->json('payment_accounts')->nullable();
            $table->integer('timezone')->nullable();
            $table->text('description')->nullable();
            $table->boolean('is_verified')->nullable();
            $table->string('author_pseudonym')->nullable();

            $table->dateTime('email_verified_at')->nullable();

            $table->rememberToken();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    static public function down()
    {
        Schema::dropIfExists(self::TABLE);
    }
}
