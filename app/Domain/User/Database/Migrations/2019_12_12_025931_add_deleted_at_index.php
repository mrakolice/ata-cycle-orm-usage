<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDeletedAtIndex extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', static function(Blueprint $table){
            $table->index(['deleted_at', 'login', 'email']);
        });
        Schema::table('departments', static function(Blueprint $table){
            $table->index(['deleted_at', 'id']);
        });
        Schema::table('divisions', static function(Blueprint $table){
            $table->index(['deleted_at', 'id']);
        });
        Schema::table('currencies', static function(Blueprint $table){
            $table->index(['deleted_at', 'id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', static function(Blueprint $table){
            $table->dropIndex(['deleted_at', 'login', 'email']);
        });
        Schema::table('departments', static function(Blueprint $table){
            $table->dropIndex(['deleted_at', 'id']);
        });
        Schema::table('divisions', static function(Blueprint $table){
            $table->dropIndex(['deleted_at', 'id']);
        });
        Schema::table('currencies', static function(Blueprint $table){
            $table->dropIndex(['deleted_at', 'id']);
        });
    }
}
