<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

final class CreatePasswordResetsTable extends Migration
{
    protected const TABLE = 'password_resets';

    static public function up()
    {
        Schema::create(self::TABLE, static function (Blueprint $table) {
            $table->string('email')->index();
            $table->string('token');
            $table->timestamp('created_at')->nullable();
        });
    }

    static public function down()
    {
        Schema::dropIfExists(self::TABLE);
    }
}
