<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

final class CreateDivisionsTable extends Migration
{
    protected const TABLE = 'divisions';

    static public function up(): void
    {
        Schema::create(self::TABLE, static function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name', 50);

            $table->softDeletes();
        });
    }

    static public function down(): void
    {
        Schema::dropIfExists(self::TABLE);
    }
}
