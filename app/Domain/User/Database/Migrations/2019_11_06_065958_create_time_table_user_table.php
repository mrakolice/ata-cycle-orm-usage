<?php
declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;


final class CreateTimeTableUserTable extends Migration
{
    protected const TABLE = 'time_table_user';

    static public function up()
    {
        Schema::create(self::TABLE, static function (Blueprint $table) {
            $table->unsignedBigInteger('time_table_id')->index();
            $table->foreign('time_table_id')->references('id')->on('time_tables')
                ->onDelete('cascade');

            $table->unsignedBigInteger('editor_id')->index();
            $table->foreign('editor_id')->references('id')->on('users')
                ->onDelete('cascade');

            $table->string('comment')->nullable();
        });
    }


    static public function down()
    {
        Schema::dropIfExists(self::TABLE);
    }
}
