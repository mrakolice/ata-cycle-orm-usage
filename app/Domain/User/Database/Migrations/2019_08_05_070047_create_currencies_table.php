<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

final class CreateCurrenciesTable extends Migration
{
    protected const TABLE = 'currencies';

    static public function up(): void
    {
        Schema::create(self::TABLE, static function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name', 15);
            $table->string('code', 3);
            $table->string('unicode_sign', 6);
            $table->string('html_sign', 7);

            $table->softDeletes();
        });
    }

    static public function down(): void
    {
        Schema::dropIfExists(self::TABLE);
    }
}
