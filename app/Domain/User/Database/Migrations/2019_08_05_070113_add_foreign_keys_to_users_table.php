<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

final class AddForeignKeysToUsersTable extends Migration
{
    protected const TABLE = 'users';

    static public function up()
    {
        Schema::table(self::TABLE, static function (Blueprint $table) {
            $table->unsignedBigInteger('currency_id')->index();
            $table->foreign('currency_id')->references('id')->on('currencies')
                ->onDelete('set null');

            $table->unsignedBigInteger('department_id')->index();
            $table->foreign('department_id')->references('id')->on('departments')
                ->onDelete('set null');
        });
    }

    static public function down()
    {
        Schema::table(self::TABLE, static function (Blueprint $table) {
            $table->dropForeign('currency_id');
            $table->dropForeign('department_id');
            $table->dropColumn(['currency_id', 'department_id']);
        });
    }
}
