<?php

declare(strict_types=1);

namespace App\Domain\User\Database\Seeds;

use Illuminate\Database\Seeder;

final class UserStoriesSeeder extends Seeder
{
    public function run(): void
    {
         $this->call([
             CurrenciesSeeder::class,
             DivisionsSeeder::class,
             RolesSeeder::class,
             UsersSeeder::class,
         ]);
    }
}
