<?php

declare(strict_types=1);

namespace App\Domain\User\Database\Seeds;

use App\Domain\User\Models\Role;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

final class RolesSeeder extends Seeder
{
    static public function run(): void
    {
        $hierarchy = [
            [
                'name' => 'Новичок', // Видит только себя \ редактирует только себя (профиль)
                'permissions' => [],
            ],
            [
                'name' => 'Копирайтер', // Только свой отдел
                'permissions' => [
                    'view users',
                ],
            ],
            [
                'name' => 'Пикчер',
                'permissions' => [
                    'view users',
                ],
            ],
            [
                'name' => 'SMM-специалист',
                'permissions' => [
                    'view users',
                ],
            ],
            [
                'name' => 'Ученик журналиста',
                'permissions' => [
                    'view users',
                ],
            ],
            [
                'name' => 'Помошник журналиста',
                'permissions' => [
                    'view users',
                ],
            ],
            [
                'name' => 'Журналист',
                'permissions' => [
                    'view users',
                ],
            ],
            [
                'name' => 'Журналист-аналитик', // Видит все отделы
                'permissions' => [
                    'view users',
                ],
            ],
            [
                'name' => 'Помошник редактора',
                'permissions' => [
                    'view users',
                ],
            ],
            [
                'name' => 'Редактор', // Изменить (тех кто ниже) в своем отделе, уволить
                'permissions' => [
                    'view users',
                    'create users',
                    'update users',
                    'delete users',
                    'CRUD transactions',
                ],
            ],
            [
                'name' => 'Директор', // Всех \ Изменить роль (тех кто ниже) в люблм отделе и редакции
                'is_protected' => 1,
                'permissions' => [
                    'view users',
                    'create users',
                    'update users',
                    'delete users',
                    'CRUD departments',
                    'CRUD divisions',
                    'view transactions',
                    'CRUD transactions',
                ],
            ],
            [
                'name' => 'Бухгалтер',
                'is_protected' => 1,
                'permissions' => [
                    'view users',
                    'create users',
                    'update users',
                    'delete users',
                    'CRUD currencies',
                    'view transactions',
                    'CRUD transactions',
                ],
            ],
            [
                'name' => 'HR',
                'is_protected' => 1,
                'permissions' => [
                    'view users',
                    'create users',
                    'update users',
                    'delete users',
                    'CRUD roles',
                    'CRUD departments',
                    'CRUD divisions',
                    'view transactions',
                    'CRUD transactions',
                ],
            ],
            [
                'name' => 'Администратор',
                'is_protected' => 1,
                'permissions' => [
                    'view users',
                    'create users',
                    'update users',
                    'delete users',
                    'CRUD currencies',
                    'CRUD roles',
                    'CRUD departments',
                    'CRUD divisions',
                    'view transactions',
                    'CRUD transactions',
                ],
            ],
            [
                'name' => 'Разработчик',
                'is_protected' => 1,
                'permissions' => [
                    'view users',
                    'create users',
                    'update users',
                    'delete users',
                    'restore users',
                    'force delete users',
                    'CRUD currencies',
                    'force delete currencies',
                    'CRUD roles',
                    'force delete roles',
                    'CRUD departments',
                    'force delete departments',
                    'CRUD divisions',
                    'force delete divisions',
                    'CRUD front routes',
                    'force delete front routes',
                    'view transactions',
                    'CRUD transactions',
                    'force delete transactions',
                ],
            ],
        ];

        collect($hierarchy)
            ->pluck('permissions')
            ->collapse()
            ->unique()->values()
            ->each(function ($permission) {
                Permission::create(['name' => $permission]);
            });

        collect($hierarchy)->each(function ($roleData) {
            /** @var Role $roleData */
            $role = Role::create(collect([
                'name' => $roleData['name'],
                'is_protected' => $roleData['is_protected'] ?? null,
            ])->filter()->all());
            collect($roleData['permissions'])->each(function ($permission) use ($role) {
                $role->givePermissionTo($permission);
            });
        });
    }
}
