<?php

declare(strict_types=1);

namespace App\Domain\User\Database\Seeds;

use Illuminate\Database\Seeder;
use App\Domain\User\Models\Division;
use App\Domain\User\Models\Department;

final class DivisionsSeeder extends Seeder
{
    static public function run(): void
    {
        $hierarchy = [
            [
                'name' => 'Восточная редакция',
                'departments' => [
                    "Армия и оружие",
                    "Наука и хайтек",
                    "Шоубизнес и светская хроника",
                    "Рекламный отдел",
                ],
            ],
            [
                'name' => 'Западная редакция',
                'departments' => [
                    "Красота и мода",
                    "Спорт",
                    "Наука и хайтек",
                    "Шоубизнес и светская хроника",
                    "Рекламный отдел",
                ],
            ],
            [
                'name' => 'Авто редакция',
                'departments' => [
                    "Автоновости",
                ],
            ],
            [
                'name' => 'Администрация',
                'departments' => [
                    "Новички",
                    "Администрация",
                    "Отдел разработки",
                ],
            ],
        ];

        collect($hierarchy)->each(static function ($division) {
            $divisionId = Division::firstOrCreate(['name' => $division['name']])->id;

            collect($division['departments'])->each(static function ($department) use ($divisionId) {
                OldDepartment::firstOrCreate([
                    'name' => $department,
                    'division_id' => $divisionId,
                ]);
            });
        });
    }
}
