<?php

declare(strict_types=1);

namespace App\Domain\User\Database\Seeds;

use Illuminate\Database\Seeder;
use App\Domain\User\Models\User;
use Spatie\Permission\Models\Role;
use App\Domain\User\Models\Currency;
use App\Domain\User\Models\OldDepartment;

final class UsersSeeder extends Seeder
{
    static public function run(): void
    {
        self::createAdmin();

        Role::all('name')->each(static function ($role) {
            self::createTestUser($role->name);
        });
    }

    private static function createAdmin(): void
    {
        factory(User::class)->states([
            'is_developer',
            'email_verified',
        ])->create([
            'email' => 'admin@admin.com',
            'first_name' => 'Лев',
            'middle_name' => 'Николаевич',
            'last_name' => 'Толстой',
            'login' => 'admin',
            'password' => 'secret',
            'currency_id' => Currency::whereName('Рубль')->first()->id ?? null,
            'department_id' => Department::whereName('Отдел разработки')->first()->id ?? null,
            'birthday' => '1828-09-09 13:15:16',
            'is_verified' => 1,
            'author_pseudonym' => 'NeTolstyi',
        ]);
    }

    private static function createTestUser(string $roleName): void
    {
        factory(User::class)
            ->create([
                'first_name' => 'Тестовый',
                'middle_name' => '',
                'last_name' => $roleName,
                'login' => $roleName,
                'password' => 'secret',
            ])->assignRole($roleName);
    }
}
