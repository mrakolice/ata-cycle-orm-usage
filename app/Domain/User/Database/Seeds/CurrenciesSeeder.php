<?php

declare(strict_types=1);

namespace App\Domain\User\Database\Seeds;

use Illuminate\Database\Seeder;
use App\Domain\User\Models\Currency;

final class CurrenciesSeeder extends Seeder
{
    static public function run(): void
    {
        Currency::firstOrCreate([
            'name' => 'Доллар',
            'code' => 'USD',
            'unicode_sign' => 'U+0024',
            'html_sign' => '&#36;',
        ]);
        Currency::firstOrCreate([
            'name' => 'Рубль',
            'code' => 'RUB',
            'unicode_sign' => 'U+20BD',
            'html_sign' => '&#8381;',
        ]);
        Currency::firstOrCreate([
            'name' => 'Гривна',
            'code' => 'UAH',
            'unicode_sign' => 'U+20B4',
            'html_sign' => '&#8372;',
        ]);
    }
}
