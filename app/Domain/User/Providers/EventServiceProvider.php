<?php

namespace App\Domain\User\Providers;

use App\Domain\User\Events\CreateUserEvent;
use App\Domain\User\Listeners\EmailVerificationListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

final class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        CreateUserEvent::class => [
            EmailVerificationListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
