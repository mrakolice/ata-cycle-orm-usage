<?php

declare(strict_types=1);

namespace App\Domain\User\Providers;

use App\Domain\User\Models\User;
use App\Domain\User\Routes\ApiRouteProvider;
use App\Domain\User\Routes\WebRouteProvider;
use App\Infrastructure\Abstracts\DomainServiceProvider;
use App\Domain\User\Database\TestFactories\UserFactory;

final class ServiceProvider extends DomainServiceProvider
{
    /**
     * @var array List of providers to load
     */
    protected $providers = [
        ApiRouteProvider::class,
        WebRouteProvider::class,
        EventServiceProvider::class,
    ];
    /**
     * @var bool Set if will load migrations
     */
    protected $hasMigrations = true;

    /**
     * @var array List of model factories to load
     */
    protected $factories = [
        UserFactory::class,
    ];

    protected $policies = [
    ];
}
