<?php

namespace App\Domain\User\Http\Controllers;

use Illuminate\Http\JsonResponse;
use App\Domain\User\Models\Transaction;
use App\Core\Http\Controllers\Controller;
use App\Domain\User\Resources\TransactionResource;
use App\Domain\User\Requests\Transaction\CreateTransactionRequest;
use App\Domain\User\Requests\Transaction\UpdateTransactionRequest;

/**
 * @group  Транзакции (transactions)
 */
final class TransactionController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.d.refresh');
    }

    /**
     * Список транзакций (list of transactions)
     *
     * @authenticated
     *
     * @queryParam page Номер возвращаемой страницы. Example: 2
     * @queryParam per_page Количество объектов на одной странице. Example: 15
     * @queryParam filter[id] Фильтрация по полю. Принимает набор аргументов через запятую. Example: 1,2,3,4,5,6,7,8,9
     * @queryParam filter[status] Фильтрация по полю. Принимает набор аргументов через запятую. Example: 0
     * @queryParam filter[amount] Фильтрация по полю. Принимает набор аргументов через запятую. Example: 21000,89
     * @queryParam filter[sender_id] Фильтрация по полю. Принимает набор аргументов через запятую. Example: 1
     * @queryParam filter[getter_id] Фильтрация по полю. Принимает набор аргументов через запятую. Example: 2
     * @queryParam filter[created_at] Фильтрация по полю. Принимает набор аргументов через запятую. Example: 2019-08-29 06:37:12
     * @queryParam filter[updated_at] Фильтрация по полю. Принимает набор аргументов через запятую. Example: 2019-08-29 06:37:12
     * @queryParam filter[deleted_at] Фильтрация по полю. Принимает набор аргументов через запятую. Example: 2019-08-29 06:37:12
     * @queryParam fields Выбор возвращаемых полей. Принимает набор аргументов через запятую. Принимаемые аргументы: `id`, `status`, `amount`, `sender_id`, `getter_id`. Example: getter_id,amount
     * @queryParam sort Сортировка по полю. Принимает набор аргументов через запятую, опционально со знаком минус перед аргументом для изменения порядка сортировки. Принимаемые аргументы: id`, `id`, `status`, `amount`, `sender_id`, `getter_id`, `created_at`, `updated_at`. Example: created_at,getter_id
     * @queryParam include Загрузка выбранных отношений. Принимает набор аргументов через запятую. Принимаемые аргументы: 'editors', `author`, `targetUser`. Example: editors
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', Transaction::class);

        return TransactionResource::collection(Transaction::qBMany()->qBGet());
    }

    /**
     * Получение транзакции (get transaction)
     *
     * @queryParam fields Выбор возвращаемых полей. Принимает набор аргументов через запятую. Принимаемые аргументы: `id`, `status`, `amount`, `sender_id`, `getter_id`. Example: getter_id,amount
     *
     * @param string $transactionId
     * @return TransactionResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(string $transactionId)
    {
        $this->authorize('view', Transaction::class);

        return TransactionResource::make(Transaction::qBOne()->findOrFail($transactionId));
    }

    /**
     * Создание транзакции (Create transaction)
     *
     * @bodyParam status array required Статус транзакции Example: 'pending'
     * @bodyParam amount string required Сумма Example: 22222
     * @bodyParam sender_id unsignedBigInteger required ID автора (авторизованный пользователь) Example: 1
     * @bodyParam getter_id unsignedBigInteger required ID получателя Example: 1
     *
     * @param CreateTransactionRequest $createTransactionRequest
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(CreateTransactionRequest $createTransactionRequest)
    {
        $this->authorize('create', Transaction::class);

        return TransactionResource::make(Transaction::create([
                'user_id' => auth()->id(),
            ] + $createTransactionRequest->all()))
            ->response()->setStatusCode(201);
    }

    /**
     * Обновление транзакции (update transaction)
     *
     * @bodyParam status enum Статус транзакции Example: 'pending'
     * @bodyParam amount bigInteger Сумма Example: 22222
     * @bodyParam sender_id unsignedBigInteger ID автора (авторизованный пользователь) Example: 1
     * @bodyParam getter_id unsignedBigInteger ID получателя Example: 1
     *
     * @param UpdateTransactionRequest $updateTransactionRequest
     * @param int $transactionId
     * @return TransactionResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(UpdateTransactionRequest $updateTransactionRequest, int $transactionId)
    {
        $this->authorize('update', Transaction::class);
        $transaction = TransactionResource::make(
            tap(Transaction::findOrFail($transactionId))
                ->update($updateTransactionRequest->all()));
        $transaction->editors = auth()->id();

        return $transaction;
    }

    /**
     * Восстановление транзакции (restore transaction)
     *
     * @bodyParam status enum Статус транзакции Example: 'pending'
     * @bodyParam amount bigInteger Сумма Example: 22222
     * @bodyParam sender_id unsignedBigInteger ID автора (авторизованный пользователь) Example: 1
     * @bodyParam getter_id unsignedBigInteger ID получателя Example: 1
     *
     * @param int $transactionId
     * @return TransactionResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function restore(int $transactionId)
    {
        $this->authorize('restore', Transaction::class);
        $transaction = TransactionResource::make(tap(Transaction::onlyTrashed()->findOrFail($transactionId))->restore());
        $transaction->editors = auth()->id();

        return $transaction;
    }

    /**
     * Перемещение транзакции в корзину (delete transaction)
     *
     * @param int $transactionId
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Exception
     */
    public function delete(int $transactionId)
    {
        $this->authorize('delete', Transaction::class);
        Transaction::findOrFail($transactionId)->delete();

        return response()->json(null, 204);
    }

    /**
     * Удаление транзакции (destroy transaction)
     *
     * @param int $transactionId
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(int $transactionId)
    {
        $this->authorize('forceDelete', Transaction::class);
        Transaction::withTrashed()->findOrFail($transactionId)->forceDelete();

        return response()->json(null, 204);
    }
}
