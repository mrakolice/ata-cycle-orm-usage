<?php

declare(strict_types=1);

namespace App\Domain\User\Http\Controllers\Auth;

use Illuminate\Http\JsonResponse;
use App\Core\Http\Controllers\Controller;
use Redirect;

/**
 * @group Авторизация (auth)
 */
class LogoutController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.d.auth')->only('apiLogout');
    }

    /**
     * Обнуление токена (invalidate the token)
     *
     * @authenticated
     *
     * @response 204 {}
     *
     * @return JsonResponse
     */
    public function apiLogout(): JsonResponse
    {
        auth('api')->logout();

        return response()->json(null, 204);
    }

    /**
     * Обнуление сессии
     *
     * @authenticated
     *
     * @response 302 {}
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function webLogout()
    {
        auth('web')->logout();

        return Redirect::to("login");
    }
}
