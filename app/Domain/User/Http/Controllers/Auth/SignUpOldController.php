<?php

declare(strict_types=1);

namespace App\Domain\User\Http\Controllers\Auth;

use App\Domain\User\Models\User;
use App\Core\Http\Controllers\Controller;
use App\Domain\User\Requests\User\CreateUserRequest;
use App\Domain\User\Resources\UserResource;

/**
 * @group Авторизация (auth)
 */
class SignUpOldController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.d.add');
    }

    /**
     * Регистрация (Sign up).
     *
     * @param CreateUserRequest $signUpRequest
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke(CreateUserRequest $signUpRequest)
    {
        auth('api')->login($user = User::create($signUpRequest->all()));
        UserResource::make($user);

        return UserResource::make($user)->response()->setStatusCode(201);
    }
}
