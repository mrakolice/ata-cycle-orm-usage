<?php

declare(strict_types=1);

namespace App\Domain\User\Http\Controllers\Auth;

use App\Domain\User\Models\User;
use App\Domain\User\Resources\MeResource;
use App\Http\Requests\Auth\LoginRequest;
use App\Core\Http\Controllers\Controller;
use Auth;
use Redirect;
use Spatie\Permission\Exceptions\UnauthorizedException;

/**
 * @group Авторизация (auth)
 */
class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware(['jwt.d.add'])->only('apiLogin');
    }

    /**
     * Авторизация (log the user in)
     *
     * NOTICE: `login`, `username` и `email` взаимозаменяемы
     *
     * @bodyParam login string Email или логин пользователя. Example: admin
     * @bodyParam username string Email или логин пользователя. Example: admin
     * @bodyParam email string Email пользователя. Example: example@example.com
     * @bodyParam password string required Пароль пользователя. Example: secret
     *
     * @param LoginRequest $loginRequest
     * @return MeResource
     */
    public function apiLogin(LoginRequest $loginRequest)
    {
        $user = $this->getUser($loginRequest);
        $token = auth('api')->attempt([
            'login' => $user->login,
            'password' => $loginRequest->get('password'),
        ]);

        if (!$token) {
            throw new UnauthorizedException(401);
        }

        return MeResource::make($user);
    }


    /**
     * Сессионная авторизация
     *
     * @bodyParam login string Email или логин пользователя. Example: -=66Nagibator66=-
     * @bodyParam username string Email или логин пользователя. Example: -=66Nagibator66=-
     * @bodyParam email string Email пользователя. Example: example@example.com
     * @bodyParam password string required Пароль пользователя. Example: SecRet123@#$

     * @param LoginRequest $loginRequest
     * @return \Illuminate\Http\RedirectResponse
     */
    public function webLogin(LoginRequest $loginRequest)
    {
        if (Auth::guard('web')->attempt([
            'login' => $loginRequest->get('username'),
            'password' => $loginRequest->get('password'),
        ])) {
            return redirect()->intended('dashboard');
        }
        return Redirect::to("login");
    }

    /**
     * Получение авторзованного пользователя (get auth user)
     *
     * @param LoginRequest $loginRequest
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    private static function getUser(LoginRequest $loginRequest)
    {
        $login = $loginRequest->get('login') ??
            $loginRequest->get('username') ??
            $loginRequest->get('email');

        return User::
            where('login', $login)
            ->orWhere('email', $login)
            ->firstOrFail();
    }
}
