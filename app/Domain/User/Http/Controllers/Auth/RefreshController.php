<?php

declare(strict_types=1);

namespace App\Domain\User\Http\Controllers\Auth;

use App\Core\Http\Controllers\Controller;

/**
 * @group Авторизация (auth)
 */
class RefreshController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.d.refresh');
    }

    /**
     * Обновление токена (refresh the token)
     *
     * @authenticated
     *
     * @Response 204 {}
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function __invoke()
    {
        return response()->json(null, 204);
    }
}
