<?php

declare(strict_types=1);

namespace App\Domain\User\Http\Controllers\Auth;

use App\Core\Http\Controllers\Controller;
use App\Domain\User\Models\User;
use App\Domain\User\Resources\MeResource;

/**
 * @group Авторизация (auth)
 */
class MeController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.d.refresh');
    }

    /**
     * Получение авторизованного пользователя (get the authenticated user)
     *
     * @authenticated
     *
     * @return MeResource
     */
    public function __invoke()
    {
        return MeResource::make(User::qBOne()->findOrFail(auth()->id()));
    }
}
