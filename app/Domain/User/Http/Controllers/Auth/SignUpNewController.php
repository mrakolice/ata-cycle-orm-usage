<?php

declare(strict_types=1);

namespace App\Domain\User\Http\Controllers\Auth;

use App\Domain\Profile\Models\Education;
use App\Domain\Profile\Models\Profile;
use App\Domain\Profile\Models\Schedule;
use App\Domain\Profile\Models\WorkPlace;
use App\Domain\Profile\Resources\EducationResource;
use App\Domain\Profile\Resources\ProfileResource;
use App\Domain\Profile\Resources\ScheduleResource;
use App\Domain\Profile\Resources\WorkPlaceResource;
use App\Domain\User\Models\User;
use App\Core\Http\Controllers\Controller;
use App\Domain\User\Requests\Registration\RegistrationRequest;
use App\Domain\User\Resources\UserResource;

/**
 * @group Авторизация (auth)
 */
class SignUpNewController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.d.add');
    }

    /**
     * Регистрация (Sign up).
     *
     * @param RegistrationRequest $registrationRequest
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function __invoke($registrationRequest)
    {
        //создаём пользователя
        $user = User::create(
            $registrationRequest->all() + [
                'division_id' => '11',
                'department_id' => '4'
            ]);
        auth()->login($user);
        UserResource::make($user->assgnRole(1));

        //создаём анкету
        $profile = Profile::create([$registrationRequest->all(), 'user_id' => $user['id']]);
        ProfileResource::make($profile);

        //создаём образование
        $education = Education::create([
            'name' => $registrationRequest['university_name'],
            'university_department' => $registrationRequest['university_department'],
            'started_at' => $registrationRequest['education_started_at'],
            'ended_at' => $registrationRequest['education_ended_at'],
            'profile_id' => $profile['id'],
        ]);
        EducationResource::make($education);

        //создаём желаемый график
        $schedule = Schedule::create([$registrationRequest->all(), 'profile_id' => $profile['id']]);
        ScheduleResource::make($schedule);

        //создаём места работы
        $workPlace = WorkPlace::create([
            'name' => $registrationRequest['work_place_name'],
            'position' => $registrationRequest['position'],
            'started_at' => $registrationRequest['work_place_started_at'],
            'ended_at' => $registrationRequest['work_place_ended_at'],
            'profile_id' => $profile['id'],
        ]);
        WorkPlaceResource::make($workPlace);

        return response()->setStatusCode(201);
    }
}
