<?php

declare(strict_types=1);

namespace App\Domain\User\Http\Controllers;

use App\Core\Http\Controllers\Controller;
use App\Domain\User\Models\User;
use App\Domain\User\Resources\UserResource;

/**
 * @group Пользователи (users)
 */
final class UserRoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.d.refresh');
    }

    /**
     * Назначение роли пользователю (attach role)
     *
     * @param int $userId
     * @param int $roleId
     * @return UserResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function attachRole(int $userId, int $roleId)
    {
        $user = User::findOrFail($userId);
        $this->authorize('update', $user);

        return UserResource::make($user->assignRole($roleId));
    }

    /**
     * Удаление роли у пользователя (detach role)
     *
     * @param int $userId
     * @param int $roleId
     * @return UserResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function detachRole(int $userId, int $roleId)
    {
        $user = User::findOrFail($userId);
        $this->authorize('update', $user);

        return UserResource::make($user->removeRole($roleId));
    }
}
