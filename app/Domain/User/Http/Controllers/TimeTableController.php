<?php

namespace App\Domain\User\Http\Controllers;

use Illuminate\Http\Response;
use Illuminate\Http\JsonResponse;
use App\Core\Http\Controllers\Controller;
use App\Domain\User\Models\TimeTable;
use App\Domain\User\Resources\TimeTableResource;
use App\Domain\User\Requests\CreateTimeTableRequest;
use App\Domain\User\Requests\UpdateTimeTableRequest;

/**
 * @group График работы (time table)
 */
final class TimeTableController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.d.refresh');
    }

    /**
     * Просмотр всех доступных графиков рабочего времени (list of time tables)
     *
     * @authenticated
     *
     * @queryParam page Номер возвращаемой страницы. Example: 2
     * @queryParam per_page Количество объектов на одной странице. Example: 15
     * @queryParam filter[id] Фильтрация по полю. Принимает набор аргументов через запятую. Example: 1,2,3,4,5,6,7,8,9
     * @queryParam filter[owner_id] Фильтрация по полю. Принимает набор аргументов через запятую. Example: 1
     * @queryParam filter[work_day] Фильтрация по полю. Принимает набор аргументов через запятую. Example: 2019-08-29
     * @queryParam filter[work_hours] Фильтрация по полю. Принимает набор аргументов через запятую. Example: 8
     * @queryParam filter[owner_comment] Фильтрация по полю. Принимает набор аргументов через запятую. Example: Я заболел, поэтому не явился на работу 21.09.2019
     * @queryParam filter[editor_comment] Фильтрация по полю. Принимает набор аргументов через запятую. Example: Я изменил твой график, потому что в этом месяце у тебя стояло мало часов
     * @queryParam filter[created_at] Фильтрация по полю. Принимает набор аргументов через запятую. Example: 2019-08-29 06:37:12
     * @queryParam filter[updated_at] Фильтрация по полю. Принимает набор аргументов через запятую. Example: 2019-08-29 06:37:12
     * @queryParam filter[deleted_at] Фильтрация по полю. Принимает набор аргументов через запятую. Example: 2019-08-29 06:37:12
     * @queryParam fields Выбор возвращаемых полей. Принимает набор аргументов через запятую. Принимаемые аргументы: `id`, `owner_id`, `work_day`, `work_hours`, `owner_comment`, `editor_comment`. Example: owner_id,work_day
     * @queryParam sort Сортировка по полю. Принимает набор аргументов через запятую, опционально со знаком минус перед аргументом для изменения порядка сортировки. Принимаемые аргументы: id`, `owner_id`, `work_day`, `work_hours`, `owner_comment`, `editor_comment`, `created_at`, `updated_at`. Example: created_at,work_hours
     * @queryParam include Загрузка выбранных отношений. Принимает набор аргументов через запятую. Принимаемые аргументы: 'targetUser', `editors`. Example: editors
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', TimeTable::class);

        return TimeTableResource::collection(TimeTable::qBMany()->qBGet());
    }

    /**
     * Получение рабочего времени (show time table)
     *
     * @queryParam fields Выбор возвращаемых полей. Принимает набор аргументов через запятую. Принимаемые аргументы: `id`, `owner_id`, `work_day`, `work_hours`, `owner_comment`, `editor_comment`. Example: owner_id,work_day
     *
     * @param string $timeTableId
     * @return TimeTableResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(string $timeTableId)
    {
        $this->authorize('view', TimeTable::class);

        return TimeTableResource::make(TimeTable::qBOne()->findOrFail($timeTableId));
    }

    /**
     * Создание рабочего времени (store time table)
     *
     * @bodyParam owner_id string required Имя выбранного пользователя Example: Eugene
     * @bodyParam work_day date required Время изменения Example: 1999_11_22
     * @bodyParam work_hours integer required Количество времени Example: 11
     * @bodyParam owner_comment string Комментарий хозяина расписания Example: Я устал
     * @bodyParam editor_comment string Комментарий редактора Example: Иди работай
     *
     * @param CreateTimeTableRequest $createTimeTableRequest
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(CreateTimeTableRequest $createTimeTableRequest)
    {
        $this->authorize('create', TimeTable::class);

        return TimeTableResource::make(TimeTable::create([
                'user_id' => auth()->id(),
            ] + $createTimeTableRequest->all()))
            ->response()->setStatusCode(201);
    }

    /**
     * Обновление рабочего времени (update time table)
     *
     * @bodyParam owner_id string required Имя выбранного пользователя Example: Eugene
     * @bodyParam work_day date required Время изменения Example: 1999_11_22
     * @bodyParam work_hours integer required Количество времени Example: 11
     * @bodyParam owner_comment string Комментарий хозяина расписания Example: Я устал
     * @bodyParam editor_comment string Комментарий редактора Example: Иди работай
     *
     * @param UpdateTimeTableRequest $updateTimeTableRequest
     * @param int $timeTableId
     * @return TimeTableResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(UpdateTimeTableRequest $updateTimeTableRequest, int $timeTableId)
    {
        $this->authorize('update', TimeTable::class);

        return TimeTableResource::make(
            tap(TimeTable::findOrFail($timeTableId))
                ->update($updateTimeTableRequest->all())
        );
    }

    /**
     * Восстановление транзакции (restore time table)
     *
     * @bodyParam owner_id string required Имя выбранного пользователя Example: Eugene
     * @bodyParam work_day date required Время изменения Example: 1999_11_22
     * @bodyParam work_hours integer required Количество времени Example: 11
     * @bodyParam owner_comment string Комментарий хозяина расписания Example: Я устал
     * @bodyParam editor_comment string Комментарий редактора Example: Иди работай
     *
     * @param int $timeTableId
     * @return TimeTableResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function restore(int $timeTableId)
    {
        $this->authorize('restore', TimeTable::class);

        return TimeTableResource::make(tap(TimeTable::onlyTrashed()->findOrFail($timeTableId))->restore());
    }

    /**
     * Перемещение транзакции в корзину (delete time table)
     *
     * @param int $timeTableId
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Exception
     */
    public function delete(int $timeTableId)
    {
        $this->authorize('delete', TimeTable::class);
        TimeTable::findOrFail($timeTableId)->delete();

        return response()->json(null, 204);
    }

    /**
     * Удаление транзакции (destroy time table)
     *
     * @param int $timeTableId
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(int $timeTableId)
    {
        $this->authorize('forceDelete', TimeTable::class);
        TimeTable::withTrashed()->findOrFail($timeTableId)->forceDelete();

        return response()->json(null, 204);
    }
}
