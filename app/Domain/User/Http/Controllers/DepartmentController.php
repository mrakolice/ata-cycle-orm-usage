<?php

namespace App\Domain\User\Http\Controllers;

use App\Domain\User\Models\Department;
use App\Domain\User\Requests\Department\CreateDepartmentRequest;
use App\Domain\User\Requests\Department\UpdateDepartmentRequest;
use App\Domain\User\Resources\DepartmentResource;
use App\Core\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;

/**
 * @group  Департаменты (departments)
 */
final class DepartmentController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.d.add')->only('index', 'show');
        $this->middleware('jwt.d.refresh')->except('index', 'show');
    }

    /**
     * Список департаментов (list of departments)
     *
     * @authenticated
     *
     * @queryParam page Номер возвращаемой страницы. Example: 2
     * @queryParam per_page Количество объектов на одной странице. Example: 15
     * @queryParam filter[id] Фильтрация по полю. Принимает набор аргументов через запятую. Example: 1,2,3,4,5,6,7,8,9
     * @queryParam filter[name] Фильтрация по полю. Принимает набор аргументов через запятую. Example: Шоу-бизнес
     * @queryParam filter[division_id] Фильтрация по полю. Принимает набор аргументов через запятую. Example: 4
     * @queryParam filter[deleted_at] Фильтрация по полю. Принимает набор аргументов через запятую. Example: 2019-08-29 06:37:12
     * @queryParam fields Выбор возвращаемых полей. Принимает набор аргументов через запятую. Принимаемые аргументы: `id`, `name`, `division_id`. Example: id,name
     * @queryParam sort Сортировка по полю. Принимает набор аргументов через запятую, опционально со знаком минус перед аргументом для изменения порядка сортировки. Принимаемые аргументы: `id`, `name`, `division_id`. Example: division_id,name
     * @queryParam include Загрузка выбранных отношений. Принимает набор аргументов через запятую. Принимаемые аргументы: 'division'. Example: division
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', Department::class);

        return DepartmentResource::collection(Department::qBMany()->qBGet());
    }

    /**
     * Получение департамента (show department)
     *
     * @queryParam fields Выбор возвращаемых полей. Принимает набор аргументов через запятую. Принимаемые аргументы: `id`, `name`, `division_id`. Example: id,name
     *
     * @param string $departmentId
     * @return DepartmentResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(string $departmentId)
    {
        $this->authorize('view', OldDepartment::class);

        return DepartmentResource::make(OldDepartment::qBOne()->findOrFail($departmentId));
    }

    /**
     * Создание департамента (store department)
     *
     * @bodyParam name string required Название департамента. Example: Шоу-бизнес
     * @bodyParam division_id int required Идентификатор дивизиона . Example: 4
     *
     * @param CreateDepartmentRequest $createDepartmentRequest
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(CreateDepartmentRequest $createDepartmentRequest)
    {
        $this->authorize('create', OldDepartment::class);

        return DepartmentResource::make(OldDepartment::create([
                'user_id' => auth()->id(),
            ] + $createDepartmentRequest->all()))
            ->response()->setStatusCode(201);
    }

    /**
     * Обновление департамента (update department)
     *
     * @bodyParam name string  Название департамента. Example: Шоу-бизнес
     * @bodyParam division_id int Идентификатор дивизиона . Example: 4
     *
     * @param UpdateDepartmentRequest $updateDepartmentRequest
     * @param int $departmentId
     * @return DepartmentResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(UpdateDepartmentRequest $updateDepartmentRequest, int $departmentId)
    {
        $this->authorize('update', Department::class);

        return DepartmentResource::make(
            tap(OldDepartment::find($departmentId))
                ->update($updateDepartmentRequest->all())
        );
    }

    /**
     * Восстановление департамента (restore department)
     *
     * @bodyParam name string  Название департамента. Example: Шоу-бизнес
     * @bodyParam division_id int Идентификатор дивизиона . Example: 4
     *
     * @param int $departmentId
     * @return DepartmentResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function restore(int $departmentId)
    {
        $this->authorize('restore', Department::class);

        return DepartmentResource::make(tap(Department::onlyTrashed()->findOrFail($departmentId))->restore());
    }

    /**
     * Перемещение департамента в корзину (delete department)
     *
     * @param int $departmentId
     * @return JsonResponse
     * @throws \Exception
     */
    public function delete(int $departmentId)
    {
        $this->authorize('delete', Department::class);
        OldDepartment::findOrFail($departmentId)->delete();

        return response()->json(null, 204);
    }

    /**
     * Удаление департамента (destroy department)
     *
     * @param int $departmentId
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(int $departmentId)
    {
        $this->authorize('forceDelete', OldDepartment::class);
        OldDepartment::withTrashed()->findOrFail($departmentId)->forceDelete();

        return response()->json(null, 204);
    }
}
