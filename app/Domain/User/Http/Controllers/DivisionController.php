<?php

namespace App\Domain\User\Http\Controllers;

use App\Domain\User\Models\Division;
use App\Domain\User\Requests\Division\CreateDivisionRequest;
use App\Domain\User\Requests\Division\UpdateDivisionRequest;
use App\Domain\User\Resources\DivisionResource;
use Illuminate\Http\JsonResponse;
use App\Core\Http\Controllers\Controller;

/**
 * @group  Дивизионы (divisions)
 */
final class DivisionController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.d.refresh')->except('index', 'show');
    }

    /**
     * Список дивизионов (list of divisions)
     *
     * @authenticated
     *
     * @queryParam page Номер возвращаемой страницы. Example: 2
     * @queryParam per_page Количество объектов на одной странице. Example: 15
     * @queryParam filter[id] Фильтрация по полю. Принимает набор аргументов через запятую. Example: 1,2,3,4,5,6,7,8,9
     * @queryParam filter[name] Фильтрация по полю. Принимает набор аргументов через запятую. Example: Dollar
     * @queryParam filter[deleted_at] Фильтрация по полю. Принимает набор аргументов через запятую. Example: 2019-08-29 06:37:12
     * @queryParam fields Выбор возвращаемых полей. Принимает набор аргументов через запятую. Принимаемые аргументы: `id`, `name`. Example: id,name
     * @queryParam sort Сортировка по полю. Принимает набор аргументов через запятую, опционально со знаком минус перед аргументом для изменения порядка сортировки. Принимаемые аргументы: `id`, `name`. Example: id,name
     * @queryParam include Загрузка выбранных отношений. Принимает набор аргументов через запятую. Принимаемые аргументы: 'departments', `departments.users`. Example: departments
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', OldDivision::class);

        return DivisionResource::collection(Division::qBMany()->qBGet());
    }

    /**
     * Получение дивизиона (show division)
     *
     * @queryParam fields Выбор возвращаемых полей. Принимает набор аргументов через запятую. Принимаемые аргументы: `id`, `name`. Example: id,name
     *
     * @param string $divisionId
     * @return DivisionResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(string $divisionId)
    {
        $this->authorize('view', OldDivision::class);

        return DivisionResource::make(Division::qBOne()->findOrFail($divisionId));
    }

    /**
     * Создание дивизиона (store divisions)
     *
     * @bodyParam name string required Название дивизиона. Example: Восточная редакция
     *
     * @param CreateDivisionRequest $createDivisionRequest
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(CreateDivisionRequest $createDivisionRequest)
    {
        $this->authorize('create', Division::class);

        return DivisionResource::make(OldDivision::create([
                'user_id' => auth()->id(),
            ] + $createDivisionRequest->all()))
            ->response()->setStatusCode(201);
    }

    /**
     * Обновление дивизиона (update division)
     *
     * @bodyParam name string Название дивизиона. Example: Восточная редакция
     *
     * @param UpdateDivisionRequest $updateDivisionRequest
     * @param int $divisionId
     * @return DivisionResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(UpdateDivisionRequest $updateDivisionRequest, int $divisionId)
    {
        $this->authorize('update', Division::class);

        return DivisionResource::make(
            tap(Division::find($divisionId))
                ->update($updateDivisionRequest->all())
        );
    }

    /**
     * Перемещение дивизиона в корзину (delete division)
     *
     * @param int $divisionId
     * @return JsonResponse
     * @throws \Exception
     */
    public function delete(int $divisionId)
    {
        $this->authorize('delete', OldDivision::class);
        Division::findOrFail($divisionId)->delete();

        return response()->json(null, 204);
    }

    /**
     * Восстановление дивизиона (restore division)
     *
     * @bodyParam name string Название дивизиона. Example: Восточная редакция
     *
     * @param int $divisionId
     * @return DivisionResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function restore(int $divisionId)
    {
        $this->authorize('restore', Division::class);

        return DivisionResource::make(tap(OldDivision::onlyTrashed()->findOrFail($divisionId))->restore());
    }

    /**
     * Удаление департамента (destroy department)
     *
     * @param int $divisionId
     * @return JsonResponse
     */
    public function destroy(int $divisionId)
    {
        Division::withTrashed()->findOrFail($divisionId)->forceDelete();

        return response()->json(null, 204);
    }
}
