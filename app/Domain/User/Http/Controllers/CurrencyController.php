<?php

declare(strict_types=1);

namespace App\Domain\User\Http\Controllers;

use App\Core\Http\Controllers\Controller;
use App\Domain\User\Models\Currency;
use App\Domain\User\Requests\Currency\UpdateCurrencyRequest;
use App\Domain\User\Requests\Currency\CreateCurrencyRequest;
use App\Domain\User\Resources\CurrencyResource;

/**
 * @group Валюты (currencies)
 */
final class CurrencyController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.d.add')->only('index', 'show');
        $this->middleware('jwt.d.refresh')->except('index', 'show');
    }

    /**
     * Список валют (list of currencies)
     *
     * @authenticated
     *
     * @queryParam page Номер возвращаемой страницы. Example: 2
     * @queryParam per_page Количество объектов на одной странице. Example: 15
     * @queryParam filter[id] Фильтрация по полю. Принимает набор аргументов через запятую. Example: 1,2,3,4,5,6,7,8,9
     * @queryParam filter[name] Фильтрация по полю. Принимает набор аргументов через запятую. Example: Dollar
     * @queryParam filter[code] Фильтрация по полю. Принимает набор аргументов через запятую. Example: USD
     * @queryParam filter[unicode_sign] Фильтрация по полю. Принимает набор аргументов через запятую. Example: U+0024
     * @queryParam filter[html_sign] Фильтрация по полю. Принимает набор аргументов через запятую. Example: &#36;
     * @queryParam filter[deleted_at] Фильтрация по полю. Принимает набор аргументов через запятую. Example: 2019-08-29 06:37:12
     * @queryParam fields Выбор возвращаемых полей. Принимает набор аргументов через запятую. Принимаемые аргументы: `id`, `name`, `code`, `unicode_sign`, `html_sign`. Example: id,name
     * @queryParam sort Сортировка по полю. Принимает набор аргументов через запятую, опционально со знаком минус перед аргументом для изменения порядка сортировки. Принимаемые аргументы: `id`, `name`, `code`, `unicode_sign`, `html_sign`. Example: id,unicode_sign
     * @queryParam include Загрузка выбранных отношений. Принимает набор аргументов через запятую. Принимаемые аргументы: 'users'. Example: users
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', Currency::class);

        return CurrencyResource::collection(Currency::qBMany()->qBGet());
    }

    /**
     * Получение валюты (show currency)
     *
     * @queryParam fields Выбор возвращаемых полей. Принимает набор аргументов через запятую. Принимаемые аргументы: `id`, `name`, `code`, `unicode_sign`, `html_sign`. Example: id,name
     *
     * @param int $currencyId
     * @return CurrencyResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(int $currencyId)
    {
        $this->authorize('view', Currency::class);

        return CurrencyResource::make(Currency::qBOne()->findOrFail($currencyId));
    }

    /**
     * Создание валюты (store currency)
     *
     * @bodyParam name string required Название валюты Example: Dollar
     * @bodyParam code string required Код валюты Example: USD
     * @bodyParam unicode_sign string required Символ в Unicode Example: U+0024
     * @bodyParam html_sign string required Символ HTML Example: &#36;
     *
     * @param CreateCurrencyRequest $createCurrencyRequest
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(CreateCurrencyRequest $createCurrencyRequest)
    {
        $this->authorize('create', Currency::class);

        return CurrencyResource::make(Currency::create([
                'user_id' => auth()->id(),
            ] + $createCurrencyRequest->all()))
            ->response()->setStatusCode(201);
    }

    /**
     * Обновление валюты (update currency)
     *
     * @bodyParam name string Название валюты Example: Dollar
     * @bodyParam code string Код валюты Example: USD
     * @bodyParam unicode_sign string Символ в Unicode Example: U+0024
     * @bodyParam html_sign string Символ HTML Example: &#36;
     *
     * @param UpdateCurrencyRequest $updateCurrencyRequest
     * @param int $currencyId
     * @return CurrencyResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(UpdateCurrencyRequest $updateCurrencyRequest, int $currencyId)
    {
        $this->authorize('update', Currency::class);

        return CurrencyResource::make(
            tap(Currency::findOrFail($currencyId))->update($updateCurrencyRequest->all())
        );
    }

    /**
     * Восстановление валюты (restore currency)
     *
     * @bodyParam name string Название валюты Example: Dollar
     * @bodyParam code string Код валюты Example: USD
     * @bodyParam unicode_sign string Символ в Unicode Example: U+0024
     * @bodyParam html_sign string Символ HTML Example: &#36;
     *
     * @param int $currencyId
     * @return CurrencyResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function restore(int $currencyId)
    {
        $this->authorize('restore', Currency::class);

        return CurrencyResource::make(tap(Currency::onlyTrashed()->findOrFail($currencyId))->restore());
    }

    /**
     * Перемещение валюты в корзину (delete currency)
     *
     * @param int $currencyId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete(int $currencyId)
    {
        $this->authorize('delete', Currency::class);
        Currency::findOrFail($currencyId)->delete();

        return response()->json(null, 204);
    }

    /**
     * Удаление валюты (destroy currency)
     *
     * @param int $currencyId
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(int $currencyId)
    {
        $this->authorize('forceDelete', Currency::class);
        Currency::withTrashed()->findOrFail($currencyId)->forceDelete();

        return response()->json(null, 204);
    }
}
