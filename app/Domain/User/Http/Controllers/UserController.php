<?php

declare(strict_types=1);

namespace App\Domain\User\Http\Controllers;

use App\Domain\User\Models\User;
use App\Core\Http\Controllers\Controller;
use App\Domain\User\Resources\UserResource;
use App\Domain\User\Requests\User\CreateUserRequest;
use App\Domain\User\Requests\User\UpdateUserRequest;
use Illuminate\Auth\Access\AuthorizationException;

/**
 * @group Пользователи (users)
 */
final class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.d.refresh');
    }

    /**
     * Список пользователей (list of users)
     *
     * @authenticated
     *
     * @queryParam page Номер возвращаемыой страницы. Example: 2
     * @queryParam per_page Количество объектов на одной странице. Example: 15
     * @queryParam filter[id] Фильтрация по полю (exact). Принимает набор аргументов через запятую. Example: 1,2,3,4,5,6,7,8,9
     * @queryParam filter[first_name] Фильтрация по полю. Принимает набор аргументов через запятую. Example: Tom
     * @queryParam filter[middle_name] Фильтрация по полю. Принимает набор аргументов через запятую. Example: Marvolo
     * @queryParam filter[last_name] Фильтрация по полю. Принимает набор аргументов через запятую. Example: Riddle
     * @queryParam filter[email] Фильтрация по полю (exact). Принимает набор аргументов через запятую. Example: volandemort@mail.com
     * @queryParam filter[phone_number] Фильтрация по полю (exact). Принимает набор аргументов через запятую. Example: +79873215476
     * @queryParam filter[login] Фильтрация по полю (exact). Принимает набор аргументов через запятую. Example: the_volandemort
     * @queryParam filter[description] Фильтрация по полю. Принимает набор аргументов через запятую. Example: Тот, кого нельзя называть.
     * @queryParam filter[password] Фильтрация по полю. Принимает набор аргументов через запятую. Example: SeCrEt666=).
     * @queryParam filter[avatar] Фильтрация по полю. Принимает набор аргументов через запятую. Example: picture.png.
     * @queryParam filter[birthday] Фильтрация по полю. Принимает набор аргументов через запятую. Example: 1970-01-01.
     * @queryParam filter[contacts] Фильтрация по полю. Принимает набор аргументов через запятую. Example: [{"type":"VKontakte", "account":"id000000000"},{"type":"Telegram", "account":"89999999999"}].
     * @queryParam filter[payment_accounts] Фильтрация по полю. Принимает набор аргументов через запятую. Example: [{"type":"Sberbank", "account":"4276500012345678"},{"type":"Qiwi", "account":"89999999999"}].
     * @queryParam filter[timezone] Фильтрация по полю. Принимает набор аргументов через запятую. Example: -3.
     * @queryParam filter[is_verified] Фильтрация по полю. Принимает набор аргументов через запятую. Example: true.
     * @queryParam filter[author_pseudonym] Фильтрация по полю. Принимает набор аргументов через запятую. Example: Volyk.
     * @queryParam filter[currency_id] Фильтрация по полю (exact). Принимает набор аргументов через запятую. Example: 1
     * @queryParam filter[department_id] Фильтрация по полю (exact). Принимает набор аргументов через запятую. Example: 13
     * @queryParam filter[created_at] Фильтрация по полю. Принимает набор аргументов через запятую. Example: 2019-08-29 06:37:12
     * @queryParam filter[updated_at] Фильтрация по полю. Принимает набор аргументов через запятую. Example: 2019-08-29 06:37:12
     * @queryParam filter[deleted_at] Фильтрация по полю. Принимает набор аргументов через запятую. Example: 2019-08-29 06:37:12
     * @queryParam fields Выбор возвращаемых полей. Принимает набор аргументов через запятую. Принимаемые аргументы: `id`, `first_name`, `middle_name`, `last_name`, `email`, `phone_number`, `login`, `description`, `password`, `avatar`, `birthday`, `payment_accounts`, `timezone`, `is_verified`, `author_pseudonym`, `currency_id`, `contacts`, `department_id`. Example: id,first_name,last_name
     * @queryParam sort Сортировка по полю. Принимает набор аргументов через запятую, опционально со знаком минус перед аргументом для изменения порядка сортировки. Принимаемые аргументы: `id`, `first_name`, `middle_name`, `last_name`, `email`, `phone_number`, `login`, `description`, `password`, `avatar`, `birthday`, `payment_accounts`, `timezone`, `is_verified`, `author_pseudonym`, `currency_id`, `contacts`, `department_id`, `created_at`, `updated_at`. Example: first_name,-last_name
     * @queryParam include Загрузка выбранных отношений. Принимает набор аргументов через запятую. Принимаемые аргументы: `roles`, `roles.permissions`, `currency`, `department`, `department.division`, `profile`, `profile.education`, `profile.schedule`, `profile.work_places`. Example: department,department.division
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', User::class);

        return UserResource::collection(User::qBMany()->qBGet());
    }

    /**
     * Получение пользователя (show user)
     *
     * @queryParam fields Выбор возвращаемых полей. Принимает набор аргументов через запятую. Принимаемые аргументы: `id`, `first_name`, `middle_name`, `last_name`, `email`, `phone_number`, `login`, `description`, `password`, `avatar`, `birthday`, `payment_accounts`, `timezone`, `is_verified`, `author_pseudonym`, `currency_id`, `contacts`, `department_id`. Example: id,first_name
     *
     * @param int $userId
     * @return UserResource
     * @throws AuthorizationException
     */
    public function show(int $userId)
    {
        $user = User::qBOne()->findOrFail($userId);
        dd($user->toArray());
        $this->authorize('view', $user);

        return UserResource::make($user);
    }

    /**
     * Создание пользователя (create user)
     *
     * @bodyParam login string required Логин Example: -=66Nagibator66=-
     * @bodyParam email email required Электронная почта Example: example@example.com
     * @bodyParam phone_number string required Номер телефона Example: 89999999999
     * @bodyParam password string required Пароль Example: SecRet123@#$
     * @bodyParam password_confirmation string required Пароль Example: SecRet123@#$
     * @bodyParam first_name string required Имя Example: Иоанн
     * @bodyParam middle_name string Отчество Example: Васильевич
     * @bodyParam last_name string required Фамилия Example: Грозный
     * @bodyParam avatar string Аватар Example: image.jpeg
     * @bodyParam birthday string required Дата рождения Example: 1970-01-01
     * @bodyParam contacts string required Контакты Example: [{"type":"VKontakte", "account":"id000000000"},{"type":"Telegram", "account":"89999999999"}]
     * @bodyParam payment_accounts string Аккаунты платежей Example: [{"type":"Sberbank", "account":"4276500012345678"},{"type":"Qiwi", "account":"89999999999"}]
     * @bodyParam timezone int Часовой пояс Example: +10
     * @bodyParam description string Описание Example: Работник года
     * @bodyParam currency_id numeric Идентификатор валюты Example: 1
     * @bodyParam department_id numeric required Идентификатор департамента Example: 1
     * @bodyParam email_verified_at date required Дата подтверждения электронного адреса Example: 2019-11-11 04:59:40
     * @bodyParam author_pseudonym string Псевдоним автора Example: Tsar
     * @bodyParam is_verified boolean Верифицирован ли пользователь Example: 1
     *
     * @param CreateUserRequest $createUserRequest
     * @return \Illuminate\Http\JsonResponse
     * @throws AuthorizationException
     */
    public function store(CreateUserRequest $createUserRequest)
    {
        $model = User::make([
                'contacts' => json_decode($createUserRequest->input('contacts')),
                'payment_accounts' => json_decode($createUserRequest->input('payment_accounts')),
            ] + $createUserRequest->validated());
        $this->authorize('create', $model);
        $model->save();

        return UserResource::make($model)
            ->response()->setStatusCode(201);
    }

    /**
     * Обновление пользователя (update user)
     *
     * @bodyParam login string Логин Example: -=66Nagibator66=-
     * @bodyParam email email Электронная почта Example: example@example.com
     * @bodyParam phone_number string Номер телефона Example: 89999999999
     * @bodyParam password string Пароль Example: SecRet123@#$
     * @bodyParam first_name string Имя Example: Иоанн
     * @bodyParam middle_name string Отчество Example: Васильевич
     * @bodyParam last_name string Фамилия Example: Грозный
     * @bodyParam avatar string Аватар Example: image.jpg
     * @bodyParam birthday string Дата рождения Example: 01.01.1970
     * @bodyParam contacts string Контакты Example: [{'type':'VKontakte', 'account':'id000000000'},{'type':'Telegram', 'account':'89999999999'}]
     * @bodyParam payment_accounts string Аккаунты платежей Example: [{'type':'Sberbank', 'account':'4276500012345678'},{'type':'Qiwi', 'account':'89999999999'}]
     * @bodyParam timezone int Часовой пояс Example: +10
     * @bodyParam description string Описание Example: Работник года
     * @bodyParam currency_id numeric Идентификатор валюты Example: 1
     * @bodyParam department_id string Идентификатор департамента Example: 1
     * @bodyParam email_verified_at date Дата подтверждения электронного адреса Example: 2019-11-11 04:59:40
     * @bodyParam author_pseudonym string Псевдоним автора Example: Tsar
     * @bodyParam is_verified boolean Верифицирован ли пользователь Example: 1
     *
     * @param UpdateUserRequest $updateUserRequest
     * @param int $userId
     * @return UserResource
     * @throws AuthorizationException
     */
    public function update(UpdateUserRequest $updateUserRequest, int $userId)
    {
        $user = User::findOrFail($userId);
        $this->authorize('update', $user);

        return UserResource::make(
            tap($user)->update([
                    'contacts' => json_decode($updateUserRequest->input('contacts')),
                    'payment_accounts' => json_decode($updateUserRequest->input('payment_accounts')),
                ] + $updateUserRequest->validated())
        );
    }

    /**
     * Удаление пользователя (delete user)
     *
     * @param int $userId
     * @return \Illuminate\Http\JsonResponse
     * @throws AuthorizationException
     */
    public function delete(int $userId)
    {
        $this->authorize('delete', User::class);
        User::destroy($userId);

        return response()->json(null, 204);
    }

    /**
     * Восстановление пользователя (restore user)
     *
     * @param int $userId
     * @return UserResource
     * @throws AuthorizationException
     */
    public function restore(int $userId)
    {
        $this->authorize('restore', User::class);

        return UserResource::make(
            tap(User::withTrashed()->findOrFail($userId))->restore()
        );
    }

    /**
     * Полное удаление пользователя (destroy user)
     *
     * @param int $userId
     * @return \Illuminate\Http\JsonResponse
     * @throws AuthorizationException
     */
    public function destroy(int $userId)
    {
        $this->authorize('forceDelete', User::class);
        User::withTrashed()->findOrFail($userId)->forceDelete();

        return response()->json(null, 204);
    }
}
