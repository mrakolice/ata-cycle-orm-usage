<?php

declare(strict_types=1);

namespace App\Domain\User\Http\Controllers;

use App\Domain\User\Models\Role;
use App\Core\Http\Controllers\Controller;
use App\Domain\User\Requests\Role\CreateRoleRequest;
use App\Domain\User\Requests\Role\UpdateRoleRequest;
use App\Domain\User\Resources\RoleResource;
use Illuminate\Http\JsonResponse;

/**
 * @group Роли (roles)
 */
final class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.d.refresh')->except('index', 'show');
    }

    /**
     * Список ролей (list of roles)
     *
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function index()
    {
        $this->authorize('viewAny', Role::class);

        return RoleResource::collection(Role::qBMany()->qBGet());
    }

    /**
     * Получение ролей (show roles)
     *
     * @param int $roleId
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function show(int $roleId)
    {
        $this->authorize('view', Role::class);

        return RoleResource::make(Role::qBOne()->findOrFail($roleId));
    }

    /**
     * Создание роли (store role)
     *
     * @bodyParam name string required Имя Example: Турбоклинер
     *
     * @param CreateRoleRequest $createRoleRequest
     * @return RoleResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(CreateRoleRequest $createRoleRequest)
    {
        $this->authorize('create', Role::class);

        return RoleResource::make(Role::create(['name' => $createRoleRequest['name']]))
            ->response()->setStatusCode(201);
    }

    /**
     * Обоновление роли (update role)
     *
     * @bodyParam name string Имя Example: чистельщик кода
     *
     * @param UpdateRoleRequest $updateRoleRequest
     * @param int $roleId
     * @return RoleResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(UpdateRoleRequest $updateRoleRequest, int $roleId)
    {
        $this->authorize('update', Role::class);

        return RoleResource::make(tap(Role::findOrFail($roleId))->update($updateRoleRequest->all()));
    }

    /**
     * Удаление роли (delete role)
     *
     * @param int $roleId
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function delete(int $roleId)
    {
        $this->authorize('delete', Role::class);
        Role::destroy($roleId);

        return response()->json(null, 204);
    }

    /**
     * Восстановление роли (restore role)
     *
     * @param int $roleId
     * @return RoleResource
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function restore(int $roleId)
    {
        $this->authorize('restore', Role::class);

        return RoleResource::make(tap(Role::withTrashed()->findOrFail($roleId))->restore());
    }

    /**
     * Уничтожение роли (destroy role)
     *
     * @param int $roleId
     * @return JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(int $roleId)
    {
        $this->authorize('forceDelete', Role::class);
        Role::withTrashed()->findOrFail($roleId)->forceDelete();

        return response()->json(null, 204);
    }
}
