<?php

namespace App\Domain\User\Resources;

use App\Domain\Profile\Resources\ProfileResource;
use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    public function toArray($request): array
    {
        $user = $this->resource;

        return collect([
            'id'                => $user->id,
            'name' => array_filter([
                'first'         => $user->first_name,
                'middle'        => $user->middle_name,
                'last'          => $user->last_name,
            ]),
            'email'             => $user->email,
            'login'             => $user->login,
            'phone_number'      => $user->phone_number,
            'avatar'            => $user->avatar,
            'birthday'          => optional($user->birthday)->toDateString(),
            'contacts'          => $user->contacts ?? [],
            'payment_accounts'  => $user->payment_accounts ?? [],
            'timezone'          => $user->timezone,
            'description'       => $user->description,

            'currency_id'       => $user->currency_id,
            'department_id'     => $user->department_id,

            'currency'          => CurrencyResource::make($this->whenLoaded('currency')),
            'department'        => DepartmentResource::make($this->whenLoaded('department')),
            'profile'           => ProfileResource::make($this->whenLoaded('profile')),
            'roles'             => RoleResource::collection($this->whenLoaded('roles')),

            'email_verified_at' => optional($user->email_verified_at)->toDateTimeString(),
            'is_verified'       => $user->is_verified,
            'author_pseudonym'  => $user->author_pseudonym,

            'created_at'        => optional($user->created_at)->toDateTimeString(),
            'updated_at'        => optional($user->updated_at)->toDateTimeString(),
            'deleted_at'        => optional($user->deleted_at)->toDateTimeString(),
        ])->filter()->all();
    }
}
