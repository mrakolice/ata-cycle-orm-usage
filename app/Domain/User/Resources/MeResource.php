<?php

namespace App\Domain\User\Resources;

use App\Domain\FrontRoute\Models\FrontRoute;
use App\Domain\FrontRoute\Resources\FrontRouteResource;
use Illuminate\Http\Resources\Json\JsonResource;

class MeResource extends JsonResource
{
    public function toArray($request): array
    {
        $user = $this->resource;

        return [
            'user' => UserResource::make($user),
            'routes' => FrontRouteResource::collection(
                FrontRoute::with('menus')->get()
            ),
        ];
    }
}
