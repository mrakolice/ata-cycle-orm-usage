<?php

namespace App\Domain\User\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TransactionResource extends JsonResource
{
    public function toArray($request): array
    {
        $transaction = $this->resource;

        return collect([
            'id'             => $transaction->id,
            'sender_id'      => $transaction->sender_id,
            'getter_id'      => $transaction->getter_id,
            'status'         => $transaction->status,
            'amount'         => $transaction->amount,

            'created_at'     => optional($transaction->created_at)->toDateTimeString(),
            'updated_at'     => optional($transaction->updated_at)->toDateTimeString(),
            'deleted_at'     => optional($transaction->deleted_at)->toDateTimeString(),
        ])->filter()->all();
    }
}
