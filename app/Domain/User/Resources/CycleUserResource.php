<?php

namespace App\Domain\User\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CycleUserResource extends JsonResource
{
    public function toArray($request): array
    {
        $currency = $this->resource;

        return collect([
            'id'           => $currency->id,
            'name'         => $currency->name,
        ])->filter()->all();
    }
}
