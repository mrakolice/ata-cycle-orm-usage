<?php

namespace App\Domain\User\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TimeTableResource extends JsonResource
{
    public function toArray($request): array
    {
        $timeTable = $this->resource;

        return collect([
            'id'             => $timeTable->id,
            'owner_id'        => $timeTable->user_id,
            'work_day'       => $timeTable->work_day,
            'work_hours'     => $timeTable->work_hours,
            'owner_comment'  => $timeTable->owner_comment,
            'editor_comment' => $timeTable->editor_comment,

            'owner'          => UserResource::make($this->whenLoaded('owner')),
            'editors'        => UserResource::collection($this->whenLoaded('editors')),

            'created_at'  => optional($timeTable->created_at)->toDateTimeString(),
            'updated_at'  => optional($timeTable->updated_at)->toDateTimeString(),
            'deleted_at'  => optional($timeTable->deleted_at)->toDateTimeString(),
        ])->filter()->all();
    }
}
