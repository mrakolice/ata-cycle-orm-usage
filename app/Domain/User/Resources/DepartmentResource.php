<?php

namespace App\Domain\User\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DepartmentResource extends JsonResource
{
    public function toArray($request): array
    {
        $department = $this->resource;

        return collect([
            'id'          => $department->id,
            'name'        => $department->name,
            'division_id' => $department->division_id,

            'division'    => DivisionResource::make($this->whenLoaded('division')),
            'users'       => UserResource::collection($this->whenLoaded('users')),

            'deleted_at'  => optional($department->deleted_at)->toDateTimeString(),
        ])->filter()->all();
    }
}
