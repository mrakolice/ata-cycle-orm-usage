<?php

namespace App\Domain\User\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CurrencyResource extends JsonResource
{
    public function toArray($request): array
    {
        $currency = $this->resource;

        return collect([
            'id'           => $currency->id,
            'name'         => $currency->name,
            'code'         => $currency->code,
            'unicode_sign' => $currency->unicode_sign,
            'html_sign'    => $currency->html_sign,

            'users'        => UserResource::collection($this->whenLoaded('users')),

            'deleted_at'   => optional($currency->deleted_at)->toDateTimeString(),
        ])->filter()->all();
    }
}
