<?php

namespace App\Domain\User\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DivisionResource extends JsonResource
{
    public function toArray($request): array
    {
        $division = $this->resource;

        return collect([
            'id'          => $division->id,
            'name'        => $division->name,

            'departments' => DepartmentResource::collection($this->whenLoaded('departments')),

            'deleted_at'  => optional($division->deleted_at)->toDateTimeString(),
        ])->filter()->all();
    }
}
