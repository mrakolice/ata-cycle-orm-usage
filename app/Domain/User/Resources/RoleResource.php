<?php


namespace App\Domain\User\Resources;


use Illuminate\Http\Resources\Json\JsonResource;

class RoleResource extends JsonResource
{
    public function toArray($request)
    {
        $role = $this->resource;

        return [
            'id'         => $role->id,
            'name'       => $role->name,
            'guard_name' => $role->email,

            'created_at' => optional($role->created_at)->toDateTimeString(),
            'updated_at' => optional($role->updated_at)->toDateTimeString(),
        ];
    }
}
