<?php


namespace App\Domain\User\Models;


use AtaCycle\Models\BaseModel;
use Cycle\Annotated\Annotation\Column;
use Cycle\Annotated\Annotation\Entity;
use Cycle\Annotated\Annotation\Relation\BelongsTo;
use Cycle\Annotated\Annotation\Table;
use Cycle\Annotated\Annotation\Table\Index;
use App\Domain\User\Models\Department;
use AtaCycle\Mappers\DefaultMapper;
use AtaCycle\Constrains\NotDeletedConstrain;

/**
 * @Entity(mapper=DefaultMapper::class, constrain=NotDeletedConstrain::class)
 * @Table(
 *      indexes={
 *             @Index(columns = {"login"}, unique = true),
 *             @Index(columns = {"email"}, unique = true),
 *             @Index(columns = {"phone_number"}, unique = true),
 *             @Index(columns = {"deleted_at", "login", "email"}),
 *      }
 * )
 */
class User extends BaseModel
{
    /** @Column(type= "string(255)") */
    public $login;

    /** @Column(type= "string(255)") */
    public $email;

    /** @Column(type= "string(12)") */
    public $phone_number;

    /** @Column(type= "string(255)") */
    public $password;

    /** @Column(type= "string(255)") */
    public $first_name;

    /** @Column(type= "string(255)", nullable=true) */
    public $middle_name;

    /** @Column(type= "string(255)") */
    public $last_name;

    /** @Column(type= "string(255)", nullable=true) */
    public $avatar;

    /** @Column(type= "timestamp") */
    public $birthday;

    /** @Column(type= "json", nullable=true) */
    public $contacts;

    /** @Column(type= "json", nullable=true) */
    public $payment_accounts;

    /** @Column(type= "int", nullable=true) */
    public $timezone;

    /** @Column(type= "text", nullable=true) */
    public $description;

    /** @Column(type= "datetime", nullable=true) */
    public $email_verified_at;

    /** @Column(type= "string(100)", nullable=true) */
    public $remember_token;

    /** @BelongsTo(target=Department::class, nullable=true) */
    public $department;
}
