<?php


namespace App\Domain\User\Models;


use App\Infrastructure\Abstracts\BaseCycleModel;
use Cycle\Annotated\Annotation\Column;
use Cycle\Annotated\Annotation\Entity;
use Cycle\Annotated\Annotation\Relation\BelongsTo;
use App\Domain\User\Models\Division;
use App\Infrastructure\Abstracts\Constrains\NotDeletedConstrain;
use App\Infrastructure\Abstracts\Mappers\DefaultMapper;

/**
 * @Entity
 *
*/
class Department extends BaseCycleModel
{
    /** @Column(type= "string(50)") */
    public $name;

    /** @BelongsTo(target=Division::class) */
    public $division;
}
