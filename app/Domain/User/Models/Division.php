<?php


namespace App\Domain\User\Models;


use App\Infrastructure\Abstracts\BaseCycleModel;
use Cycle\Annotated\Annotation\Column;
use Cycle\Annotated\Annotation\Entity;

use App\Infrastructure\Abstracts\Constrains\NotDeletedConstrain;
use App\Infrastructure\Abstracts\Mappers\DefaultMapper;

/**
 * @Entity
 *
 */
class Division extends BaseCycleModel
{
    /** @Column(type= "string(50)") */
    public $name;
}
