<?php

namespace App\Domain\User\Policies;

use App\Domain\User\Models\User;
use App\Domain\User\Models\Department;
use Illuminate\Auth\Access\HandlesAuthorization;

class DepartmentPolicy
{
    use HandlesAuthorization;

    public function viewAny(?User $user): bool
    {
        return true;
    }

    public function view(?User $user, Department $model): bool
    {
        return true;
    }

    public function create(User $user): bool
    {
        return $user->can('CRUD departments');
    }

    public function update(User $user, Department $model): bool
    {
        return $user->can('CRUD departments');
    }

    public function delete(User $user, OldDepartment $model): bool
    {
        return $user->can('CRUD departments');
    }

    public function restore(User $user, Department $model): bool
    {
        return $user->can('force delete departments');
    }

    public function forceDelete(User $user, OldDepartment $model): bool
    {
        return $user->can('force delete departments');
    }
}
