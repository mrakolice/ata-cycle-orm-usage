<?php

namespace App\Domain\User\Policies;

use App\Domain\User\Models\User;
use App\Domain\User\Models\Division;
use Illuminate\Auth\Access\HandlesAuthorization;

class DivisionPolicy
{
    use HandlesAuthorization;

    public function viewAny(): bool
    {
        return true;
    }

    public function view(): bool
    {
        return true;
    }

    public function create(User $user): bool
    {
        return $user->can('CRUD divisions');
    }

    public function update(User $user): bool
    {
        return $user->can('CRUD divisions');
    }

    public function delete(User $user): bool
    {
        return $user->can('CRUD divisions');
    }

    public function restore(User $user): bool
    {
        return $user->can('force delete divisions');
    }

    public function forceDelete(User $user): bool
    {
        return $user->can('force delete divisions');
    }
}
