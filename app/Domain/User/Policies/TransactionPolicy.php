<?php

namespace App\Domain\User\Policies;

use App\Domain\User\Models\User;
use App\Domain\User\Models\Transaction;
use Illuminate\Auth\Access\HandlesAuthorization;

class TransactionPolicy
{
    use HandlesAuthorization;

    public function viewAny(?User $user): bool
    {
        return $user->can('view transactions');
    }

    public function view(?User $user, Transaction $model): bool
    {
        return $user->id === $model->sender_id ??
            $user->id === $model->getter_id ??
            $user->can('view transactions');
    }

    public function create(User $user, Transaction $model): bool
    {
        return $user->id === $model->getter_id ??
            $user->can('CRUD transactions');
    }

    public function update(User $user): bool
    {
        return $user->can('CRUD transactions');
    }

    public function delete(User $user): bool
    {
        return $user->can('CRUD transactions');
    }

    public function restore(User $user): bool
    {
        return $user->can('force delete transactions');
    }

    public function forceDelete(User $user): bool
    {
        return $user->can('force delete transactions');
    }
}
