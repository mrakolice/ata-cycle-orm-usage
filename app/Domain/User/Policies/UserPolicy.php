<?php

namespace App\Domain\User\Policies;

use App\Domain\User\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    public function viewAny(User $user): bool
    {
        return $user->can('view users');
    }

    public function view(User $user, User $model): bool
    {
        dd($user->toArray());
        return $user->id === $model->id ??
            $user->can('view users');
    }

    public function create(User $user): bool
    {
        return $user->can('create users');
    }

    public function update(User $user, User $model): bool
    {
        return $user->id === $model->id ??
            $user->can('update users');
    }

    public function delete(User $user, User $model): bool
    {
        return $user->id !== $model->id && $user->can('delete users');
    }

    public function restore(User $user, User $model): bool
    {
        return $user->id !== $model->id && $user->can('restore users');
    }

    public function forceDelete(User $user, User $model): bool
    {
        return $user->id !== $model->id && $user->can('force delete users');
    }
}
