<?php

namespace App\Domain\User\Policies;

use App\Domain\User\Models\User;
use App\Domain\User\Models\TimeTable;
use Illuminate\Auth\Access\HandlesAuthorization;

class TimeTablePolicy
{
    use HandlesAuthorization;

    public function viewAny(?User $user): bool
    {
        return $user->can('view users');
    }

    public function view(?User $user, TimeTable $model): bool
    {
        return $user->id === $model->owner_id ??
            $user->can('view users');
    }

    public function create(User $user): bool
    {
        return $user->can('CRUD users');
    }

    public function update(User $user): bool
    {
        return $user->can('CRUD users');
    }

    public function delete(User $user): bool
    {
        return $user->can('CRUD users');
    }

    public function restore(User $user): bool
    {
        return $user->can('force delete users');
    }

    public function forceDelete(User $user): bool
    {
        return $user->can('force delete users');
    }
}
