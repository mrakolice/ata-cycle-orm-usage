<?php

namespace App\Domain\User\Policies;

use App\Domain\User\Models\User;
use App\Domain\User\Models\Role;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;

    public function viewAny(?User $user): bool
    {
        return true;
    }

    public function view(?User $user): bool
    {
        return true;
    }

    public function create(User $user): bool
    {
        return $user->can('CRUD roles');
    }

    public function update(User $user): bool
    {
        return $user->can('CRUD roles');
    }

    public function delete(User $user): bool
    {
        return $user->can('CRUD roles');
    }

    public function restore(User $user): bool
    {
        return $user->can('force delete roles');
    }

    public function forceDelete(User $user): bool
    {
        return $user->can('force delete roles');
    }
}
