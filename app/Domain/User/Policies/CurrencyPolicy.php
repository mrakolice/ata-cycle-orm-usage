<?php

namespace App\Domain\User\Policies;

use App\Domain\User\Models\User;
use App\Domain\User\Models\Currency;
use Illuminate\Auth\Access\HandlesAuthorization;

class CurrencyPolicy
{
    use HandlesAuthorization;

    public function viewAny(): bool
    {
        return true;
    }

    public function view(): bool
    {
        return true;
    }

    public function create(User $user): bool
    {
        return $user->can('CRUD currencies');
    }

    public function update(User $user): bool
    {
        return $user->can('CRUD currencies');
    }

    public function delete(User $user): bool
    {
        return $user->can('CRUD currencies');
    }

    public function restore(User $user): bool
    {
        return $user->can('force delete currencies');
    }

    public function forceDelete(User $user): bool
    {
        return $user->can('force delete currencies');
    }
}
