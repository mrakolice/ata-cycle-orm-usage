<?php


namespace App\Core\Support\QueryBuilder\Filters;

use Spatie\QueryBuilder\Filters\Filter;
use Illuminate\Database\Eloquent\Builder;

class DeletedModelFilter implements Filter
{
    public function __invoke(Builder $query, $value, string $property)
    {
        $query->withTrashed();
    }
}
