<?php

declare(strict_types=1);

namespace App\Core\Http\Middleware\Traits;

use App\Domain\User\Models\User;
use Closure;

trait JwtMiddleware
{
    private function authIfDocumentation($request): void
    {
        if (app()->environment('documentation')) {
            if (auth()->guest()) {
                $token = auth()->login(User::whereLogin('admin')->firstOrFail());
                $this->setAuthenticationHeader($request, $token);
            }
        }
    }
}
