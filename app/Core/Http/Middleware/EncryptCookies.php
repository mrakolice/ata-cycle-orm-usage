<?php

declare(strict_types=1);

namespace App\Core\Http\Middleware;

use Illuminate\Cookie\Middleware\EncryptCookies as Middleware;

final class EncryptCookies extends Middleware
{
    /**
     * @var array
     */
    protected $except = [];
}
