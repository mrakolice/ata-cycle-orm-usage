<?php

declare(strict_types=1);

namespace App\Core\Http\Middleware;

use App\Core\Http\Middleware\Traits\JwtMiddleware;
use Closure;
use Tymon\JWTAuth\Http\Middleware\RefreshToken;

class JwtRefresh extends RefreshToken
{
    use JwtMiddleware;

    public function handle($request, Closure $next)
    {
        $this->authIfDocumentation($request);

        return parent::handle($request, $next);
    }
}
