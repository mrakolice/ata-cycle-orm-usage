<?php

declare(strict_types=1);

namespace App\Core\Http\Middleware;

use App\Core\Http\Middleware\Traits\JwtMiddleware;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;

class AddJwtAuthTokenToHeader extends BaseMiddleware
{
    use JwtMiddleware;

    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param \Closure $next
     * @return JsonResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $this->authIfDocumentation($request);

        $response = $next($request);

        if (auth()->check()) {
            $response->headers->set('Authorization', 'Bearer ' . auth('api')->refresh());
        }

        return $response;
    }
}
