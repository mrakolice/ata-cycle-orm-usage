<?php

declare(strict_types=1);

namespace App\Core\Exceptions;

use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;

final class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array<string>
     */
    protected $dontReport = [];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array<string>
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Render an exception into an HTTP response.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Exception $exceptionResponse
     *
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($request->expectsJson() || $request->isJson()) {
            $exceptionResponse = $this->prepareException($exception);

            if ($exception instanceof HttpResponseException) {
                $exceptionResponse = $exceptionResponse->getResponse();
            }

            if ($exception instanceof AuthenticationException) {
                $exceptionResponse = $this->unauthenticated($request, $exception);
            }

            if ($exception instanceof ValidationException) {
                $exceptionResponse = $this->convertValidationExceptionToResponse($exception, $request);
            }

            return $this->renderJson($exceptionResponse, $exception);
        }

        return parent::render($request, $exception);
    }

    /**
     * @param Exception $exceptionResponse
     *
     * @return \Illuminate\Http\Response
     */
    private function renderJson($exceptionResponse, Exception $exception): JsonResponse
    {
        $response = [];

        if (method_exists($exceptionResponse, 'getStatusCode')) {
            $response['status_code'] = $exceptionResponse->getStatusCode();
        } else {
            $response['status_code'] = 500;
        }

        switch ($response['status_code']) {
            case 401:
                $response['message'] = 'Unauthorized';
                break;
            case 403:
                $response['message'] = 'Forbidden';
                break;
            case 404:
                $response['message'] = 'Not Found';
                break;
            case 405:
                $response['message'] = 'Method Not Allowed';
                break;
            case 422:
                $response['message'] = $exceptionResponse->original['message'];
                $response['errors'] = $exceptionResponse->original['errors'];
                break;
            default:
                $response['message'] = ($response['status_code'] === 500) ? 'Whoops, looks like something went wrong' : $exceptionResponse->getMessage();
                break;
        }

        if (config('app.debug')) {
            $response['debug'] = self::getDebugInfo($exception);
        }

        return response()->json($response, $response['status_code']);
    }

    /**
     * @param Exception $exception
     * @param int $fallbackStatus
     *
     * @return int
     */
    private function getStatusCode(Exception $exception, int $fallbackStatus = 400): int
    {
        if ($this->isHttpException($exception)) {
            return $exception->getStatusCode();
        }

        return $fallbackStatus;
    }


    /**
     * @param Exception $exception
     *
     * @return array
     */
    private static function getDebugInfo(Exception $exception): array
    {
        return [
            'exception' => get_class($exception),
            'message' => $exception->getMessage(),
            'trace' => $exception->getTrace(),
        ];
    }
}
