<?php

namespace App\Core\Exceptions;

use Exception;

class ModelCannotUseQueryBuilderException extends Exception
{
//    /**
//     * Report the exception.
//     *
//     * @return void
//     */
//    public function report()
//    {
//
//    }

    /**
     * Render the exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request
     * @return \Illuminate\Http\Response
     */
    public function render()
    {
        return response([
            'message' => 'BaseModel has not enough functions.'
        ], 500);
    }
}
