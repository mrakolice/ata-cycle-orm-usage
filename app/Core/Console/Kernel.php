<?php

declare(strict_types=1);

namespace App\Core\Console;

use App\Core\Console\Commands;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

final class Kernel extends ConsoleKernel
{
    /**
     * @var array The Artisan commands provided by your application.
     */
    protected $commands = [
        Commands\DomainModelMake::class,
        Commands\DomainCommandMake::class,
        Commands\DomainControllerMake::class,
        Commands\DomainEventMake::class,
        Commands\DomainExceptionMake::class,
        Commands\DomainFactoryMake::class,
        Commands\DomainJobMake::class,
        Commands\DomainListenerMake::class,
        Commands\DomainMailMake::class,
        Commands\DomainMiddlewareMake::class,
        Commands\DomainMigrationMake::class,
        Commands\DomainNotificationMake::class,
        Commands\DomainObserverMake::class,
        Commands\DomainPolicyMake::class,
        Commands\DomainProviderMake::class,
        Commands\DomainRequestMake::class,
        Commands\DomainResourceMake::class,
        Commands\DomainRuleMake::class,
        Commands\DomainSeederMake::class,
        Commands\DomainTestMake::class,
        Commands\GenerateDocsCommand::class,

    ];

    protected function schedule(Schedule $schedule)
    {
        $schedule->command('telescope:prune --hours=48')->daily();
    }

    protected function commands()
    {
        // App\Console\Commands\ExampleCommand::class,
    }
}
