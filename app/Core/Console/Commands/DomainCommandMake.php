<?php

namespace App\Core\Console\Commands;

use App\Core\Console\Commands\Configuration\Constants;
use App\Core\Console\Commands\Traits\WithDomainOption;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Foundation\Console\ConsoleMakeCommand;
use Illuminate\Foundation\Console\ModelMakeCommand;

class DomainCommandMake extends ConsoleMakeCommand
{
    use WithDomainOption;

    private $domainOptionDescription = 'Generate command in required domain';

    public function __construct(Filesystem $files)
    {
        parent::__construct($files);

        $this->namespaceMap = app()->get('NamespaceMap');
        $this->configuration = $this->namespaceMap[Constants::COMMAND];
    }
}
