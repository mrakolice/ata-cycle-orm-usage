<?php


namespace App\Core\Console\Commands;


use App\Core\Console\Commands\Configuration\Constants;
use App\Core\Console\Commands\Traits\WithDomainOption;
use Illuminate\Console\GeneratorCommand;
use Illuminate\Database\Console\Seeds\SeederMakeCommand;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Composer;
use Symfony\Component\Console\Input\InputOption;

class DomainSeederMake extends SeederMakeCommand
{
    use WithDomainOption;

    private $domainOptionDescription = 'Generate seeder in required domain';

    public function __construct(Filesystem $files, Composer $composer)
    {
        parent::__construct($files, $composer);

        $this->namespaceMap = app()->get('NamespaceMap');
        $this->configuration = $this->namespaceMap[Constants::SEEDER];
    }

    protected function getNamespace($name)
    {
        return $this->getDefaultNamespace('App');
    }

    protected function getStub()
    {
        return app()->basePath() . '/resources/stubs/seeder.stub';
    }

    protected function getOptions()
    {
        return array_merge(
            parent::getOptions(),
            [
                ['domain', 'd', InputOption::VALUE_OPTIONAL, $this->domainOptionDescription],
                ['model', 'm', InputOption::VALUE_OPTIONAL, 'Model that used in seeder'],
            ]
        );
    }

    protected function buildClass($name)
    {
        $namespaceModel = $this->option('model')
            ? $this->namespaceMap[Constants::MODEL]->getNamespace(trim($this->rootNamespace(), '\\'), $this->option('domain')) . '\\' . $this->option('model')
            : '';

        // remove unused use
        $key = $this->option('model') ? 'NamespacedDummyModel' : 'use NamespacedDummyModel;';

        return str_replace(
            [
                $key,
            ],
            [
                $namespaceModel,
            ],
            GeneratorCommand::buildClass($name)
        );
    }
}
