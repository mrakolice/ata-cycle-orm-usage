<?php


namespace App\Core\Console\Commands;


use App\Core\Console\Commands\Configuration\Constants;
use App\Core\Console\Commands\Traits\WithDomainOption;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Foundation\Console\EventMakeCommand;

class DomainEventMake extends EventMakeCommand
{
    use WithDomainOption;

    private $domainOptionDescription = 'Generate event in required domain';


    public function __construct(Filesystem $files)
    {
        parent::__construct($files);

        $this->namespaceMap = app()->get('NamespaceMap');
        $this->configuration = $this->namespaceMap[Constants::EVENT];
    }
}
