<?php


namespace App\Core\Console\Commands;


use App\Core\Console\Commands\Configuration\Constants;
use App\Core\Console\Commands\Traits\WithDomainOption;
use Illuminate\Database\Console\Migrations\MigrateMakeCommand;
use Illuminate\Database\Migrations\MigrationCreator;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Composer;

class DomainMigrationMake extends MigrateMakeCommand
{
    use WithDomainOption;

    private $domainOptionDescription = 'Generate migration in required domain';

    protected $signature = 'make:migration {name : The name of the migration}
        {--create= : The table to be created}
        {--table= : The table to migrate}
        {--path= : The location where the migration file should be created}
        {--realpath : Indicate any provided migration file paths are pre-resolved absolute paths}
        {--fullpath : Output the full path of the migration}
        {--d|domain= : Generate migration in required domain}';

    private $files;

    public function __construct(MigrationCreator $creator, Composer $composer, Filesystem $files)
    {
        parent::__construct($creator, $composer);

        $this->namespaceMap = app()->get('NamespaceMap');
        $this->configuration = $this->namespaceMap[Constants::MIGRATION];
        $this->files = $files;
    }


    protected function getMigrationPath()
    {
        if (!$this->option('domain')) {
            return parent::getMigrationPath();
        }
        return $this->makeDirectory($this->configuration->getFullPath($this->option('domain')));
    }

    /**
     * Build the directory for the class if necessary.
     *
     * @param  string  $path
     * @return string
     */
    protected function makeDirectory($path)
    {
        if (! $this->files->isDirectory($path)) {
            $this->files->makeDirectory($path, 0777, true, true);
        }

        return $path;
    }
}
