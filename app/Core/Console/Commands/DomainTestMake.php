<?php


namespace App\Core\Console\Commands;

use App\Core\Console\Commands\Configuration\Constants;
use App\Core\Console\Commands\Traits\WithDomainOption;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Foundation\Console\TestMakeCommand;

class DomainTestMake extends TestMakeCommand
{
    use WithDomainOption;

    protected $signature = 'make:test {name : The name of the class} {--u|unit : Create a unit test}
        {--d|domain= : Generate migration in required domain}';

    public function __construct(Filesystem $files)
    {
        parent::__construct($files);

        $this->namespaceMap = app()->get('NamespaceMap');
        $this->configuration = $this->namespaceMap[Constants::TEST];
    }

}

