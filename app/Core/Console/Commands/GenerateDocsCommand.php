<?php

    namespace App\Core\Console\Commands;

    use App\User;
    use App\DripEmailer;
    use Illuminate\Console\Command;
    use Mni\FrontYAML\Parser;
    use Windwalker\Renderer\BladeRenderer;
    class GenerateDocsCommand extends Command
    {
        /**
         * The name and signature of the console command.
         *
         * @var string
         */
        protected $signature = 'docs';

        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Generate documentation with customized frontend';

        /**
         * Create a new command instance.
         *
         * @return void
         */
        public function __construct()
        {
            parent::__construct();
        }
        /**
         * Execute the console command.
         *
         * @return mixed
         */
        public function handle()
        {
            $this->info('⚙️ Generating documentation with custom frontend…');

            $folder = 'docs';
            $source_dir = "$folder/source";

            foreach ([
                // Avoid bug with regeneration
                // public_path("$folder/source/index.md"),
                public_path("$folder/source/.compare.md"),
                // If you remove one of them
                public_path("$folder/source/prepend.md"),
                public_path("$folder/source/append.md"),
            ] as $file) {
                if (is_file($file)) unlink($file);
            }

            // Remove all generated files
            if (is_dir(config('apidoc.output'))) $this->deleteDir(config('apidoc.output'));

            // Copy additional files from own sources to generated docs folder
            rcopy(resource_path('docs'), public_path('docs'));

            // Call original docs initiator
            $this->create(config('apidoc.output'));

            // Call original command
            \Artisan::call('apidoc:generate');

            // Render documentation HTML with customized templates

            /*
            This code is copy of `public function generate()` from `vendor/mpociot/documentarian/src/Documentarian.php`
            because `$renderer` contains `__DIR__` which pints to `vendor/mpociot/documentarian/src`
            but we want to point to `resources/$folder`
            so paths is customized
            */

            $parser = new Parser();

            $document = $parser->parse(file_get_contents(public_path("$source_dir/index.md")));

            $frontmatter = $document->getYAML();
            $html = $document->getContent();

            $renderer = new BladeRenderer(
                [resource_path("$folder/resources/views")],
                ['cache_path' => public_path("$source_dir/_tmp")]
            );

            // Parse and include optional include markdown files
            if (isset($frontmatter['includes'])) {
                foreach ($frontmatter['includes'] as $include) {
                    if (file_exists($include_file = public_path("$source_dir/includes/_$include.md"))) {
                        $document = $parser->parse(file_get_contents($include_file));
                        $html .= $document->getContent();
                    }
                }
            }

            $output = $renderer->render('index', [
                'page' => $frontmatter,
                'content' => $html,
            ]);

            // Upgrade layout to split page into sections
            foreach ([
                '/<p>NOTICE:\s(.*)<\/p>/i' => "<aside class='notice'>$1</aside>",
                '/<p>WARNING:\s(.*)<\/p>/i' => "<aside class='warning'>$1</aside>",
                '/<p>SUCCESS:\s(.*)<\/p>/i' => "<aside class='success'>$1</aside>",
                // Links with `href` starting with `http` will open in new tab
                '/(href=[\'"]http)/i' => "target=_blank $1",
                // Server-side rendering for Prism blocks
                '/<pre><code class=[\'"]language-(\w+)/i' => '<pre class="language-$1"><code class="language-$1"',
                // Highlight inline code
                '/<code>/i' => "<code class='language-js'>",
                // Disable this since it breaks Documentarian frontend
                // '/(<!-- START_([\d\w]+) -->)/i' => "<section id='block-$2'>$1",
                // '/(<!-- END_([\d\w]+) -->)/i' => '$1</section>',
            ] as $pattern => $replacement) {
                $output = preg_replace($pattern, $replacement, $output);
            }

            // Write output

            file_put_contents(public_path("$folder/index.html"), $output);

            // Log success
            $this->info('✅ Documentation with custom frontend is generated');

            return;
        }

        public static function deleteDir($dirPath) {
            if (is_dir($dirPath)) {
                $objects = scandir($dirPath);
                foreach ($objects as $object) {
                    if ($object != "." && $object !="..") {
                        if (filetype($dirPath . DIRECTORY_SEPARATOR . $object) == "dir") {
                            self::deleteDir($dirPath . DIRECTORY_SEPARATOR . $object);
                        } else {
                            unlink($dirPath . DIRECTORY_SEPARATOR . $object);
                        }
                    }
                }
                reset($objects);
                rmdir($dirPath);
            }
        }

        public function create($folder)
        {
            $folder = $folder . '/source';
            if (!is_dir($folder)) {
                mkdir($folder, 0777, true);
                mkdir($folder . '/../css');
                mkdir($folder . '/../js');
                mkdir($folder . '/includes');
                mkdir($folder . '/assets');
            }

            $dir = 'vendor/mpociot/documentarian/src';

            // copy stub files
            copy($dir . '/../resources/stubs/index.md', $folder . '/index.md');
            copy($dir . '/../resources/stubs/gitignore.stub', $folder . '/.gitignore');
            copy($dir . '/../resources/stubs/includes/_errors.md', $folder . '/includes/_errors.md');
            copy($dir . '/../resources/stubs/package.json', $folder . '/package.json');
            copy($dir . '/../resources/stubs/gulpfile.js', $folder . '/gulpfile.js');
            copy($dir . '/../resources/stubs/config.php', $folder . '/config.php');
            copy($dir . '/../resources/stubs/js/all.js', $folder . '/../js/all.js');
            copy($dir . '/../resources/stubs/css/style.css', $folder . '/../css/style.css');

            // copy resources
            rcopy($dir . '/../resources/images/', $folder . '/assets/images');
            rcopy($dir . '/../resources/js/', $folder . '/assets/js');
            rcopy($dir . '/../resources/stylus/', $folder . '/assets/stylus');
        }
    }
