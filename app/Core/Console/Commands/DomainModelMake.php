<?php

namespace App\Core\Console\Commands;

use App\Core\Console\Commands\Configuration\Constants;
use App\Core\Console\Commands\Traits\WithDomainOption;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Foundation\Console\ModelMakeCommand;
use Illuminate\Support\Str;

class DomainModelMake extends ModelMakeCommand
{
    use WithDomainOption;

    private $domainOptionDescription = 'Generate model in required domain';


    public function __construct(Filesystem $files)
    {
        parent::__construct($files);

        $this->namespaceMap = app()->get('NamespaceMap');
        $this->configuration = $this->namespaceMap[Constants::MODEL];
    }


    /**
     * Create a model factory for the model.
     *
     * @return void
     */
    protected function createFactory()
    {
        $factory = Str::studly(class_basename($this->argument('name')));

        $this->call('make:factory', [
            'name' => $factory,
            '--model' => $factory,
            '--domain' => $this->option('domain'),
        ]);
    }

    /**
     * Create a migration file for the model.
     *
     * @return void
     */
    protected function createMigration()
    {
        $table = Str::snake(Str::pluralStudly(class_basename($this->argument('name'))));

        if ($this->option('pivot')) {
            $table = Str::singular($table);
        }

        $this->call('make:migration', [
            'name' => "create_{$table}_table",
            '--create' => $table,
            '--domain' => $this->option('domain'),
        ]);
    }

    /**
     * Create a controller for the model.
     *
     * @return void
     */
    protected function createController()
    {
        $controller = Str::studly(class_basename($this->argument('name')));

        $modelName = $this->qualifyClass($this->getNameInput());

        $this->call('make:controller', [
            'name' => $controller,
            '--model' => $this->option('resource') ? $controller : null,
            '--domain' => $this->option('domain'),
        ]);
    }
}
