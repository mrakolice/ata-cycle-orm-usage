<?php


namespace App\Core\Console\Commands\Traits;


use Symfony\Component\Console\Input\InputOption;

trait WithDomainOption
{
    private $namespaceMap;
    private $configuration;

    protected function getOptions()
    {
        return array_merge(
            parent::getOptions(),
            [
                ['domain', 'd', InputOption::VALUE_OPTIONAL, $this->domainOptionDescription],
            ]
        );
    }


    protected function getStub()
    {
        return $this->configuration->stub
            ? app()->basePath() . "/resources/stubs/{$this->configuration->stub}"
            : parent::getStub();
    }

    protected function getDefaultNamespace($rootNamespace)
    {
        if (!$this->option('domain')) {
            return parent::getDefaultNamespace($rootNamespace);
        }
        return $this->configuration->getNamespace($rootNamespace, $this->option('domain'));
    }

    protected function getPath($name)
    {
        return $this->configuration->getFullPath($this->option('domain'), $this->argument('name'));
    }

    protected function replaceClass($stub, $name){
        $model = class_basename($name);

        if ($this->configuration->postfix && !strpos($model, $this->configuration->postfix)) {
            $name .= $this->configuration->postfix;
        }

        return parent::replaceClass($stub, $name);
    }

    protected function alreadyExists($rawName)
    {
        if (!$this->option('domain')) {
            return parent::alreadyExists($rawName);
        }
        return class_exists($this->getDefaultNamespace($this->rootNamespace()).$rawName);
    }

}
