<?php


namespace App\Core\Console\Commands;


use App\Core\Console\Commands\Configuration\Constants;
use App\Core\Console\Commands\Traits\WithDomainOption;
use Illuminate\Console\GeneratorCommand;
use Illuminate\Database\Console\Factories\FactoryMakeCommand;
use Illuminate\Filesystem\Filesystem;

class DomainFactoryMake extends FactoryMakeCommand
{
    use WithDomainOption;

    private $domainOptionDescription = 'Generate factory in required domain';

    public function __construct(Filesystem $files)
    {
        parent::__construct($files);
        $this->namespaceMap = app()->get('NamespaceMap');
        $this->configuration = $this->namespaceMap[Constants::FACTORY];
    }

    protected function buildClass($name)
    {
        $namespaceModel = $this->option('model')
            ? $this->namespaceMap[Constants::MODEL]->getNamespace(trim($this->rootNamespace(), '\\'), $this->option('domain')) . '\\' . $this->option('model')
            : '';

        // remove unused use
        $key = $this->option('model') ? 'NamespacedDummyModel' : 'use NamespacedDummyModel;';


        $model = class_basename($namespaceModel);

        return str_replace(
            [
                $key,
                'DummyModel',
            ],
            [
                $namespaceModel,
                $model,
            ],
            GeneratorCommand::buildClass($name)
        );
    }
}
