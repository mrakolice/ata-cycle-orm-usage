<?php


namespace App\Core\Console\Commands\Configuration;


final class Constants
{
    const DOMAIN_PREFIX = 'Domain';

    const COMMAND = 'command';
    const COMMAND_PREFIX = 'Console\\Commands';
    const COMMAND_POSTFIX = 'Command';

    const CONTROLLER = 'controller';
    const CONTROLLER_PREFIX = 'Http\\Controllers';
    const CONTROLLER_POSTFIX = 'Controller';

    const EVENT = 'event';
    const EVENT_PREFIX = 'Events';
    const EVENT_POSTFIX = 'Event';

    const EXCEPTION = 'exception';
    const EXCEPTION_PREFIX = 'Exceptions';
    const EXCEPTION_POSTFIX = 'Exception';

    const FACTORY = 'factory';
    const FACTORY_PREFIX = 'Database\\TestFactories';
    const FACTORY_POSTFIX = 'Factory';
    const FACTORY_STUB = 'factory.stub';

    const JOB = 'job';
    const JOB_PREFIX = 'Jobs';
    const JOB_POSTFIX = 'Job';

    const LISTENER = 'listener';
    const LISTENER_PREFIX = 'Listeners';
    const LISTENER_POSTFIX = 'Listener';

    const MAIL = 'mail';
    const MAIL_PREFIX = 'Mails';
    const MAIL_POSTFIX = 'Mail';

    const MIDDLEWARE = 'middleware';
    const MIDDLEWARE_PREFIX = 'Http\\Middleware';
    const MIDDLEWARE_POSTFIX = 'Middleware';

    const MIGRATION = 'migration';
    const MIGRATION_PREFIX = 'Database\\Migrations';
    const MIGRATION_POSTFIX = 'Migration';

    const MODEL = 'model';
    const MODEL_PREFIX = 'OldModels';
    const MODEL_POSTFIX = '';

    const NOTIFICATION = 'notification';
    const NOTIFICATION_PREFIX = 'Notifications';
    const NOTIFICATION_POSTFIX = 'Notification';

    const OBSERVER = 'observer';
    const OBSERVER_PREFIX = 'Observers';
    const OBSERVER_POSTFIX = 'Observer';

    const POLICY = 'policy';
    const POLICY_PREFIX = 'Policies';
    const POLICY_POSTFIX = 'Policy';

    const PROVIDER = 'provider';
    const PROVIDER_PREFIX = 'Providers';
    const PROVIDER_POSTFIX = 'Provider';

    const REQUEST = 'request';
    const REQUEST_PREFIX = 'Requests';
    const REQUEST_POSTFIX = 'Request';
    const REQUEST_UPDATE = 'Update';
    const REQUEST_CREATE = 'Create';

    const RESOURCE = 'resource';
    const RESOURCE_PREFIX = 'Resources';
    const RESOURCE_POSTFIX = 'Resource';

    const RULE = 'rule';
    const RULE_PREFIX = 'Rules';
    const RULE_POSTFIX = 'Rule';

    const SEEDER = 'seeder';
    const SEEDER_PREFIX = 'Database\\Seeds';
    const SEEDER_POSTFIX = 'Seeder';
    const SEEDER_STUB = 'seeder.stub';

    const TEST = 'test';
    const TEST_PREFIX = 'Tests';
    const TEST_POSTFIX = 'Test';

}
