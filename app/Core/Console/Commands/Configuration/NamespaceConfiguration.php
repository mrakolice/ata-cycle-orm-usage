<?php


namespace App\Core\Console\Commands\Configuration;


class NamespaceConfiguration
{
    public $prefix;
    public $postfix;
    public $stub;

    public function withPrefix($prefix)
    {
        $this->prefix = $prefix;

        return $this;
    }

    public function withPostfix($postfix)
    {
        $this->postfix = $postfix;

        return $this;
    }

    public function withStub($stub)
    {
        $this->stub = $stub;

        return $this;
    }


    public function getNamespace($rootNamespace, $domain)
    {
        return $rootNamespace . '\\' . Constants::DOMAIN_PREFIX . '\\' . $domain . '\\' . $this->prefix;
    }

    public function getFullPath($domain, $name = null, $prefix = null)
    {
        $basePath = app()->basePath() . DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . Constants::DOMAIN_PREFIX;
        $prefix = $prefix ? str_replace('\\', DIRECTORY_SEPARATOR, $prefix) : $prefix;
        $prefix = $this->prefix ? str_replace('\\', DIRECTORY_SEPARATOR, $this->prefix) . DIRECTORY_SEPARATOR . $prefix : $prefix;

        if ($name) {
            $name = str_replace('\\', DIRECTORY_SEPARATOR, $name);

            if ($this->postfix && !strpos($name, $this->postfix)) {
                $name .= $this->postfix;
            }

            $name = DIRECTORY_SEPARATOR . $name . '.php';
        }


        return $basePath . DIRECTORY_SEPARATOR . $domain . DIRECTORY_SEPARATOR . $prefix . $name;
    }
}
