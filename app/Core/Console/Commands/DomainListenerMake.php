<?php


namespace App\Core\Console\Commands;


use App\Core\Console\Commands\Configuration\Constants;
use App\Core\Console\Commands\Traits\WithDomainOption;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Foundation\Console\ListenerMakeCommand;

class DomainListenerMake extends ListenerMakeCommand
{
    use WithDomainOption;

    private $domainOptionDescription = 'Generate listeners in required domain';
    private $namespace = 'Listeners';

    public function __construct(Filesystem $files)
    {
        parent::__construct($files);

        $this->namespaceMap = app()->get('NamespaceMap');
        $this->configuration = $this->namespaceMap[Constants::LISTENER];
    }
}
