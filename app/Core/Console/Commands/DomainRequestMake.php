<?php


namespace App\Core\Console\Commands;


use App\Core\Console\Commands\Configuration\Constants;
use App\Core\Console\Commands\Traits\WithDomainOption;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Foundation\Console\RequestMakeCommand;
use Symfony\Component\Console\Input\InputOption;

class DomainRequestMake extends RequestMakeCommand
{
    use WithDomainOption;

    private $domainOptionDescription = 'Generate request in required domain';

    public function __construct(Filesystem $files)
    {
        parent::__construct($files);

        $this->namespaceMap = app()->get('NamespaceMap');
        $this->configuration = $this->namespaceMap[Constants::REQUEST];
    }

    protected function getOptions()
    {
        return array_merge(
            parent::getOptions(),
            [
                ['domain', 'd', InputOption::VALUE_OPTIONAL, $this->domainOptionDescription],
                ['model', 'm', InputOption::VALUE_NONE, 'Generate create and update requests in folder with name as model'],
            ]
        );
    }

    public function handle()
    {
        if ($this->option('model')) {
            $this->generateRequestFile(Constants::REQUEST_CREATE . $this->getNameInput());
            $this->generateRequestFile(Constants::REQUEST_UPDATE . $this->getNameInput());
        } else {
            parent::handle();
        }
    }

    protected function getPath($name)
    {
        if ($this->option('model')) {
            return $this->configuration->getFullPath($this->option('domain'), $name, $this->getNameInput());
        }

        return $this->configuration->getFullPath($this->option('domain'), $name);
    }

    /**
     * Execute the console command.
     *
     * @return bool|null
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    private function generateRequestFile($name)
    {
        $path = $this->getPath($name);

        // First we will check to see if the class already exists. If it does, we don't want
        // to create the class and overwrite the user's code. So, we will bail out so the
        // code is untouched. Otherwise, we will continue generating this class' files.
        if ((!$this->hasOption('force') ||
                !$this->option('force')) &&
            $this->alreadyExists($this->getNameInput())) {
            $this->error($this->type . ' already exists!');

            return false;
        }

        // Next, we will generate the path to the location where this class' file should get
        // written. Then, we will build the class and make the proper replacements on the
        // stub files so that it gets the correctly formatted namespace and class name.
        $this->makeDirectory($path);

        $this->files->put($path, $this->sortImports($this->buildClass($name)));

        $this->info($this->type . ' created successfully.');
    }
}
