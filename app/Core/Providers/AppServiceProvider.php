<?php

declare(strict_types=1);

namespace App\Core\Providers;

use App\Core\Console\Commands\Configuration\Constants as NamespaceConstants;
use App\Core\Console\Commands\Configuration\NamespaceConfiguration;
use Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider;
use Faker;
use Illuminate\Session\SessionServiceProvider;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\ServiceProvider;
use Laravel\Telescope\TelescopeServiceProvider as VendorTelescopeServiceProvider;

final class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Redis::enableEvents();
    }

    public function register()
    {
        if ($this->app->isLocal()) {
            $this->app->register(IdeHelperServiceProvider::class);
            $this->app->register(SessionServiceProvider::class);
            $this->app->register(VendorTelescopeServiceProvider::class);
            $this->app->register(TelescopeServiceProvider::class);
            $this->app->singleton('NamespaceMap', function($app){
                return [
                    NamespaceConstants::COMMAND => (new NamespaceConfiguration())
                        ->withPrefix(NamespaceConstants::COMMAND_PREFIX)
                        ->withPostfix(NamespaceConstants::COMMAND_POSTFIX),

                    NamespaceConstants::CONTROLLER => (new NamespaceConfiguration())
                        ->withPrefix(NamespaceConstants::CONTROLLER_PREFIX)
                        ->withPostfix(NamespaceConstants::CONTROLLER_POSTFIX),

                    NamespaceConstants::EVENT => (new NamespaceConfiguration())
                        ->withPrefix(NamespaceConstants::EVENT_PREFIX)
                        ->withPostfix(NamespaceConstants::EVENT_POSTFIX),

                    NamespaceConstants::EXCEPTION => (new NamespaceConfiguration())
                        ->withPrefix(NamespaceConstants::EXCEPTION_PREFIX)
                        ->withPostfix(NamespaceConstants::EXCEPTION_POSTFIX),

                    NamespaceConstants::FACTORY => (new NamespaceConfiguration())
                        ->withPrefix(NamespaceConstants::FACTORY_PREFIX)
                        ->withPostfix(NamespaceConstants::FACTORY_POSTFIX)
                        ->withStub(NamespaceConstants::FACTORY_STUB),

                    NamespaceConstants::JOB => (new NamespaceConfiguration())
                        ->withPrefix(NamespaceConstants::JOB_PREFIX)
                        ->withPostfix(NamespaceConstants::JOB_POSTFIX),

                    NamespaceConstants::LISTENER => (new NamespaceConfiguration())
                        ->withPrefix(NamespaceConstants::LISTENER_PREFIX)
                        ->withPostfix(NamespaceConstants::LISTENER_POSTFIX),

                    NamespaceConstants::MAIL => (new NamespaceConfiguration())
                        ->withPrefix(NamespaceConstants::MAIL_PREFIX)
                        ->withPostfix(NamespaceConstants::MAIL_POSTFIX),

                    NamespaceConstants::MIDDLEWARE => (new NamespaceConfiguration())
                        ->withPrefix(NamespaceConstants::MIDDLEWARE_PREFIX)
                        ->withPostfix(NamespaceConstants::MIDDLEWARE_POSTFIX),

                    NamespaceConstants::MIGRATION => (new NamespaceConfiguration())
                        ->withPrefix(NamespaceConstants::MIGRATION_PREFIX)
                        ->withPostfix(NamespaceConstants::MIGRATION_POSTFIX),

                    NamespaceConstants::MODEL => (new NamespaceConfiguration())
                        ->withPrefix(NamespaceConstants::MODEL_PREFIX)
                        ->withPostfix(NamespaceConstants::MODEL_POSTFIX),

                    NamespaceConstants::NOTIFICATION => (new NamespaceConfiguration())
                        ->withPrefix(NamespaceConstants::NOTIFICATION_PREFIX)
                        ->withPostfix(NamespaceConstants::NOTIFICATION_POSTFIX),

                    NamespaceConstants::OBSERVER => (new NamespaceConfiguration())
                        ->withPrefix(NamespaceConstants::OBSERVER_PREFIX)
                        ->withPostfix(NamespaceConstants::OBSERVER_POSTFIX),

                    NamespaceConstants::POLICY => (new NamespaceConfiguration())
                        ->withPrefix(NamespaceConstants::POLICY_PREFIX)
                        ->withPostfix(NamespaceConstants::POLICY_POSTFIX),

                    NamespaceConstants::PROVIDER => (new NamespaceConfiguration())
                        ->withPrefix(NamespaceConstants::PROVIDER_PREFIX)
                        ->withPostfix(NamespaceConstants::PROVIDER_POSTFIX),

                    NamespaceConstants::REQUEST => (new NamespaceConfiguration())
                        ->withPrefix(NamespaceConstants::REQUEST_PREFIX)
                        ->withPostfix(NamespaceConstants::REQUEST_POSTFIX),

                    NamespaceConstants::RESOURCE => (new NamespaceConfiguration())
                        ->withPrefix(NamespaceConstants::RESOURCE_PREFIX)
                        ->withPostfix(NamespaceConstants::RESOURCE_POSTFIX),

                    NamespaceConstants::RULE => (new NamespaceConfiguration())
                        ->withPrefix(NamespaceConstants::RULE_PREFIX)
                        ->withPostfix(NamespaceConstants::RULE_POSTFIX),

                    NamespaceConstants::SEEDER => (new NamespaceConfiguration())
                        ->withPrefix(NamespaceConstants::SEEDER_PREFIX)
                        ->withPostfix(NamespaceConstants::SEEDER_POSTFIX)
                        ->withStub(NamespaceConstants::SEEDER_STUB),

                    NamespaceConstants::TEST => (new NamespaceConfiguration())
                        ->withPrefix(NamespaceConstants::TEST_PREFIX)
                        ->withPostfix(NamespaceConstants::TEST_POSTFIX),
                ];

            });
        }
    }
}
