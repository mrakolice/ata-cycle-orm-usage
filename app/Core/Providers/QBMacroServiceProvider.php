<?php

declare(strict_types=1);

namespace App\Core\Providers;

use App\Infrastructure\Contracts\QueryBuilderContract;
use App\Infrastructure\Services\QueryBuilderHelper;
use App\Core\Exceptions\ModelCannotUseQueryBuilderException;
use Spatie\QueryBuilder\QueryBuilder;
use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Eloquent\Builder;

final class QBMacroServiceProvider extends ServiceProvider
{
    public function boot()
    {
        Builder::macro('queryBuilder', function ($request = null): QueryBuilder {
            /** @var Builder $builder */
            $builder = $this;
            if ($builder->getModel() instanceof QueryBuilderContract) {
                return QueryBuilder::for($builder, $request);
            }

            throw new ModelCannotUseQueryBuilderException();
        });

        Builder::macro('qBOne', function (): QueryBuilder {
            /** @var Builder $builder */
            $builder = $this;
            $helper = new QueryBuilderHelper($builder);
            /** @var QueryBuilderContract $model */
            $model = $builder->getModel();
            $queryBuilder = $builder->queryBuilder($helper->getRequest());

            return $queryBuilder
                ->when($helper->getParsedFields(), static function (QueryBuilder $query, $parsedFields) {
                    return $query->allowedFields($parsedFields);
                })
                ->when($model->getAllowedIncludes(), static function (QueryBuilder $query, $allowedIncludes) {
                    return $query->allowedIncludes($allowedIncludes);
                })
                ->when($model->getAllowedFilters(), static function (QueryBuilder $query, $allowedFilters) {
                    return $query->allowedFilters($allowedFilters);
                });
        });

        Builder::macro('qBMany', function (): QueryBuilder {
            /** @var Builder $builder */
            $builder = $this;
            /** @var QueryBuilderContract $model */
            $model = $builder->getModel();

            return $builder->qBOne()
                ->when($model->getDefaultSorts(), static function (QueryBuilder $query, $defaultSorts) {
                    return $query->defaultSort($defaultSorts);
                })
                ->when($model->getAllowedSorts(), static function (QueryBuilder $query, $allowedSorts) {
                    return $query->allowedSorts($allowedSorts);
                });
        });

        Builder::macro('qBGet', function () {
            /** @var Builder $builder */
            $builder = $this;

            return $builder->when((int)(request()->per_page ?? request()->limit),
                function (Builder $query, $perPage) {
                    return $query->paginate($perPage);
                },
                function (Builder $query) {
                    return $query->get();
                }
            );
        });
    }
}
