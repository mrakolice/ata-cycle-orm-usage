# Contributing into Academata

## Documentation

There is auto-generated documentation available on `/docs/` from web browser.

To build it, you should run `php artisan docs` command from `app` container.

To add a section into documentation, you have to use [PHPDoc](https://ru.wikipedia.org/wiki/PHPDoc)

You can start string with `NOTICE: `, `SUCCESS: ` or `WARNING: ` to draw attention to something.
Blocks like this will be shown as full-width blocks with contrast background starting with an icon.

Example:

```php
/**
 * Method title
 *
 * Method description
 *
 * NOTICE: Draw attention here
 *
 * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
 */
```

> You should use 2 or more empty strings to have adjacent asides. Example:

```php
 * SUCCESS: Doing like this is ok
 *
 *
 * WARNING: You should not do something
```
