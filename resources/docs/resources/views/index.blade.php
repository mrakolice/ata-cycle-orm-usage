<!doctype html>
<html>
	<head>
		<meta charset='utf-8'>
		<meta content='IE=edge,chrome=1' http-equiv='X-UA-Compatible'>
		<meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1'>
		<title>{{$page['title']}}</title>
		<link rel='stylesheet' href='/docs/css/style.css'/>
		<link rel='stylesheet' href='/assets/docs/add.css'/>
		<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/prism/1.17.1/themes/prism-tomorrow.min.css'/>
	</head>
	<body>
		<div class='tocify-wrapper'>
			<img src='/assets/docs/logo.svg'/>
			@if(isset($page['language_tabs']))
				<div class='lang-selector'>
					@foreach($page['language_tabs'] as $lang)
						<a href='#' data-language-name='{{$lang}}'>{{$lang}}</a>
					@endforeach
				</div>
			@endif
			@if(isset($page['search']))
				<div class='search'>
					<input type='text' class='search' id='input-search' placeholder='Search'>
				</div>
				<ul class='search-results'></ul>
			@endif
			<div id='toc' class='tocify'></div>
			@if(isset($page['toc_footers']))
				<ul class='toc-footer'>
					@foreach($page['toc_footers'] as $link)
						<li>{!! $link !!}</li>
					@endforeach
				</ul>
			@endif
		</div>
		<div class='page-wrapper'>
			<div class='dark-box'></div>
			<div class='content'>
				{!! $content !!}
			</div>
			<div class='dark-box'>
				@if(isset($page['language_tabs']))
					<div class='lang-selector'>
					@foreach($page['language_tabs'] as $lang)
							<a href='#' data-language-name='{{$lang}}'>{{$lang}}</a>
						@endforeach
					</div>
				@endif
			</div>
		</div>
		@if(isset($page['language_tabs']))
			<script id="docs-init">
				(function(scope){
					window[scope] = {
						languages: JSON.parse('{!! json_encode($page['language_tabs']) !!}'),
					}
				})('docs')
			</script>
		@endif
		<script src='/docs/source/assets/js/lib/jquery.min.js'></script>
		<script src='/docs/source/assets/js/lib/energize.js'></script>
		<script src='/docs/source/assets/js/lib/imagesloaded.min.js'></script>
		<script src='/docs/source/assets/js/lib/jquery.highlight.js'></script>
		<script src='/docs/source/assets/js/lib/jquery_ui.js'></script>
		<script src='/docs/source/assets/js/lib/jquery.tocify.js'></script>
		<script src='/docs/source/assets/js/lib/lunr.js'></script>
		<script src='https://cdnjs.cloudflare.com/ajax/libs/prism/1.17.0/components/prism-core.min.js'></script>
		<script src='https://cdnjs.cloudflare.com/ajax/libs/prism/1.17.1/components/prism-clike.min.js'></script>
		<script src='https://cdnjs.cloudflare.com/ajax/libs/prism/1.17.1/components/prism-javascript.min.js'></script>
		<script src='https://cdnjs.cloudflare.com/ajax/libs/prism/1.17.1/plugins/autoloader/prism-autoloader.min.js'></script>
		<script src='/assets/docs/script.js'></script>
	</body>
</html>
