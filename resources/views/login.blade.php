<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
	<head>
		<title>ATA API – Вход</title>
		<meta charset='utf-8'>
		<meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no'>
		<meta name='csrf-token' content='{{ csrf_token() }}'>
		<link rel='stylesheet' href='/assets/login.css'>
	</head>
	<body>
		<form action='{{ url('admin/login') }}' method='POST' class='form'>
			@csrf
			<div class='cell'>
				<input
					type='text'
					name='username'
					id='username'
					class='input'
					placeholder='username'
					required
				>
				<label for='username' class='label'>Логин</label>
			</div>
			@if ($errors->has('username') ?? $errors )
				<div class='cell'>
					<span class='alert alert_error'>
						{{ $errors->first('username') ?? '' }}
					</span>
				</div>
			@endif
			<div class='cell'>
				<input
					type='password'
					name='password'
					id='password'
					class='input'
					placeholder='•••••'
					required
				>
				<label for='password' class='label'>Пароль</label>
			</div>
			@if ($errors->has('password'))
				<div class='cell'>
					<span class='alert alert_error'>
						{{ $errors->first('password') }}
					</span>
				</div>
			@endif
			<div class='cell cell_button'>
				<button type='submit' class='button'>
					{{ 'Войти' }}
				</button>
			</div>
		</form>
	</body>
</html>
