<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>{{ config('app.name') }} - {{ __('messages.dashboard') }}</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--Bootsrap 4 CDN-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
<div class="container-fluid">
    <div class="row no-gutter">
        <a href="{{url('admin/horizon')}}">Horizon</a>
        <a href="{{url('admin/telescope')}}">Telescope</a>
        <a href="{{url('admin/wiki')}}">WiKi</a>
    </div>
    <div class="row no-gutter">
        <a href="{{url('docs/')}}">API documentation</a>
    </div>
</div>

</body>
</html>
