title: Academata API Reference

language_tabs:
@foreach($settings['languages'] as $language)
- {{ $language }}
@endforeach

includes:

search: true

toc_footers:
- Generated on <time data-shift-timezone>{{ \Carbon\Carbon::now()->toRfc2822String() }}</time>

---

# Introduction

Welcome to the Academata API! You can use our API to access Academata API endpoints, which can get information on various things in our database.

We have language bindings in JavaScript (Fetch API) and [`axios`](https://github.com/axios/axios)! You can view code examples in the dark area to the right, and you can switch the examples with the tabs in the top right.
