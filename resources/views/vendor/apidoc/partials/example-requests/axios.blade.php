```axios
@php
	// There can be a list of methods on same route
	foreach($route['methods'] as $_method) {
		$method = strtolower($_method);
		$url = $route['boundUri'];
		$body = $route['cleanBodyParameters'];
		$query = $route['cleanQueryParameters'];
		switch($_method) {
			case 'GET':
			case 'DELETE':
			case 'HEAD':
			case 'OPTIONS':
				echo 'axios.' . $method . '(' . "'$url'";
					if ($query) {
						echo ", {";
						echo "\n	params: {\n";
						foreach($query as $key => $value) {
						    $value = json_encode($value);
							echo "		$key: '$value',\n";
						}
						echo "	}\n";
						echo '})';
					}
					else {
						echo ')';
					}
				break;
			case 'POST':
			case 'PUT':
			case 'PATCH':
				echo 'axios.' . $method . '(' . "'$url'";
					if(count($body) > 0 && count($query) > 0) {
						// body and query
						echo ", {\n";
						foreach($body as $key => $value) {
							echo "	$key: '$value',\n";
						}
						echo "}, {\n	params: {\n";
						foreach($query as $key => $value) {
							echo "		$key: '$value',\n";
						}
						echo "	},\n";
						echo '})';
					}
					elseif(count($body) > 0) {
						// no query, only body
						echo ", {\n";
						foreach($body as $key => $value) {
							echo "	$key: '$value',\n";
						}
						echo '})';
					}
					elseif(count($query) > 0) {
						// no body, only query
						echo ", {\n";
						echo "	params: {\n";
						foreach($query as $key => $value) {
							echo "		$key: '$value',\n";
						}
						echo "	}\n";
						echo '})';
					}
					else {
						echo ')';
					}
				break;
			default:
				// custom method
				echo "axios({\n";
				echo "	method: '$method',\n";
				echo "	url: '$url',\n";
				if($body) {
					echo "	data: {\n";
					foreach($body as $key => $value) {
						echo "		$key: '$value',\n";
					}
					echo "	},\n";
				}
				if($query) {
					echo "	params: {\n";
					foreach($query as $key => $value) {
						echo "		$key: '$value',\n";
					}
					echo "	},\n";
				}
				echo '})';
		}
		echo "
	.then(response => {
		// handle success
		console.log(response);
	})
";
		if(count($route['methods']) == 1) {
			// if there is a list of methods, let's try to save some vertical space
			echo
'	.catch(error => {
		// handle error
		console.log(error);
	})
	.finally(() => {
		// always executed
	});';
		}
	}
@endphp

```
