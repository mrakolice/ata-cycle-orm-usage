/*
 Copyright 2016 Marcel Pociot
 Copyright 2008-2013 Concur Technologies, Inc.

 Licensed under the Apache License, Version 2.0 (the "License") you may
 not use this file except in compliance with the License. You may obtain
 a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 License for the specific language governing permissions and limitations
 under the License.
 */
const scopeName = 'docs'
document.addEventListener('DOMContentLoaded', () => {
    init(window, scopeName)
    search()
    highlightMarkdownFencedBlocksCode()
    shiftTimezone()
})
function init(global, scopeName) {
    'use strict'
    // hljs.initHighlightingOnLoad()
    const languages = window[scopeName].languages
    setupLanguages(languages)
    function activateLanguage(language) {
        if (!language) return
        if (language === '') return
        $('.lang-selector a').removeClass('active')
        $('.lang-selector a[data-language-name=\'' + language + '\']').addClass('active')
        languages.forEach(name => {
            $(`code[class*=language-${name}]`).parent().css({ display: 'none' })
        })
        $('code.language-' + language).parent().css({ display: 'block' })
        document.addEventListener('DOMContentLoaded', () => {
            global.toc.calculateHeights()
        })
        // scroll to the new location of the position
        if ($(window.location.hash).get(0)) {
            $(window.location.hash).get(0).scrollIntoView(true)
        }
    }
    // parseURL and stringifyURL are from https://github.com/sindresorhus/query-string
    // MIT licensed
    // https://github.com/sindresorhus/query-string/blob/7bee64c16f2da1a326579e96977b9227bf6da9e6/license
    function parseURL(str) {
        if (typeof str !== 'string') {
            return {}
        }
        str = str.trim().replace(/^(\?|#|&)/, '')
        if (!str) {
            return {}
        }
        return str.split('&').reduce(function (ret, param) {
            const parts = param.replace(/\+/g, ' ').split('=')
            let key = parts[0]
            let val = parts[1]
            key = decodeURIComponent(key)
            // missing `=` should be `null`:
            // http://w3.org/TR/2012/WD-url-20120524/#collect-url-parameters
            val = val === undefined ? null : decodeURIComponent(val)
            if (!ret.hasOwnProperty(key)) {
                ret[key] = val
            } else if (Array.isArray(ret[key])) {
                ret[key].push(val)
            } else {
                ret[key] = [ret[key], val]
            }
            return ret
        }, {})
    }
    function stringifyURL(obj) {
        return obj ? Object.keys(obj).sort().map(function (key) {
            const val = obj[key]
            if (Array.isArray(val)) {
                return val.sort().map(function (val2) {
                    return encodeURIComponent(key) + '=' + encodeURIComponent(val2)
                }).join('&')
            }
            return encodeURIComponent(key) + '=' + encodeURIComponent(val)
        }).join('&') : ''
    }
    // gets the language set in the query string
    function getLanguageFromQueryString() {
        if (location.search.length >= 1) {
            const language = parseURL(location.search).language
            if (language) {
                return language
            } else if (jQuery.inArray(location.search.substr(1), languages) !== -1) {
                return location.search.substr(1)
            }
        }
        return false
    }
    // returns a new query string with the new language in it
    function generateNewQueryString(language) {
        const url = parseURL(location.search)
        if (url.language) {
            url.language = language
            return stringifyURL(url)
        }
        return language
    }
    // if a button is clicked, add the state to the history
    function pushURL(language) {
        if (!history) {
            return
        }
        let hash = window.location.hash
        if (hash) {
            hash = hash.replace(/^#+/, '')
        }
        history.pushState({}, '', '?' + generateNewQueryString(language) + '#' + hash)
        // save language as next default
        localStorage.setItem('language', language)
    }
    function setupLanguages(list) {
        const defaultLanguage = localStorage.getItem('language')
        const languages = list
        const presetLanguage = getLanguageFromQueryString()
        if (presetLanguage) {
            // the language is in the URL, so use that language!
            activateLanguage(presetLanguage)
            localStorage.setItem('language', presetLanguage)
        } else if ((defaultLanguage !== null) && (jQuery.inArray(defaultLanguage, languages) !== -1)) {
            // the language was the last selected one saved in localstorage, so use that language!
            activateLanguage(defaultLanguage)
        } else {
            // no language selected, so use the default
            activateLanguage(languages[0])
        }
    }
    function slugify(text) {
        return text.toString().toLowerCase()
        .replace(/\s+/g, '-')           // Replace spaces with -
        .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
        .replace(/\-\-+/g, '-')         // Replace multiple - with single -
        .replace(/^-+/, '')             // Trim - from start of text
        .replace(/-+$/, '')            // Trim - from end of text
    }
    // if we click on a language tab, activate that language
    $(function () {
        $('h1, h2').each(function () {
            $(this).prop('id', slugify($(this).text()))
        })
        $('.lang-selector a').on('click', function () {
            const language = $(this).data('language-name')
            pushURL(language)
            activateLanguage(language)
            return false
        })
        window.onpopstate = function () {
            activateLanguage(getLanguageFromQueryString())
        }
    })
    const makeToc = function () {
        global.toc = $('#toc').tocify({
            selectors: 'h1, h2',
            extendPage: false,
            theme: 'none',
            smoothScroll: false,
            showEffectSpeed: 0,
            hideEffectSpeed: 180,
            ignoreSelector: '.toc-ignore',
            highlightOffset: 60,
            scrollTo: -1,
            scrollHistory: true,
            hashGenerator(text, element) {
                return element.prop('id')
            },
        }).data('toc-tocify')
    }
    // Hack to make already open sections to start opened,
    // instead of displaying an ugly animation
    function animate() {
        setTimeout(function () {
            toc.setOption('showEffectSpeed', 180)
        }, 50)
    }
    $(function () {
        makeToc()
        animate()
        $('.content').imagesLoaded(function () {
            global.toc.calculateHeights()
        })
    })
}
function search() {
    'use strict'
    let content
    let searchResults
    const highlightOpts = {
        element: 'span',
        className: 'search-highlight',
    }
    const index = new lunr.Index()
    index.ref('id')
    index.field('title', { boost: 10 })
    index.field('body')
    index.pipeline.add(lunr.trimmer, lunr.stopWordFilter)
    $(populate)
    $(bind)
    function populate() {
        $('h1, h2').each(function () {
            const title = $(this)
            const body = title.nextUntil('h1, h2')
            index.add({
                id: title.prop('id'),
                title: title.text(),
                body: body.text(),
            })
        })
    }
    function bind() {
        content = $('.content')
        searchResults = $('.search-results')
        $('#input-search').on('keyup', search)
    }
    function search(event) {
        unhighlight()
        searchResults.addClass('visible')
        // ESC clears the field
        if (event.keyCode === 27) this.value = ''
        if (this.value) {
            const results = index.search(this.value).filter(function (r) {
                return r.score > 0.0001
            })
            if (results.length) {
                searchResults.empty()
                $.each(results, function (index, result) {
                    const elem = document.getElementById(result.ref)
                    searchResults.append(`<li><a href="#${result.ref}">${$(elem).text()}</a></li>`)
                })
                highlight.call(this)
            } else {
                searchResults.html('<li></li>')
                $('.search-results li').text(`No Results Found for "${this.value}"`)
            }
        } else {
            unhighlight()
            searchResults.removeClass('visible')
        }
    }
    function highlight() {
        if (this.value) content.highlight(this.value, highlightOpts)
    }
    function unhighlight() {
        content.unhighlight(highlightOpts)
    }
}
function highlightMarkdownFencedBlocksCode() {
    Prism.languages.axios = Prism.languages.javascript
    Prism.highlightAll()
}
function shiftTimezone() {
    document.querySelectorAll('time[data-shift-timezone]').forEach(element => {
        element.innerText = new Date(element.innerText)
    })
}
