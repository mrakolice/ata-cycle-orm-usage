# Academata

Внутренний проект ATA-Back

## Начало работы

### Установка


```
make init
```

* Проект будет доступен на `http://localhost/`
### Запуск

```
docker-compose up -d
```

### Вход в контейнер приложения: 

```
docker-compose exec -u docker-user app bash
```

### Остановка

```
docker-compose down
```

## Make-команды

Makefile содержит следующие команды:
* `make` или `make list`  или `make all` - выводит список доступных make-команд;
* `make init` - выполняет команду`make clean`, подготавливает файл .env, устанавливает composer-зависимости, выполняет миграции и сиды, выполняет `make ide-helper`;
* `make clean` - останавливает докер-контейнеры, удаляет все сервисы и повисшие контейнеры, сети и тома docker;
* `make ide-helper` - генерация вспомогательной информации для PhpStorm. Это нужно, чтобы PhpStorm видел методы моделей и все вспомогательные фунцкии Laravel.

